import { createContext, useState } from 'react'
import React from 'react';
import PropTypes from 'prop-types';

export const ModalContext = createContext({})



const ModalProvider = ({ children }) => {
    const [currentEditProduct, setCurrentEditProduct] = useState({})
    const [currentEditCategory, setcurrentEditCategory] = useState({})
    const [showModalAuth, setShowModalAuth] = useState(false)
    const [showModalOrder, setShowModalOrder] = useState(false)
    const [showMenuMobile, setShowMenuMobile] = useState(false)
    const [showModalCreateProducts, setShowModalCreateProducts] = useState(false);
    const [showModalOrderCanceled, setshowModalOrderCanceled] = useState(false);
    const [showModalCreateCategory, setShowModalCreateCategory] = useState(false);
    const [showModalEditCategory, setShowModalEditCategory] = useState(false);


    const toggleScrollBody = (flag) => {
        const classBody = document.querySelector('body')
        if (flag) {
            classBody.classList.remove('is-overflow')
        } else {
            classBody.classList.add('is-overflow')
        }
    }

    const fnshowMenuMobile = () => {
        setShowMenuMobile(!showMenuMobile)
        toggleScrollBody(showMenuMobile)

    }

    const fnShowModalAuth = () => {
        setShowModalAuth(!showModalAuth)
        toggleScrollBody(showModalAuth)

    }
    const fnShowModalOrder = () => {
        setShowModalOrder(!showModalOrder)
        toggleScrollBody(showModalOrder)
    }
    const fnShowModalCreateProducts = (dataProduct = undefined) => {
        if (dataProduct) {
            setCurrentEditProduct(dataProduct)
        } else {
            setCurrentEditProduct({})
        }
        setShowModalCreateProducts(!showModalCreateProducts);
        toggleScrollBody(showModalCreateProducts)
    }
    const fnShowModalOrderCanceled = () => {
        setshowModalOrderCanceled(!showModalOrderCanceled);
        toggleScrollBody(showModalOrderCanceled)
    }

    const fnShowModalCreateCategory = (dataCategoty = undefined) => {

        if (dataCategoty) {
            setcurrentEditCategory(dataCategoty)
        } else {
            setcurrentEditCategory({})
        }
        setShowModalCreateCategory(!showModalCreateCategory);
        toggleScrollBody(showModalCreateCategory)

    }
    const fnShowModalEditCategory = () => {
        setShowModalEditCategory(!showModalEditCategory);
    }

    return (
        <ModalContext.Provider
            value={{
                showModalAuth,
                fnShowModalAuth,

                showModalCreateProducts,
                fnShowModalCreateProducts,

                showMenuMobile,
                fnshowMenuMobile,

                showModalOrderCanceled,
                fnShowModalOrderCanceled,

                showModalCreateCategory,
                fnShowModalCreateCategory,

                showModalEditCategory,
                fnShowModalEditCategory,

                showModalOrder,
                fnShowModalOrder,

                currentEditProduct,
                currentEditCategory
            }}
        >
            {children}
        </ModalContext.Provider >
    );
};

ModalProvider.propTypes = {};

export default ModalProvider;