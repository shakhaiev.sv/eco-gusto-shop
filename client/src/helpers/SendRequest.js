import { HOST_DEV, HOST_PROD } from '../contans/API.js'

export const sendRequest = async (url, method, config, token) => {

    if (method === 'GET' && token) {
        const responce = await fetch(`${HOST_PROD}${url}`, {
            method,
            headers: {
                "Authorization": token,
                "Content-Type": "application/json",
            },
        })
        const result = await responce.json()
        return result

    } else if (token) {
        const responce = await fetch(`${HOST_PROD}${url}`, {
            method,
            headers: {
                "Authorization": token,
                "Content-Type": "application/json",
            },
            body: JSON.stringify(config)
        })
        const result = await responce.json()
        return result

    } else if (method === 'GET') {
        const responce = await fetch(`${HOST_PROD}${url}`, {
            method,

            headers: {
                "Content-Type": "application/json",
            },
        })
        const result = await responce.json()
        return result

    } else {
        const responce = await fetch(`${HOST_PROD}${url}`, {
            method,
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(config)
        })

        const result = await responce.json()
        return result

    }
}


export const sendRequestUploadImage = async (url, method, config, token) => {

    const responce = await fetch(`${HOST_PROD}${url}`, {
        method,
        headers: {
            "path": "./static/images/products/",
            "Authorization": token,
        },
        body: config
    })
    const result = await responce.json()
    return result;
}


