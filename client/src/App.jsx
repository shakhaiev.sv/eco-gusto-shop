import './App.scss';
import RootRoters from './routers';
import Header from './components/Header/Header.jsx';
import Footer from './components/Footer/Footer.jsx';
import { useEffect } from 'react';
import { actionSetListFavoriteFromLocalStorage } from '../src/store/slices/favorite.js';
import { selectorLoadingCategoties, selectorLoadingPopularproducts, selectorLoadingDeals } from './store/selectors/selectors.js'
import { actiohSetCartFromLocalSrorage, actionAllLengthCoodsOnCart } from './store/slices/cart.js';
import { actionSetTokenFromLocaleStorage } from './store/slices/clients.js'
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import './App.scss';

function App() {
    const location = useLocation()
    const dispatch = useDispatch();
    const loadingCategories = useSelector(selectorLoadingCategoties)
    const loadingDeals = useSelector(selectorLoadingDeals)
    const loadingpopularProducts = useSelector(selectorLoadingPopularproducts)

    useEffect(() => {
        dispatch(actionSetListFavoriteFromLocalStorage());
        dispatch(actiohSetCartFromLocalSrorage());
        dispatch(actionAllLengthCoodsOnCart());
        dispatch(actionSetTokenFromLocaleStorage())
    }, []);

    useEffect(() => {
        if (location.pathname !== '/') {
            const intialLoader = document.getElementById('loader')
            if (intialLoader) {
                intialLoader.remove()

            }

        }
    }, [])


    useEffect(() => {

        if (!loadingCategories && !loadingDeals && !loadingpopularProducts) {
            const intialLoader = document.getElementById('loader')
            if (intialLoader) {
                intialLoader.remove()

            }
        }
    }, [loadingCategories, loadingDeals, loadingpopularProducts])


    return (
        <>
            <Header />
            <main>
                <RootRoters />
            </main>
            <Footer />
        </>

    );
}

export default App;
