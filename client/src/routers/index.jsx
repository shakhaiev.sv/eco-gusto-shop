import { Routes, Route } from 'react-router-dom';
import HomePage from '../pages/HomePage/HomePage.jsx'
import AccountPage from '../pages/AccountPage/AccountPage.jsx';
import CartPage from '../pages/CartPage/CartPage.jsx';
import FavoritesPage from '../pages/FavoritesPage/FavoritesPage.jsx';
import ContactPage from '../pages/contactPage/ContactPage.jsx'
import AboutPage from '../pages/AboutPage/AboutPage.jsx'
import GoodsPage from '../pages/GoodsPage/GoodsPage.jsx';
import DealsPage from '../pages/DealsPage/DealsPage.jsx';
import DealsPageToId from '../pages/DealsPageToId/DealsPageToId.jsx'
import ProductViewPage from '../pages/ProductViewPage/ProductViewPage.jsx';
import RegistrationPage from '../pages/RegistrationPage/RegistrationPage.jsx'
import NotFoundPage from '../pages/NotFoundPage/NotFoundPage.jsx'
import Blogpage from '../pages/BlogPage/BlogPage.jsx';



export default () => {

    return (
        <Routes>
            <Route path={'/'} element={<HomePage />} />
            <Route path={"/products"} element={<GoodsPage />} />
            <Route path={"/account"} element={<AccountPage />} />
            <Route path={"/cart"} element={<CartPage />} />
            <Route path={"/favorite"} element={<FavoritesPage />} />
            <Route path={"/deals"} element={<DealsPage />} />
            <Route path={"/deals/:id"} element={<DealsPageToId />} />
            <Route path={"/product/:id"} element={<ProductViewPage />} />
            <Route path={"/registration"} element={<RegistrationPage />} />
            <Route path={"/blog"} element={<Blogpage />} />
            <Route path={"/contact"} element={<ContactPage />} />
            <Route path={"/about"} element={<AboutPage />} />
            <Route path={"*"} element={<NotFoundPage />} />
        </Routes>
    )

}