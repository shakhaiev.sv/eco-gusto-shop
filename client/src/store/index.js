import { configureStore } from '@reduxjs/toolkit';
import submitFormSlice from './slices/clients.js'
import categoriesrSlice from './slices/categories.js'
import dealsSlice from './slices/deals.js'
import productsSlice from './slices/products.js'
import favoriteSlice from './slices/favorite.js'
import cartSlice from './slices/cart.js';
import registrationReducer from './slices/registrationSlice';
import orderSlice from './slices/order.js'
import userReducer from './slices/userSlice';
import reviewSlice from "./slices/reviewSlice.js";


export default configureStore({

  reducer: {
    submitForm: submitFormSlice,
    categories: categoriesrSlice,
    deals: dealsSlice,
    cart: cartSlice,
    products: productsSlice,
    favorite: favoriteSlice,
    order: orderSlice,
    registration: registrationReducer,
    user: userReducer,
    reviews: reviewSlice,
  }

})

