export const selectorContactForm = (store) => store.submitForm.contactForm;
export const selectorResponceAuth = (store) => store.submitForm.responceAuth;
export const selectorAuthorizationForm = (store) => store.submitForm.dataAuthorizationFormClient;
export const selectorResponceAuthorization = (store) => store.submitForm.responceNotValid;
export const selectorUserLogin = (store) => store.submitForm.dataAuthorizationFormClient;
export const selectorToken = (store) => store.submitForm.token;
export const selectorAdmin = (store) => store.submitForm.isAdmin;

export const selectorCategories = (store) => store.categories.categoriesList;
export const selectorLoadingCategoties = (store) => store.categories.loading

export const selectorDeals = (store) => store.deals.dealsList;
export const selectorDealsById = (store) => store.deals.dealsById;
export const selectorBestDeals = (store) => store.deals.bestDeals;
export const selectorPageDeals = (store) => store.deals.paginationDeals
export const selectorLoadingDeals = (store) => store.deals.loading

export const selectorPopularProducts = (store) => store.products.popularProducts
export const selectorProducts = (store) => store.products.allProducts;
export const selectorCurrentProduct = (store) => store.products.currentProduct;
export const selectorPopularProductsPage = (store) => store.products.popularProductsPage;
export const selectorNextPagePopulaProducts = (store) => store.products.nextPagePopulaProducts;
export const selectorLoadingPopularproducts = (store) => store.products.loading


export const selectorProductsPage = (store) => store.products.productsPage;
export const selectorNextPageProducts = (store) => store.products.nextPageProducts;
export const selectorQueryParamsProducts = (store) => store.products.queryParamsProducts;
export const selectorListFavorite = (store) => store.favorite.listFavorite;

export const selectorListCart = (store) => store.cart.listCart;
export const selectorLengthListCart = (store) => store.cart.lengthListCart;
export const selectorSummCart = (store) => store.cart.summCart;
export const selectorResponceCart = (store) => store.cart.responeCart;


export const selectorDataClientForDeliveryCart = (store) => store.order.dataClientForDelivetyGoods;
export const selectorDataResponceFromOrder = (store) => store.order.dataResponce;
export const selectorStatisticData = (store) => store.order.statisticData;
export const selectorAllOrderClient = (store) => store.order.orderClient;
export const selectorDataResponceCanceledOrder = (store) => store.order.dataResponceCanceledOrder;
export const selectorOrderClientForAdmin = (store) => store.order.orderClientForAdmin;
export const selectorRespShippedOrder = (store) => store.order.dataRespShippedOrder;
export const selectorOrdersStatuses = (store) => store.order.statuses;
export const selectorOrdersDelete = (store) => store.order.dataResOrderDelete;
export const selectorDateFilterOrderCient = (store) => store.order.dateFilterOrderCient;
export const selectorDateFilterStatistic = (store) => store.order.dateFilterStatistic;
export const selectorLoadingOrder = (store) => store.order.loadingOrder;



export const selectorUserFirstName = (store) => store.user.firstName
export const selectorUserLastName = (store) => store.user.lastName
export const selectorUserEmail = (store) => store.user.lastName
export const selectorUserPhone = (store) => store.user.lastName
export const selectorDataClient = (store) => store.user.dataClient



export const selectorReviews = (store) => store.reviews.allReviews
export const selectorProductReviews = (store) => store.reviews.currentProductReviews
export const selectorNewReviewFormData = (store) => store.reviews.newReviewFormData
export const selectorNewReview = (store) => store.reviews.newReview

export const selectorResponceRegistr = (store) => store.registration.resRegistration

