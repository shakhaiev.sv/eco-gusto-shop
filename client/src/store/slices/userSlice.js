
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { sendRequest } from '../../helpers/SendRequest';
import { selectorToken, selectorUserLogin } from '../../store/selectors/selectors';


export const fetchUserData = createAsyncThunk(
  'user/fetchUserData',
  async (_, { getState, rejectWithValue }) => {
    const state = getState();
    const token = selectorToken(state);
    const userLogin = selectorUserLogin(state);
    if (userLogin && token) {
      try {
        const response = await sendRequest('/customers/customer', 'POST', {
          login: userLogin.loginOrEmail
        }, token);
        return response;
      } catch (error) {
        return rejectWithValue(error.message);
      }
    } else {
      return rejectWithValue('User login or token not found');
    }
  }
);

export const updateUserData = createAsyncThunk(
  'user/updateUserData',
  async (updatedData, { getState, rejectWithValue }) => {
    const state = getState();
    const token = selectorToken(state);
    try {
      const response = await sendRequest('/customers', 'PUT', updatedData, token);
      return response;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const userSlice = createSlice({
  name: 'user',
  initialState: {
    dataClient: {},
    id: '',
    firstName: '',
    lastName: '',
    email: '',
    login: '',
    gender: '',
    telephone: '',
    loading: false,
    error: null,
    successMessage: null
  },
  reducers: {
    resetState: (state) => {
      state.error = null;
      state.successMessage = null;
    },
    actionLogOutUser: (state) => {
      state.firstName = ''
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUserData.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchUserData.fulfilled, (state, action) => {
        state.loading = false;
        const { id, firstName, lastName, email, login, gender, telephone } = action.payload;
        state.id = id;
        state.firstName = firstName;
        state.lastName = lastName;
        state.email = email;
        state.login = login;
        state.gender = gender;
        state.telephone = telephone;
        state.dataClient = action.payload
      })
      .addCase(fetchUserData.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      })
      .addCase(updateUserData.pending, (state) => {
        state.loading = true;
      })
      .addCase(updateUserData.fulfilled, (state) => {

        state.loading = false;
        state.successMessage = 'User data successfully updated!';

      })
      .addCase(updateUserData.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      })

  }
});

export const { resetState, actionLogOutUser } = userSlice.actions;

export default userSlice.reducer;
