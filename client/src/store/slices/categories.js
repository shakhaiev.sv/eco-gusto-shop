import { sendRequest } from '../../helpers/SendRequest.js'
import { API_URL_CATEGORIES } from '../../contans/API.js'
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
    firstRender: true,
    categoriesList: [],
    loading: true
}

export const actionFetchCategories = createAsyncThunk(
    'categories/fetchAllCategories',
    async () => {

        const result = await sendRequest(API_URL_CATEGORIES, 'GET')
        return result
    }
)


const categoriesSlice = createSlice({
    name: 'categories',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchCategories.fulfilled, (state, { payload }) => {
                const listCategorieseEdit = payload.map(({ _id: id, name, imgUrl }) => { return { id, name, imgUrl } })
                state.categoriesList = [...listCategorieseEdit]
                if (state.firstRender && payload.length) {
                    state.loading = false
                    state.firstRender = false
                }

            })
    }
})

export default categoriesSlice.reducer