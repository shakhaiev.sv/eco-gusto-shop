import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import { sendRequest } from "../../helpers/SendRequest.js";
import { API_URL_PRODUCTS } from '../../contans/API.js'
import { paginationLimit } from '../../contans/constans.js'

const initialState = {

    allProducts: [],
    firsrtRender: true,
    loading: true,
    popularProducts: [],
    currentProduct: {},
    popularProductsPage: {
        page: 1,
        limit: paginationLimit
    },
    nextPagePopulaProducts: {},
    productsPage: {
        page: 1,
        limit: paginationLimit
    },
    nextPageProducts: {},
    queryParamsProducts: ''
}

export const actionFetchAllProducts = createAsyncThunk(
    'products/FetchAllProducts',
    async () => {
        const result = await sendRequest(API_URL_PRODUCTS, 'GET')
        return result.data
    }
)

export const actionFetchPopularProduct = createAsyncThunk(
    'products/FetchPopularProduct',
    async ({ queryParams, pagination }) => {
        const responce = await sendRequest(`${API_URL_PRODUCTS}/filter?${queryParams}`, "POST", pagination)
        return responce

    }
)

export const actionFetchProducts = createAsyncThunk(
    'products/FetchProducts',
    async ({ queryParams, pagination }) => {
        const responce = await sendRequest(`${API_URL_PRODUCTS}/filter${queryParams}`, "POST", pagination)
        return responce

    }
)

export const actionFetchProdyctById = createAsyncThunk(
    'product/FetchProdyctById',
    async (idProduct) => {
        const responce = await sendRequest(`${API_URL_PRODUCTS}/${idProduct}`, "GET")
        return responce
    }

)

const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchPopularProduct.fulfilled, (state, { payload }) => {

                if (payload.results) {
                    if (!payload.previous) {
                        state.popularProducts = payload.results.filter((item) => item.quantity)
                    } else {
                        state.popularProducts = [...state.popularProducts, ...payload.results.filter((item) => item.quantity)]
                    }
                    if (state.firsrtRender) {
                        state.loading = false
                        state.firsrtRender = false
                    }
                }

                if (payload.next) {
                    state.nextPagePopulaProducts = payload.next
                } else {
                    state.nextPagePopulaProducts = {}
                }


            })
            .addCase(actionFetchAllProducts.fulfilled, (state, { payload }) => {
                state.allProducts = payload;
            })
            .addCase(actionFetchProdyctById.fulfilled, (state, { payload }) => {
                state.currentProduct = payload;
            })
            .addCase(actionFetchProducts.fulfilled, (state, { payload }) => {

                if (!payload.result && payload.message) {
                    state.allProducts.length = 0

                    return
                }
                if (payload.results) {
                    if (!payload.previous) {
                        state.allProducts = payload.results
                    } else {
                        state.allProducts = [...state.allProducts, ...payload.results]
                    }
                }

                if (payload.next) {
                    state.nextPageProducts = payload.next
                } else {
                    state.nextPageProducts = {}
                }

                const stockEmpty = state.allProducts.filter((item) => item.quantity === 0)
                const stock = state.allProducts.filter((item) => item.quantity > 0)

                state.allProducts = [...stock, ...stockEmpty]

            })
    }
})

export const { } = productsSlice.actions

export default productsSlice.reducer


