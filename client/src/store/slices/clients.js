import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { sendRequest } from "../../helpers/SendRequest.js";
import { timeOfActionToken } from "../../contans/constans.js";

const initialState = {
    contactForm: {
        firstName: '',
        email: '',
        phone: '',
        subject: '',
        description: ''
    },
    dataAuthorizationFormClient: {
        loginOrEmail: '',
        password: '',
    },
    responceAuth: {},
    token: '',
    userLogin: '',
    isAdmin: false,
}

export const actionsFetchAuthorizationClient = createAsyncThunk(
    'customer/fetchAuthorizationClient',
    async ({ loginOrEmail, password }) => {
        const resultToken = await sendRequest('/customers/login', "POST", {
            'loginOrEmail': loginOrEmail,
            'password': password
        })
        return resultToken
    }
)


const submitFormSlice = createSlice({
    name: 'submit-form',
    initialState,
    reducers: {
        actionSubmitFormContact: (state, { payload }) => {
            alert('Data sent')
        },
        actionDataAuthorizationClient: (state, { payload }) => {
            state.dataAuthorizationFormClient = { ...payload }
        },
        actionSetTokenFromLocaleStorage: (state) => {
            const dataAuth = JSON.parse(localStorage.getItem('dataAuth'))
            if (dataAuth) {
                const currentTime = Date.parse(new Date())
                if ((currentTime - dataAuth.timestamp) < timeOfActionToken) {
                    state.token = dataAuth.token
                    state.dataAuthorizationFormClient = { 'loginOrEmail': dataAuth.login }
                    if (dataAuth.success === 1) {
                        state.isAdmin = true
                    }
                }
            }
        },
        actionLogOut: (state, { payload }) => {



            state.token = ''
            state.userLogin = '',
                state.isAdmin = false,
                state.responceAuth = ''
            state.dataAuthorizationFormClient = {
                loginOrEmail: '',
                password: '',
            }
            localStorage.removeItem('dataAuth')


        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(actionsFetchAuthorizationClient.fulfilled, (state, { payload }) => {

                state.responceAuth = payload
                if (state.responceAuth.success && state.responceAuth.token) {
                    state.token = payload.token
                    if (state.responceAuth.success === 1) {
                        state.isAdmin = true
                    }
                    localStorage.setItem('dataAuth', JSON.stringify({
                        'login': state.dataAuthorizationFormClient.loginOrEmail,
                        'token': state.token,
                        'success': state.responceAuth.success,
                        'timestamp': Date.parse(new Date())
                    }))
                    state.dataAuthorizationFormClient.password = ' '

                }
            })
    }
}
)

export const {
    actionSubmitFormContact,
    actionDataAuthorizationClient,
    actionSetTokenFromLocaleStorage,
    actionLogOut
} = submitFormSlice.actions

export default submitFormSlice.reducer