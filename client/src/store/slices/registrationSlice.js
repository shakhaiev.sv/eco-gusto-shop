
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { sendRequest } from '../../helpers/SendRequest';

const initialState = {
  loading: false,
  error: null,
  success: false,
  resRegistration: {}
};


export const registerUser = createAsyncThunk(
  'registration/registerUser',
  async (userData, thunkAPI) => {

    try {

      const dataNewClient = {
        firstName: userData.fullName.split(' ')[0],
        lastName: userData.fullName.split(' ')[1] || '',
        login: userData.username,
        email: userData.email,
        password: userData.password,
        telephone: userData.telephone,
        gender: userData.gender,
      };


      const response = await sendRequest('/customers', 'POST', dataNewClient);
      return response
    } catch (error) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);


const registrationSlice = createSlice({
  name: 'registration',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(registerUser.fulfilled, (state, { payload }) => {

        state.resRegistration = payload
        state.loading = false;
        state.success = true;

      })
      .addCase(registerUser.pending, (state,) => {

        state.loading = true;
        state.error = null;
        state.success = false;
      })

      .addCase(registerUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      });
  },
});

export const { } = registrationSlice.actions

export default registrationSlice.reducer;
