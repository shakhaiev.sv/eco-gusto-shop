import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { sendRequest } from '../../helpers/SendRequest.js'; // Адаптуйте шлях до вашого хелпера

// Асинхронна функція для оновлення списку улюблених
export const actionUpdateListFavoritesForAuthClient = createAsyncThunk(
    'listFavorite/updateFavorites',
    async ({ token, dataFavoritesList }, { rejectWithValue }) => {
        try {
            const response = await sendRequest('/wishlist', 'PUT', dataFavoritesList, token);
            return response; // Повертаємо дані для обробки в extraReducers
        } catch (error) {
            return rejectWithValue(error.message);
        }
    }
);



// Асинхронна функція для отримання списку улюблених
export const actionGetFavoritesForAuthClient = createAsyncThunk(
    'listFavorite/getFavorites',
    async (token, { rejectWithValue }) => {
        try {
            const response = await sendRequest('/wishlist', 'GET', undefined, token);
            return response; // Повертаємо дані для обробки в extraReducers
        } catch (error) {
            return rejectWithValue(error.message);
        }
    }
);

// Додавання до списку улюблених
export const actionAddProductToFavoritesForAuthClient = createAsyncThunk(
    'favorites/fetchAddProductToFavoritesForAuthClient',
    async ({ token, idProduct }) => {
        const result = await sendRequest(`/wishlist/${idProduct}`, 'PUT', undefined, token)
        return result
    }
)

// Видалення зі списку улюблених
export const actionDeleteProductFromFavoritesAuthClient = createAsyncThunk(
    'favorites/fetchDeleteProductFromFavoritesAuthClient',
    async ({ token, idProduct }) => {
        const result = await sendRequest(`/wishlist/${idProduct}`, 'DELETE', undefined, token)
        return result
    }
)

const initialState = {
    listFavorite: [], // Завжди масив
    status: 'idle', // Статус для асинхронних операцій
    error: null,
};

const favoriteSlice = createSlice({
    name: 'listFavorite',
    initialState,
    reducers: {
        actionAddFavoriteList: (state, { payload }) => {
            if (!Array.isArray(state.listFavorite)) {
                state.listFavorite = []; // Переконайтеся, що це масив
            }
            if (state.listFavorite.some((item) => item._id === payload._id)) {
                state.listFavorite = state.listFavorite.filter((item) => item._id !== payload._id);
            } else {
                state.listFavorite.push(payload);
            }
            localStorage.setItem('listFavorite', JSON.stringify(state.listFavorite));
        },
        actionSetListFavoriteFromLocalStorage: (state) => {

            const listFavoriteFromLS = localStorage.getItem('listFavorite');
            if (listFavoriteFromLS) {
                try {
                    const parsedList = JSON.parse(listFavoriteFromLS);
                    if (Array.isArray(parsedList)) {
                        state.listFavorite = parsedList;
                    } else {
                        console.warn('Неправильний формат listFavorite в localStorage');
                        state.listFavorite = [];
                    }
                } catch (e) {
                    console.error('Помилка при парсингу listFavorite з localStorage', e);
                    state.listFavorite = [];
                }
            }
        },
        actionClearLocalStorageFavorites: (state) => {

            localStorage.removeItem('listFavorite');
            state.listFavorite = [];
        }

    },
    extraReducers: (builder) => {
        builder
            .addCase(actionGetFavoritesForAuthClient.fulfilled, (state, action) => {
                state.listFavorite = action.payload?.products || []
                localStorage.setItem('listFavorite', JSON.stringify(state.listFavorite));
            })
            .addCase(actionAddProductToFavoritesForAuthClient.fulfilled, (state, { payload }) => {
                if (payload) {
                    state.listFavorite = payload.products?.map((product) => product)
                    localStorage.setItem('listFavorite', JSON.stringify(state.listFavorite))
                }
            })

            .addCase(actionDeleteProductFromFavoritesAuthClient.fulfilled, (state, { payload }) => {
                if (payload.products) {
                    state.listFavorite = payload.products?.map((product) => product)
                    localStorage.setItem('listFavorite', JSON.stringify(state.listFavorite))
                }
            })
    },
});

export const {
    actionAddFavoriteList,
    actionSetListFavoriteFromLocalStorage,
    actionClearLocalStorageFavorites,
} = favoriteSlice.actions;

export default favoriteSlice.reducer;
