
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { sendRequest } from '../../helpers/SendRequest.js';
import { API_URL_CART } from '../../contans/API.js'

const initialState = {
  listCart: [],
  lengthListCart: 0,
  summCart: 0,
  responeCart: {},
}

export const actionAddProductOnDealsForAuthClient = createAsyncThunk(
  'cart/fetchAddProductOnDealsForAuthClient',
  async ({ token, dataDeals }) => {
    const result = await sendRequest(API_URL_CART, 'PUT', dataDeals, token)
    return result
  }
)

export const actionUpdateListCardForAuthClient = createAsyncThunk(
  'cart/fetchUpdateListCartForAuthClient',
  async ({ token, dataCartList }) => {
    const result = await sendRequest(API_URL_CART, 'PUT', dataCartList, token)
    return result
  }
)

export const actionAddProductToCartForAuthClient = createAsyncThunk(
  'cart/fetchAddProductToCartForAuthClient',
  async ({ token, idProduct }) => {
    const result = await sendRequest(`${API_URL_CART}/${idProduct}`, 'PUT', undefined, token)
    return result
  }
)

export const actionGetCartForAuthClient = createAsyncThunk(
  'cart/fetchGetCartForAuthClient',
  async (token) => {
    const result = await sendRequest(API_URL_CART, 'GET', undefined, token)
    return result
  }
)

export const actionDeleteProductFromCartAuthClient = createAsyncThunk(
  'cart/fetchDeleteProductFromCartAuthClient',
  async ({ token, idProduct }) => {
    const result = await sendRequest(`${API_URL_CART}/${idProduct}`, 'DELETE', undefined, token)
    return result
  }
)

export const actionIncreaseProductInCartAuthClient = createAsyncThunk(
  'cart/fetchIncreaseProductInCart',
  async ({ token, idProduct }) => {
    const result = await sendRequest(`${API_URL_CART}/${idProduct}`, 'PUT', undefined, token)
    return result
  }
)

export const actionDecreaseProductInCartAuthClient = createAsyncThunk(
  'cart/fetchDecreaseProductInCart',
  async ({ token, idProduct }) => {
    const result = await sendRequest(`${API_URL_CART}/product/${idProduct}`, 'DELETE', undefined, token)
    return result
  }
)

export const actionDeleteCartAuthClient = createAsyncThunk(
  'art/fetchDeleteCart',
  async (token) => {
    const result = await sendRequest(API_URL_CART, 'DELETE', undefined, token)
    return result
  }
)

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    actionAddCardToCartList: (state, { payload }) => {
      if (state.listCart?.some((item) => item._id === payload._id)) {

        state.listCart = state.listCart.map((item) => {
          if (item._id === payload._id) {
            item.cartQuantity = item.cartQuantity + 1
            return item
          }
          return item

        })
        localStorage.setItem('listCart', JSON.stringify(state.listCart))
      } else {
        state.listCart = [...state.listCart, { ...payload, 'cartQuantity': 1 }]
        localStorage.setItem('listCart', JSON.stringify(state.listCart))
      }
    },
    actiohSetCartFromLocalSrorage: (state, action) => {
      const listCartFromLS = localStorage.getItem('listCart')
      if (listCartFromLS) {
        state.listCart = [...JSON.parse(listCartFromLS)]
      }
    },
    actionRemoveCardFromCartList: (state, { payload }) => {
      state.listCart = [...state.listCart].filter((item) => item._id !== payload._id)
      localStorage.setItem('listCart', JSON.stringify(state.listCart))
    },
    actionAllLengthCoodsOnCart: (state) => {
      state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)

    },
    actionSummCart: (state) => {
      state.summCart = state.listCart.reduce((acum, item) => acum + Number(item.cartQuantity).toFixed(3) * Number(item?.currentPrice).toFixed(2), 0)
    },
    actionIncreaseProductInCart: (state, { payload }) => {
      state.listCart = state.listCart.map((item) => {
        if (item._id === payload._id) {
          item.cartQuantity += 1
        }
        return item
      })
      localStorage.setItem('listCart', JSON.stringify(state.listCart))
    },
    actionDecreaseProductInCart: (state, { payload }) => {
      state.listCart = state.listCart.filter((item) => {
        if (item._id === payload._id) {
          if (item.cartQuantity > 1) {
            item.cartQuantity -= 1
            return item
          }
        } else {
          return item
        }

      })
      localStorage.setItem('listCart', JSON.stringify(state.listCart))
    },
    actionDeleteCart: (state, { payload }) => {
      state.listCart.length = 0
      state.lengthListCart = 0
      localStorage.removeItem('listCart')
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(actionAddProductToCartForAuthClient.fulfilled, (state, { payload }) => {
        state.listCart = payload.products?.map(({ product, cartQuantity }) => { return { cartQuantity, ...product } })
        localStorage.setItem('listCart', JSON.stringify(state.listCart))
        state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
      })

      .addCase(actionGetCartForAuthClient.fulfilled, (state, { payload }) => {
        if (payload && payload.products[0].product !== null) {
          state.listCart = payload.products?.map(({ product, cartQuantity }) => { return { cartQuantity, ...product } })
          localStorage.setItem('listCart', JSON.stringify(state.listCart))
          state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
        }

      })
      .addCase(actionDeleteProductFromCartAuthClient.fulfilled, (state, { payload }) => {
        if (payload.products) {
          state.listCart = payload.products?.map(({ product, cartQuantity }) => { return { cartQuantity, ...product } })
          localStorage.setItem('listCart', JSON.stringify(state.listCart))
          state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
          state.summCart = state.listCart.reduce((acum, item) => acum + Number(item.cartQuantity).toFixed(3) * Number(item?.currentPrice).toFixed(2), 0)
        }
      })
      .addCase(actionIncreaseProductInCartAuthClient.fulfilled, (state, { payload }) => {
        state.listCart = payload.products?.map(({ product, cartQuantity }) => { return { cartQuantity, ...product } })
        localStorage.setItem('listCart', JSON.stringify(state.listCart))
        state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
        state.summCart = state.listCart.reduce((acum, item) => acum + Number(item.cartQuantity).toFixed(3) * Number(item?.currentPrice).toFixed(2), 0)

      })
      .addCase(actionDecreaseProductInCartAuthClient.fulfilled, (state, { payload }) => {
        state.listCart = payload.products?.map(({ product, cartQuantity }) => { return { cartQuantity, ...product } })
        localStorage.setItem('listCart', JSON.stringify(state.listCart))
        state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
        state.summCart = state.listCart.reduce((acum, item) => acum + Number(item.cartQuantity).toFixed(3) * Number(item?.currentPrice).toFixed(2), 0)
      })
      .addCase(actionDeleteCartAuthClient.fulfilled, (state, { payload }) => {
        if (payload.result) {
          state.listCart.length = 0
          localStorage.removeItem('listCart')
          state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
        }
      })
      .addCase(actionAddProductOnDealsForAuthClient.fulfilled, (state, { payload }) => {
        state.listCart = payload.products?.map(({ product, cartQuantity }) => { return { cartQuantity, ...product } })
        localStorage.setItem('listCart', JSON.stringify(state.listCart))
        state.lengthListCart = state.listCart.reduce((acum, item) => acum + item?.cartQuantity, 0)
        state.summCart = state.listCart.reduce((acum, item) => acum + Number(item.cartQuantity).toFixed(3) * Number(item?.currentPrice).toFixed(2), 0)
      })
  }
});

export const {
  actionAddCardToCartList,
  actiohSetCartFromLocalSrorage,
  actionRemoveCardFromCartList,
  actionDeleteCart,
  actionAllLengthCoodsOnCart,
  actionSummCart,
  actionIncreaseProductInCart,
  actionDecreaseProductInCart
} = cartSlice.actions;

export default cartSlice.reducer;
