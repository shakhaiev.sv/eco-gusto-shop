
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { API_URL_ORDER } from '../../contans/API.js'
import { sendRequest } from '../../helpers/SendRequest.js'


const initialState = {
  dataResponce: {},
  dataClientForDelivetyGoods: {
    firstName: '',
    lastName: '',
    phone: '',
    country: '',
    deliverDate: '',
    city: '',
    streetAddress: '',
    email: ''
  },
  dateFilterOrderCient: {},
  dateFilterStatistic: {},
  statisticData: {},
  orderClient: [],
  dataResponceCanceledOrder: {},
  dataRespShippedOrder: {},
  dataResOrderDelete: {},
  orderClientForAdmin: [],
  statuses: {},
  loadingOrder: false
}


// отримати замовлення для клієнтів 
export const actionGetOrderClient = createAsyncThunk(

  'order/fetchGetOrderClient',
  async (token) => {
    const responce = await sendRequest(API_URL_ORDER, "GET", undefined, token)
    return responce
  }
)
// для  авторизованих клієнтів 
export const actionSendOrderForAuthClient = createAsyncThunk(
  'order/fetchSendOrderForAuthClient',
  async ({ token, dataClientForOrder }) => {
    const result = await sendRequest(API_URL_ORDER, 'POST', dataClientForOrder, token)
    return result
  }
)

export const actionGetAllOrders = createAsyncThunk(
  'order/fetchSendOrders',
  async ({ token, dataFilter }) => {
    const result = await sendRequest(`${API_URL_ORDER}/clients`, 'POST', dataFilter, token)
    return result
  }
)

export const actionGetAllOrdersForStatistic = createAsyncThunk(
  'order/fetchSendOrdersForStatistic',
  async ({ token, dataFilter }) => {
    const result = await sendRequest(`${API_URL_ORDER}/clients`, 'POST', dataFilter, token)
    return result
  }
)
// для не авторизованих клієнтів 
export const actionSendOrder = createAsyncThunk(
  'order/fetchOrder',
  async (dataOrder) => {
    const responce = await sendRequest(`${API_URL_ORDER}/create`, "POST", dataOrder)
    return responce
  }
)


// для не авторизованих клієнтв , відміна замовлення 
export const actionPutOrderCanceled = ({ orderId, dataCancelOrder, token }) => async (dispatch) => {
  try {

    const responce = await sendRequest(`${API_URL_ORDER}/cancel/${orderId}`, "PUT", dataCancelOrder, token)
    if (responce.order.canceled) {
      dispatch(actionCanceledOrder(responce.order))
    }
  } catch (error) {
    console.error(error)
  }

}

// оновити статус на відправлено тільки Адмін
export const actionUpdateOrderStatusOnShipped = ({ token, orderId }) => async (dispatch) => {
  try {
    const responce = await sendRequest(`${API_URL_ORDER}/shipped/${orderId}`, "PUT", undefined, token)

    if (responce.order.status === 'shipped') {
      dispatch(actionUpdateOrderClientForAdmin(orderId))
    }
  } catch (error) {
    console.error(error.message)
  }
}
// видалити замовлення тільки Адмін
export const actionDeleteOrderForAdmin = ({ token, orderId }) => async (dispatch) => {
  try {
    const responce = await sendRequest(`${API_URL_ORDER}/${orderId}`, "DELETE", undefined, token)

    if (responce.result) {
      dispatch(actionUpdateOrderClientForAdmin(orderId))
    }
  } catch (error) {
    console.error(error.message)
  }
}


const orderSlice = createSlice({
  name: 'orders',
  initialState,
  reducers: {
    actionisLoading: (state, { payload }) => {
      state.loadingOrder = payload
    },
    actionCanceledOrder: (state, { payload }) => {
      state.orderClient = state.orderClient.map((item) => {
        if (item._id === payload._id) {
          return payload
        } else {
          return item
        }
      })
    },
    actionDeleteDataOrder: (state) => {

      state.dataResponce = {}
    },
    actionUpdateDataFromOrder: (state, { payload }) => {
      if (payload) {
        const { firstName, lastName, telephone, email } = payload;
        state.dataClientForDelivetyGoods.firstName = firstName
        state.dataClientForDelivetyGoods.lastName = lastName
        state.dataClientForDelivetyGoods.email = email
        state.dataClientForDelivetyGoods.phone = telephone
      } else {
        state.dataClientForDelivetyGoods = {
          firstName: '',
          lastName: '',
          phone: '',
          country: '',
          deliverDate: '',
          city: '',
          streetAddress: '',
          email: ''
        }
      }
    },
    actionUpdateOrderClientForAdmin: (state, { payload }) => {
      state.orderClientForAdmin = state.orderClientForAdmin.filter((item) => item._id !== payload)
    },
    actiionSaveDateFilterOrder: (state, { payload }) => {
      state.dateFilterOrderCient = payload
    },
    actiionSaveDateFilterStatictic: (state, { payload }) => {
      state.dateFilterStatistic = payload
    }

  },
  extraReducers: (builder) => {
    builder
      .addCase(actionSendOrderForAuthClient.fulfilled, (state, { payload }) => {
        state.dataResponce = payload
      })
      .addCase(actionGetAllOrdersForStatistic.fulfilled, (state, { payload }) => {

        if (payload.length) {
          const orders = {
            'notShipped': payload.filter((item) => item.status === 'not shipped' && !item.canceled).sort((a, b) => Date.parse(b.date) - Date.parse(a.date)),
            'shipped': payload.filter((item) => item.status === 'shipped' && !item.canceled).sort((a, b) => Date.parse(b.date) - Date.parse(a.date)),
            'canceled': payload.filter((item) => item.canceled).sort((a, b) => Date.parse(b.date) - Date.parse(a.date)),
          }

          state.statuses = {
            notShipped: orders['notShipped'].length,
            canceled: orders['canceled'].length,
            all: orders['notShipped'].length + orders['shipped'].length + orders['canceled'].length
          }

          let arrayOfPayments = [];

          orders['shipped'].forEach((order) => {
            arrayOfPayments.push(order.totalSum)
          })
          const totalOrders = orders['shipped'].length;
          let totalSumOfAllOrders = 0;
          orders['shipped'].forEach(order => {
            totalSumOfAllOrders += order.totalSum;
          });
          const averageCheck = totalSumOfAllOrders ? (totalSumOfAllOrders / totalOrders).toFixed(2) : 0
          state.statisticData = {
            minOrders: (arrayOfPayments.length) ? Math.min(...arrayOfPayments).toFixed(2) : 0,
            maxOrders: (arrayOfPayments.length) ? Math.max(...arrayOfPayments).toFixed(2) : 0,
            totalOrders,
            totalSumOfAllOrders: totalSumOfAllOrders.toFixed(2),
            averageCheck
          }


        } else {
          state.statisticData = { minOrders: 0, maxOrders: 0, totalOrders: 0, totalSumOfAllOrders: 0, averageCheck: 0 }
          state.statuses = {
            notShipped: 0,
            canceled: 0,
            all: 0
          }
        }
      })
      .addCase(actionGetAllOrders.fulfilled, (state, { payload }) => {
        state.orderClientForAdmin = payload.sort((a, b) => Date.parse(b.date) - Date.parse(a.date))
        state.loadingOrder = true
      })
      .addCase(actionSendOrder.fulfilled, (state, { payload }) => {
        state.dataResponce = payload;
      })
      .addCase(actionGetOrderClient.fulfilled, (state, { payload }) => {
        state.orderClient = payload.sort((a, b) => Date.parse(b.date) - Date.parse(a.date))
      })

  }
});

export const {
  actionDeleteDataOrder,
  actionUpdateDataFromOrder,
  actionUpdateOrderClientForAdmin,
  actionCanceledOrder,
  actionisLoading,
  actiionSaveDateFilterOrder,
  actiionSaveDateFilterStatictic,
} = orderSlice.actions

export default orderSlice.reducer;