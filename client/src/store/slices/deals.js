import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import { sendRequest } from "../../helpers/SendRequest.js"
import { API_URL_DEALS, API_URL_PRODUCTS } from '../../contans/API.js'
import { paginationLimitForDeals } from '../../contans/constans.js'


const initialState = {
    firstRender: true,
    loading: true,
    dealsList: [],
    dealsById: [],
    bestDeals: {},
    paginationDeals: {
        page: 1,
        limit: paginationLimitForDeals
    },
}

export const actionFetchDeals = createAsyncThunk(
    'deals/fetchDeals',
    async () => {
        const result = await sendRequest('/deals', "GET");
        return result
    }
)
export const actionFetchDealsbyId = createAsyncThunk(
    'deals/fetchDealsbyId',
    async (id) => {
        const result = await sendRequest(`${API_URL_DEALS}/${id}`, "GET");
        return result
    }
)
export const actionFetchBestDeals = createAsyncThunk(
    'deals/fetchBeastDeals',
    async ({ queryParams, pageDeals }) => {
        const responce = await sendRequest(`${API_URL_PRODUCTS}/filter?${queryParams}`, "POST", pageDeals)
        return responce
    }
)


const dealsSlice = createSlice({
    name: 'deals',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchDeals.fulfilled, (state, { payload }) => {

                if (payload.length) {

                    const countPrice = payload.map((item) => {
                        item.products = item.products.filter((product) => product.product.quantity)
                        return item
                    }).map((item) => {
                        item.currentPrice = item?.products.reduce((acum, currenValue) => {
                            return acum += currenValue?.product?.currentPrice
                        }, 0);
                        item.previousPrice = item?.products.reduce((acum, currenValue) => {
                            return acum += currenValue?.product?.previousPrice
                        }, 0);
                        return item;
                    });

                    state.dealsList = [...countPrice]


                    if (state.firstRender) {
                        state.loading = false
                        state.firstRender = false
                    }
                }

            })
            .addCase(actionFetchDealsbyId.fulfilled, (state, { payload }) => {
                state.dealsById.length = 0
                state.dealsById = [payload]

            })
            .addCase(actionFetchBestDeals.fulfilled, (state, { payload }) => {
                if (Object.keys(payload.results).length) {
                    const [recently, rated, selling, trending] = Object.keys(payload.results)
                    state.bestDeals = {
                        [recently]: payload.results[recently].filter((item) => item.quantity),
                        [rated]: payload.results[rated].filter((item) => item.quantity),
                        [selling]: payload.results[selling].filter((item) => item.quantity),
                        [trending]: payload.results[trending].filter((item) => item.quantity),
                    }
                }
            })
    }
})

export default dealsSlice.reducer


