import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { sendRequest } from '../../helpers/SendRequest.js';
import { API_URL_REVIEWS } from '../../contans/API.js';

const initialState = {
    allReviews: [],
    currentProductReviews: [],
    newReviewFormData: {
        comment: "",
    },
    newReview: {
        product: {},
        comment: '',
    },
}
export const fetchAllReviews = createAsyncThunk(
    'reviews/fetchReviews',
    async () => {
        const allReviews = await sendRequest(API_URL_REVIEWS, 'GET');
        return allReviews
    }
)

export const fetchProductReviews = createAsyncThunk(
    'reviews/fetchProductReviews',
    async (id) => {
        const currentProductReviews = await sendRequest(`${API_URL_REVIEWS}/product/${id}`, 'GET');
        return currentProductReviews
    }
)

export const actionUpdateProductReviews = ({ token, newComment }) => async (dispatch) => {
    try {
        const response = await sendRequest(API_URL_REVIEWS, "POST", newComment, token)
        if (response._id) {
            dispatch(updateProductReviews(response))
        }
    } catch (error) {
        console.error(error.message)
    }
}

export const actionUpdateProductReviewsAfterDelete = ({ token, _id }) => async (dispatch) => {
    try {
        const response = await sendRequest(`${API_URL_REVIEWS}/${_id}`, 'DELETE', undefined, token)
        if (response.deletedCommentInfo._id) {
            dispatch(updateProductReviewsAfterDelete(response.deletedCommentInfo))
        }
    } catch (error) {
        console.error(error.message)
    }
}

const reviewSlice = createSlice({
    name: 'reviews',
    initialState,
    reducers: {
        getAllReviews: (state, { payload }) => {

            state.allReviews = [...payload]

        },
        updateProductReviews: (state, { payload }) => {
            state.currentProductReviews = [payload, ...state.currentProductReviews]
        },
        updateProductReviewsAfterDelete: (state, { payload }) => {
            state.currentProductReviews = state.currentProductReviews.filter((item) => item._id !== payload._id)
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllReviews.fulfilled, (state, { payload }) => {
                state.allReviews = [...payload]
            })
            .addCase(fetchProductReviews.fulfilled, (state, { payload }) => {

                state.currentProductReviews = [...payload].reverse()

            })
    }
}

)

export const { getAllReviews: actionGetAllReviews, addReview: addReviewAction, updateProductReviews, updateProductReviewsAfterDelete } = reviewSlice.actions
export default reviewSlice.reducer