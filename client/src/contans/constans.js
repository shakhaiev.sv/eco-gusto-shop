export const priorityValueforBestDeals = ['Top Selling', 'Trending Products', 'Recently Added', 'Top Rated']
export const stickersNameforProduct = ['Sale', 'Hot', 'New',]

export const timeOfActionToken = 2 * 60 * 60 * 1000;
export const paginationLimit = 12
export const paginationLimitForDeals = 9
