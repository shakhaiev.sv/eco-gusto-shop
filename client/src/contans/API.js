export const API_URL_CATEGORIES = '/catalog'
export const API_URL_DEALS = '/deals'
export const API_URL_PRODUCTS = '/products'
export const API_URL_CART = '/cart'
export const API_URL_ORDER = '/orders'
export const API_URL_REVIEWS = '/comments'

export const HOST_DEV = 'http://localhost:4000/api'
export const HOST_PROD = 'https://gusto-shop-api.vercel.app/api'
