import React, { useContext, useEffect } from "react";
import Logo from '/src/pictures/svg/header/logo.svg?react';
import Heart from '/src/pictures/svg/header/heart.svg?react';
import Cart from '/src/pictures/svg/header/cart.svg?react';
import Squares from '/src/pictures/svg/header/squares.svg?react';
import Fire from '/src/pictures/svg/header/fire.svg?react';
import Headphones from '/src/pictures/svg/header/headphones.svg?react';
import Account from '/src/pictures/svg/header/account.svg?react'
import './Header.scss';
import Container from "../../layout/container/Container";
import Button from "../Button/Button";
import {
    selectorListFavorite,
    selectorLengthListCart,
    selectorResponceAuth,
    selectorUserFirstName,
    selectorToken,
    selectorUserLogin,
    selectorDataClient,
} from '../../store/selectors/selectors.js'
import { useSelector, useDispatch } from "react-redux";
import cn from 'classnames'
import { ModalContext } from '../../context/ModalContext.jsx'
import ModalAuthorization from "../ModalAuthorization/ModalAuthorization.jsx";
import Search from "../Search/Search.jsx";
import { fetchUserData } from '../../store/slices/userSlice.js';
import { useMedia } from '../../hooks/UseMedia.js'
import { actionUpdateDataFromOrder } from '../../store/slices/order.js'

const Header = () => {
    const responceAuthorization = useSelector(selectorResponceAuth)
    const { showModalAuth,
        fnShowModalAuth,
        showMenuMobile,
        fnshowMenuMobile,
    } = useContext(ModalContext)
    const listFavorite = useSelector(selectorListFavorite)
    const lengthListCart = useSelector(selectorLengthListCart)
    const firstName = useSelector(selectorUserFirstName)
    const token = useSelector(selectorToken)
    const login = useSelector(selectorUserLogin)
    const dataClientAuth = useSelector(selectorDataClient)
    const dispath = useDispatch()
    const isMediaQuery = useMedia("(max-width:958px)")

    const handleShowModal = () => {
        fnShowModalAuth()
    }

    const handleShowMenuMobile = () => {
        fnshowMenuMobile()
    }

    useEffect(() => {
        if (Object.values(dataClientAuth).length && token) {
            dispath(actionUpdateDataFromOrder(dataClientAuth))
        }

    }, [dataClientAuth])

    useEffect(() => {
        if ((token || responceAuthorization.token) && login && !firstName) {
            dispath(fetchUserData())
        }
    }, [firstName, token, responceAuthorization.token, login])

    return (
        <header className="g-header ">
            <Container>
                <div className="header-base ">
                    <div className="header-container ">
                        <Button to='/' className="header-logo">
                            <Logo />
                            <div className="header-logo__text">
                                <span className="header-logo__text-gusto" >Gusto</span>
                                <span className="header-logo__text-shop">supermarket</span>
                            </div>
                        </Button>
                        <Search />
                        <div className="header-btn-wrapper">
                            <Button
                                to="/cart"
                                className={cn("header-cart-btn", { 'count-cart': lengthListCart })}
                                data-count={lengthListCart}
                            >
                                <Cart />
                                <p>Cart</p>
                            </Button>
                            <Button
                                to="/favorite"
                                className={cn("header-favorite-btn", { 'count-favorite': listFavorite?.length })}
                                data-count={listFavorite.length}
                            >
                                <Heart />
                                <p>Favorite</p>
                            </Button>
                            {!firstName ? (
                                <Button type='button'
                                    className="header-account-btn"
                                    onClick={handleShowModal}
                                >
                                    <Account />
                                    <p>Log in</p>
                                </Button >
                            ) : (
                                <Button to="/account" className="header-account-btn">
                                    <Account />
                                    <p>{firstName}</p>
                                </Button >
                            )}
                        </div>
                        <Button
                            onClick={handleShowMenuMobile}
                            className={cn('btn-burg', { 'btn-burg__open': showMenuMobile })}
                        />
                    </div>
                </div>

                <div className="header-bottom">
                    <div className="header-container ">
                        <nav className="header-bottom-nav">
                            <div className="category-select">
                                <Squares />
                                <Button to='/products' className='category-select__link'>Browse All Categories</Button>
                            </div>
                            <div className="nav-wrapper">
                                <Button to="/deals"><Fire /><span>Deals</span></Button>
                                <Button to='/blog' >Blog</Button>
                                <Button to='/about' >About</Button>
                                <Button to='/contact'> Contact</Button>
                            </div>
                        </nav>

                        <div className="support">
                            <a href="tel:+1900-888">
                                <Headphones />
                            </a>
                        </div>
                    </div>
                </div>

                {isMediaQuery && <div className={cn("header-mobile", { 'header-mobile--active': showMenuMobile })}>
                    <ul className="header-mobile__nav">
                        <li className="header-mobile__item">
                            <Button
                                to="/cart"
                                className={cn("header-cart-btn", { 'count-cart': lengthListCart })}
                                data-count={lengthListCart}
                                onClick={handleShowMenuMobile}
                            >
                                <Cart />
                                <p>Cart</p>
                            </Button>
                        </li>
                        <li className="header-mobile__item">
                            <Button
                                to="/favorite"
                                className={cn("header-favorite-btn", { 'count-favorite': listFavorite?.length })}
                                data-count={listFavorite.length}
                                onClick={handleShowMenuMobile}
                            >
                                <Heart />
                                <p>Favorite</p>
                            </Button>
                        </li>
                        <li className="header-mobile__item">
                            {!firstName ? (
                                <Button type='button'
                                    className="header-account-btn"
                                    onClick={() => {
                                        handleShowModal()
                                        handleShowMenuMobile()
                                    }}
                                >
                                    <Account />
                                    <p>Log in</p>
                                </Button >
                            ) : (
                                <Button to="/account" className="header-account-btn">
                                    <Account />
                                    <p>{firstName}</p>
                                </Button >
                            )}
                        </li>
                        <li className="header-mobile__item">
                            <Button to='/products' className='header-mobile__link' onClick={handleShowMenuMobile} >Browse All Categories</Button>
                        </li>
                        <li className="header-mobile__item">
                            <Button className='header-mobile__link' to="/deals" onClick={handleShowMenuMobile}><Fire /><span>Deals</span></Button>
                        </li>
                        <li className="header-mobile__item">
                            <Button className='header-mobile__link' to='/blog' onClick={handleShowMenuMobile} >Blog</Button>
                        </li>
                        <li className="header-mobile__item">
                            <Button className='header-mobile__link' to='/about' onClick={handleShowMenuMobile}  > About</Button>
                        </li>
                        <li className="header-mobile__item">
                            <Button className='header-mobile__link' to='/contact' onClick={handleShowMenuMobile} > Contact</Button>
                        </li>
                    </ul>

                </div>}

                {showModalAuth && <ModalAuthorization />}
            </Container>
        </header>
    )
}

export default Header
