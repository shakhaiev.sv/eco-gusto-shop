import React, { useEffect, useState } from 'react';
import PropTypes from "prop-types";
import Button from "../Button/Button.jsx";
import Stars from 'reactjs-star-rating';
import CartIcon from "../../pictures/svg/deals/cart.svg?react";
import "./CardDeals.scss"
import { useDispatch, useSelector } from 'react-redux'
import { actionAddCardToCartList, actionAllLengthCoodsOnCart } from '../../store/slices/cart.js'
import { selectorToken, selectorListCart } from '../../store/selectors/selectors.js'
import { actionAddProductOnDealsForAuthClient } from '../../store/slices/cart.js'
const CardDealOfTheDay = (props) => {

    const [productsDeals, setProductsDeals] = useState([])
    const {
        dataDeals
    } = props


    const token = useSelector(selectorToken)
    const listCart = useSelector(selectorListCart)
    const {
        _id,
        name,
        imgUrl,
        reviews,
        categoryName,
        currentPrice,
        previousPrice,
        products,
    } = dataDeals
    const dispatch = useDispatch();

    useEffect(() => {
        setProductsDeals(products)
    }, [])

    const handleAddProductsToCartinDeals = () => {
        if (token) {
            const productsDealsForReq = listCart.length ? productsDeals.filter((itemDeals) => {
                if (!listCart.some((item) => item._id === itemDeals.product._id)) {
                    return itemDeals
                }
            }).map(({ product }) => (
                { 'product': product._id, 'cartQuantity': 1 }
            )) : productsDeals.map(({ product }) => (
                { 'product': product._id, 'cartQuantity': 1 }
            ))
            const dataDeals = {
                products: [
                    ...listCart.map(({ cartQuantity, _id }) => {
                        if (productsDeals.some((item) => item.product._id === _id)) {
                            return {
                                'product': _id,
                                'cartQuantity': cartQuantity += 1
                            }
                        } else {
                            return {
                                'product': _id,
                                cartQuantity
                            }
                        }
                    }),
                    ...productsDealsForReq
                ]
            }
            dispatch(actionAddProductOnDealsForAuthClient({ token, dataDeals }))
        } else {
            productsDeals.forEach((item) => dispatch(actionAddCardToCartList({ cartQuantity: item.cartQuantity, ...item.product })))
            dispatch(actionAllLengthCoodsOnCart())
        }
    }

    return (
        <>
            <Button to={`/deals/${_id}`} className='sect-deals__card-link'>
                <img className='sect-deals__card-img' src={imgUrl} alt={name} />
            </Button>

            <div className='sect-deals__block-info'>
                <div className='sect-deals__card-data'>
                    <Button to={`/deals/${_id}`} className='sect-deals__card-title'>{name}</Button>
                    <div className='sect-deals__card-reviews'>
                        <Stars
                            className='starIcon'
                            maxRating='5'
                            size={20}
                            defaultRating={reviews}
                            color='#ffd700'
                            readOnly={true}
                            showLabel={false}
                        />
                        <span className='sect-deals__card-rating'>{reviews?.toFixed(1)}</span></div>
                    <p className='sect-deals__card-categories' >Category: <span>{categoryName}</span></p>
                    <div className="sect-deals__card-price">
                        <p className="sect-deals__card-currentprice">{`$${currentPrice.toFixed(2)}`}</p>
                        <del className="sect-deals__card-previousprice">{`$${previousPrice.toFixed(2)}`}</del>
                        <Button
                            type="button"
                            className="sect-deals__btn-add"
                            onClick={handleAddProductsToCartinDeals}
                        >
                            <CartIcon />
                            <span>Add</span>
                        </Button>
                    </div>
                </div>
            </div>
        </>
    )
};

CardDealOfTheDay.propTypes = {
    dataDeals: PropTypes.object
}

export default CardDealOfTheDay;