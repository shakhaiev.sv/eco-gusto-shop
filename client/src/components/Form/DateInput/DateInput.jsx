import React from 'react';
import PropTypes from 'prop-types';
import { Field, ErrorMessage, useFormikContext } from 'formik'
import cn from 'classnames'

const DateInput = (props) => {
    const { values, setFieldValue } = useFormikContext();
    const {
        children,
        name,
        className,
        classNameLabel,
        error,
        min,
        ...restProps
    } = props


    return (
        <label className={cn(classNameLabel, { 'has-valid': error })}>
            {children}
            <Field
                name={name}
                type="date"
                placeholder="DD/MM/YYYY"
                className={cn('form-input', className, { 'has-valid': error })}
                onChange={(e) => setFieldValue(name, e.target.value)} // Оновлення значення
                min={min}
                {...restProps}
            />
            <ErrorMessage name={name} className='error-mes' />
        </label>
    );
};

DateInput.propTypes = {};

export default DateInput;