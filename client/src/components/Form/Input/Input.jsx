import React from 'react';
import PropTypes from 'prop-types';
import './Input.scss'
import { Field, ErrorMessage } from 'formik'
import cn from 'classnames'
import { InputMask } from '@react-input/mask'

const Input = (props) => {
    const {
        type,
        placeholder,
        label,
        name,
        className,
        error,
        component,
        children,
        classNameLabel,
        ...restProps
    } = props

    return (
        <label className={cn(classNameLabel, { 'has-valid': error })}>
            {children}
            {component ?
                <InputMask
                    replacement={{ _: /\d/ }}
                    type={type}
                    name={name}
                    placeholder={placeholder}
                    className={cn('form-input', className,)}
                    {...restProps}

                /> : <Field
                    type={type}
                    placeholder={placeholder}
                    name={name}
                    className={cn('form-input', className, { 'has-valid': error })}
                    {...restProps}
                />
            }
            {label && <span>{label}</span>}
            <ErrorMessage name={name} className='error-mes error' component='div' />
        </label>
    );
};

Input.propTypes = {};

export default Input;