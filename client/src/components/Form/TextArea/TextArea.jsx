import React from 'react';
import './TextArea.scss'
import { ErrorMessage, Field } from 'formik';
import cn from 'classnames'
const TextArea = (props) => {

    const {
        name,
        placeholder,
        error,
        className
    } = props
    return (
        <label>
            <Field
                as='textarea'
                className={cn('form-textarea', className, { 'has-valid': error })}
                name={name}
                placeholder={placeholder}
            />

            <ErrorMessage name={name} className='error-mes' />
        </label>
    );
};

TextArea.propTypes = {};

export default TextArea;