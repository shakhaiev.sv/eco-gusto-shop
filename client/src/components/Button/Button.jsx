import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './Button.scss';
import { Link } from 'react-router-dom';

const Button = (props) => {
    const {
        onClick,
        children,
        type,
        className,
        to,
        product,
        ...restProps
    } = props;


    let Element = to ? Link : 'button';

    return (
        <>
            {
                type ? <Element
                    type={type}
                    className={cn('btn', className)}
                    onClick={onClick}
                    to={to}
                    {...restProps}
                >
                    {children}
                </Element > : <Element
                    className={cn(className)}
                    onClick={onClick}
                    to={to}
                    {...restProps}
                >
                    {children}
                </Element>
            }
        </>
    );
};

Button.propTypes = {
    children: PropTypes.any,
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    to: PropTypes.string,
    product: PropTypes.object,
};

export default Button;