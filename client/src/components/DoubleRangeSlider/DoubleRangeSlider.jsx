import React from 'react';
import './DoubleRangeSlider.scss';

const DoubleRangeSlider = ({ minPrice,
    setMinPrice,
    maxPrice,
    setMaxPrice }) => {

    const min = 0;
    const max = 3000;
    const priceGap = 1;

    const handleMinInputChange = (e) => {
        let value = parseInt(e.target.value);
        if (isNaN(value)) {
            value = min;
        }

        if (value > maxPrice - priceGap) {
            value = maxPrice - priceGap;
        }

        setMinPrice(value);
    };

    const handleMaxInputChange = (e) => {
        let value = parseInt(e.target.value);
        if (isNaN(value)) {
            value = max;
        }

        if (value < minPrice + priceGap) {
            value = minPrice + priceGap;
        }

        setMaxPrice(value);
    };

    const handleRangeInputChange = (e, type) => {
        const value = parseInt(e.target.value);

        if (type === 'min') {
            if (value <= maxPrice - priceGap) {
                setMinPrice(value);
            }
        } else {
            if (value >= minPrice + priceGap) {
                setMaxPrice(value);
            }
        }
    };

    return (
        <div className="slider-container">
            <div className="range-input">
                <input
                    type="range"
                    className="min-range"
                    min={min}
                    max={max}
                    value={minPrice}
                    onChange={(e) => handleRangeInputChange(e, 'min')}
                />
                <input
                    type="range"
                    className="max-range"
                    min={min}
                    max={max}
                    value={maxPrice}
                    onChange={(e) => handleRangeInputChange(e, 'max')}
                />
            </div>
            <div className="price-input-wrapper">
                <div className="price-inputs">
                    <div className="price-input-min">
                        <p>From: <span>$</span></p>
                        <input
                            type="number"
                            className="min-input"
                            min={min}
                            max={max}
                            value={minPrice}
                            onBlur={handleMinInputChange}
                            onChange={(e) => setMinPrice(parseInt(e.target.value))}
                        />
                    </div>
                    <div className="price-input-max">
                        <p>To: <span>$</span></p>
                        <input
                            type="number"
                            className="max-input"
                            min={min}
                            max={max}
                            value={maxPrice}
                            onBlur={handleMaxInputChange}
                            onChange={(e) => setMaxPrice(parseInt(e.target.value))}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default DoubleRangeSlider;
