import React from 'react';
import './Footer.scss';
import Container from '../../layout/container/Container.jsx'
import Button from '../Button/Button.jsx';
import Logo from '../../pictures/svg/header/logo.svg?react';
import AdressLogo from '../../pictures/svg/footer/footer-adress.svg?react';
import CallLogo from '../../pictures/svg/footer/footer-call.svg?react';
import EmailLogo from '../../pictures/svg/footer/footer-email.svg?react';
import HoursLogo from '../../pictures/svg/footer/footer-hours.svg?react';
import Inst from '../../pictures/svg/footer/inst.svg?react';
import Facebook from '../../pictures/svg/footer/facebook.svg?react';
import Twitter from '../../pictures/svg/footer/twitter.svg?react';
import Pinterest from '../../pictures/svg/footer/pinterest.svg?react';
import YouTube from '../../pictures/svg/footer/youtube.svg?react';
import PhoneIcon from '../../pictures/svg/footer/phone-icon.svg?react'

const Footershop = () => {
  return (
    <footer className="footer">
      <Container>

        <div className="footer__container">
          <div className="footer__container-block1">
            <div className="footer__section footer__section--logo">
              <Button to='/' className="footer-logo">
                <Logo />
                <div className="footer-logo__text">
                  <span className="footer-logo__text-gusto">Gusto</span>
                  <span className="footer-logo__text-shop">supermarket</span>
                </div>
              </Button>
            </div>

            <div className="footer__section footer__section--info">

              <div className="footer__contacts">

                <div className="footer__contacts-item">

                  <p><AdressLogo /><span>Address:</span> 5171 W Campbell Ave undefined Kent, Utah 53127 United States</p>
                </div>

                <div className="footer__contacts-item">

                  <p><CallLogo /><span>Call Us:</span> <a href="tel:+380505400224">+380(50)-540-02-24</a></p>
                </div>

                <div className="footer__contacts-item">

                  <p><EmailLogo /><span>Email:</span> <a href="mailto:sales@Eco.com">sales@Eco.com</a></p>
                </div>

                <div className="footer__contacts-item">

                  <p><HoursLogo /><span>Hours:</span> 10:00 - 18:00, Mon - Sat</p>
                </div>
              </div>
            </div>
          </div>
          <div className="footer__container-block2">
            <ul className="footer__section footer__section--links">
              <li className='footer__link-items'><Button to='/about'>About Us</Button></li>
              <li className='footer__link-items'><Button to='/products'>Our Products</Button></li>
              <li className='footer__link-items'><Button to='/contact'>Contact Us</Button></li>
              <li className='footer__link-items'><Button to='/cart'>View Cart</Button></li>
              <li className='footer__link-items'><Button to='/favorite'>My Wishlist</Button></li>
              <li className='footer__link-items'><Button to='/deals'>Deals</Button></li>
            </ul>
            <div className="footer__section footer__section--install">
              <h4 className='footer__install'>Install App</h4>
              <div className="footer__link-app-wrapper">
                <a className='footer__link-app' href="#"><img
                  src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721854211/Google_d6x8zi.png"
                  alt="Google Play" /></a>
                <a className='footer__link-app' href="#"><img
                  src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721854210/Apple_qogi1j.png"
                  alt="App Store" /></a>
              </div>
              <p className='footer__par--install'>Secured Payment Gateways</p>
            </div>
          </div>
        </div>
        <div className="footer__bottom">

          <p className="footer__copyright">
            © 2022, <span>Gusto</span> - HTML Ecommerce Template
            All rights reserved
          </p>

          <div className="footer__phonenumbers">
            <div className="number">
              <PhoneIcon />
              <div className="tel">
                <a href="tel:1900 - 6666">1900 - 6666</a>
                <p>Working 8:00 - 22:00</p>
              </div>
            </div>

            <div className="number">
              <PhoneIcon />
              <div className="tel">
                <a href="tel:1900 - 8888">1900 - 8888</a>
                <p>24/7 Support Center</p>
              </div>
            </div>
          </div>

          <div className="footer__media">
            <div className="footer__media-links">
              <h5>Follow Us</h5>

              <div className='links-list'>
                <div className="icon-container"><Facebook /></div>
                <div className="icon-container"><Twitter /></div>
                <div className="icon-container"><Inst /></div>
                <div className="icon-container"><Pinterest /></div>
                <div className="icon-container"><YouTube /></div>
              </div>
            </div>
            <p>Up to 15% discount on your first subscribe</p>
          </div>
        </div>

      </Container>
    </footer>
  );
};

export default Footershop;