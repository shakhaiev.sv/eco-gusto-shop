import React from 'react';
import Button from '../Button/Button.jsx'
import cn from 'classnames'
import Stars from 'reactjs-star-rating'
import './MiniCard.scss'


const MiniCard = (props) => {
    const { dataProduct, className, linkNot } = props
    const { _id, imageUrls, name, currentPrice, previousPrice, reviews } = dataProduct
    return (
        <>
            {
                linkNot ? <div className={cn('mini-card__link', className)}>
                    <img className='mini-card__img' src={imageUrls[0]} alt={name} />
                    <div className='mini-card__info'>
                        <h3 className='mini-card__title'>{name}</h3>
                        <div className='mini-card__reviews'>
                            <Stars
                                className='starIcon'
                                maxRating='5'
                                size={20}
                                defaultRating={reviews}
                                color='#ffd700'
                                readOnly={true}
                                showLabel={false}
                            />
                            <span>{reviews?.toFixed(1)}</span></div>
                        <div className='mini-card__price'>
                            {currentPrice && <p className="mini-card__current">{`$${currentPrice.toFixed(2)}`}</p>}
                            {previousPrice && <del className="mini-card__previous">{`$${previousPrice.toFixed(2)}`}</del>}
                        </div>
                    </div>
                </div > : < Button
                    to={`/product/${_id}`
                    }
                    className={cn('mini-card__link', className, { 'non-available': !dataProduct.quantity })}
                >
                    <img className='mini-card__img' src={imageUrls[0]} alt={name} />
                    <div className='mini-card__info'>
                        <h3 className='mini-card__title'>{name}</h3>
                        <div className='mini-card__reviews'>
                            <Stars
                                className='starIcon'
                                maxRating='5'
                                size={20}
                                defaultRating={reviews}
                                color='#ffd700'
                                readOnly={true}
                                showLabel={false}
                            />
                            <span className='mini-card__rating'>{reviews?.toFixed(1)}</span></div>
                        <div className='mini-card__price'>
                            {currentPrice && <p className="mini-card__current">{`$${currentPrice.toFixed(2)}`}</p>}
                            {previousPrice && <del className="mini-card__previous">{`$${previousPrice.toFixed(2)}`}</del>}
                        </div>

                    </div>
                </Button>
            }
        </>
    );
};

MiniCard.propTypes = {};

export default MiniCard