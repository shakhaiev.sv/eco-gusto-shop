import React, { useEffect, useState, useCallback } from "react";
import SearchIcon from '/src/pictures/svg/header/search.svg?react';
import { sendRequest } from "../../helpers/SendRequest";
import MiniCard from "../MiniCard/MiniCard";
import debounce from 'lodash/debounce';

import './Search.scss'

const Search = () => {
    const [query, setQuery] = useState('');
    const [items, setItems] = useState([]);
    const [showDropdown, setShowDropdown] = useState(false);

    const handleInputChange = (e) => {
        setQuery(e.target.value);
    }

    const handleBlur = (e) => {
        if (!e.currentTarget.contains(e.relatedTarget)) {
            hideDropdown()
        }
    }

    const hideDropdown = () => {
        setShowDropdown(false);
    };


    const fetchItems = useCallback(
        debounce(async (query) => {
            if (query.length > 1) {
                try {
                    const response = await sendRequest('/products/search', 'POST', { search: query });
                    if (response && Array.isArray(response)) {
                        setItems(response);
                        setShowDropdown(true);
                    } else {
                        setItems([]);
                        setShowDropdown(false);
                    }
                } catch (error) {
                    setItems([]);
                    setShowDropdown(false);
                }
            } else {
                setShowDropdown(false);
            }
        }, 1000), []
    )

    useEffect(() => {
        fetchItems(query)
        return () => {
            fetchItems.cancel();
        };
    }, [query, fetchItems]);

    const handleScroll = () => {
        hideDropdown();
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);


    return (
        <>
            <div className="search" onBlur={handleBlur}>
                <input type="search" placeholder="Search for items..." value={query} onChange={handleInputChange} />
                <SearchIcon />{showDropdown && (
                    <div className="dropdown">
                        {items.map((item, index) => (
                            <MiniCard key={index} dataProduct={item} />
                        ))}
                    </div>
                )}
            </div>


        </>
    );

}

export default Search;
