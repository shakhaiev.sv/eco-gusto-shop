
import React, { useEffect, useState, useContext } from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Button from '../Button/Button.jsx';
import FavoritesIcon from '../../pictures/svg/product/favorite.svg?react';
import Stars from 'reactjs-star-rating';
import CartIcon from '../../pictures/svg/deals/cart.svg?react';
import Edit from '../../pictures/svg/product/edit.svg?react'
import { useDispatch, useSelector } from 'react-redux';
import {
    actionAddFavoriteList,
    actionAddProductToFavoritesForAuthClient,
    actionDeleteProductFromFavoritesAuthClient,
    actionSetListFavoriteFromLocalStorage
} from '../../store/slices/favorite.js';
import {
    selectorAdmin,
    selectorListFavorite,
    selectorToken
} from '../../store/selectors/selectors.js';
import {
    actionAddCardToCartList,
    actionAllLengthCoodsOnCart,
    actionAddProductToCartForAuthClient
} from '../../store/slices/cart.js';
import SnackbarAlert from '../SnackbarAlert/SnackbarAlert.jsx';
import './Product.scss';
import { ModalContext } from '../../context/ModalContext.jsx';


const Product = (props) => {
    const { fnShowModalCreateProducts, } = useContext(ModalContext)
    const [openSnackbarAlert, setOpenSnackbarAlert] = useState(false);
    const dispatch = useDispatch();
    const { dataProduct, isEdit } = props;
    const { _id, imageUrls, name, reviews, categoryName, currentPrice, previousPrice, sticker } = dataProduct;
    const [isFavorite, setIsFavorite] = useState(false)
    const Admin = useSelector(selectorAdmin)
    const token = useSelector(selectorToken);
    const listFavorite = useSelector(selectorListFavorite) || []; // Перевірка на пустий масив
    useEffect(() => {
        dispatch(actionSetListFavoriteFromLocalStorage());
    }, [dispatch]);

    useEffect(() => {
        setIsFavorite(listFavorite.some(item => item['_id'] === _id))
    }, [listFavorite, _id])

    const handleAddCardToFavorite = async () => {
        if (token) {
            if (isFavorite) {
                dispatch(actionDeleteProductFromFavoritesAuthClient({ token, idProduct: _id }));
            } else {

                dispatch(actionAddProductToFavoritesForAuthClient({ token, idProduct: _id }));
            }
        } else {
            // Локальне зберігання для неавторизованих користувачів
            dispatch(actionAddFavoriteList(dataProduct));
        }
    }

    const handleAddCardtoCart = () => {
        if (token) {
            dispatch(actionAddProductToCartForAuthClient({ token, idProduct: _id }));
        } else {
            dispatch(actionAddCardToCartList(dataProduct));
            dispatch(actionAllLengthCoodsOnCart());
        }
        setOpenSnackbarAlert(true);
    };

    return (
        <div className='card-product__wrap'>
            <Button
                className='card-product__img-wrap'
                to={`/product/${_id}`}>
                <img className='card-product__img' src={imageUrls[0]} alt={name} />
            </Button>
            <Button to={`/product/${_id}`} className='card-product__link'>{name}</Button>
            <div className='card-product__reviews'>
                <Stars
                    className='starIcon'
                    maxRating='5'
                    size={20}
                    defaultRating={reviews}
                    color='#ffd700'
                    readOnly={true}
                    showLabel={false}
                />
                <span className='card-product__rating'>{reviews?.toFixed(1)}</span></div>
            <p className='card-product__category'>Category: <span>{categoryName}</span></p>
            <div className="card-product__card-price">
                <p className="card-product__card-currentprice">${currentPrice?.toFixed(2)}</p>
                {previousPrice && <del className="sect-deals__card-previousprice">${previousPrice.toFixed(2)}</del>}
                <Button
                    type="button"
                    className="card-product__btn-add"
                    onClick={handleAddCardtoCart}
                >
                    <CartIcon />
                    <span>Add</span>
                </Button>

            </div>
            <p className={cn('card-product__sticker', { 'sale': (sticker === 'Sale'), 'new': (sticker === 'New'), 'hot': (sticker === 'Hot') })}>
                {sticker}
            </p>
            <div className='card-product__btn-wrapper'>
                <Button
                    type='button'
                    className={cn('card-product__btn-favorite', {
                        'is-favorite': Array.isArray(listFavorite) && listFavorite.some((item) => item['_id'] === _id)
                    })}
                    onClick={handleAddCardToFavorite}
                >
                    <FavoritesIcon />
                </Button>
                {Admin && isEdit &&
                    <Button
                        type='button'
                        className='card-product__btn-edit'
                        onClick={() => { fnShowModalCreateProducts(dataProduct) }}
                    >
                        <Edit />
                    </Button>
                }

            </div>


            {
                !dataProduct.quantity && <Button
                    className='non-available'
                    to={`/product/${_id}`}
                >
                </Button>
            }
            {openSnackbarAlert && <SnackbarAlert open={openSnackbarAlert} setOpen={setOpenSnackbarAlert} />}
        </div >
    );
};

Product.propTypes = {
    dataProduct: PropTypes.object
};

export default Product;
