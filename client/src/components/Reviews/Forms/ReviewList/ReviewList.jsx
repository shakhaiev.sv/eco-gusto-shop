import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchProductReviews, actionUpdateProductReviewsAfterDelete } from '../../../../store/slices/reviewSlice.js';
import {
    selectorAdmin,
    selectorDataClient,
    selectorCurrentProduct,
    selectorProductReviews,
    selectorToken
} from '../../../../store/selectors/selectors.js';
import Button from '../../../Button/Button.jsx';
import UserIcon from '../../../../pictures/svg/header/account.svg?react';

import './ReviewList.scss'

const ReviewList = () => {

    const dispatch = useDispatch();
    const token = useSelector(selectorToken);
    const user = useSelector(selectorDataClient);
    const admin = useSelector(selectorAdmin)
    const product = useSelector(selectorCurrentProduct);
    const productReviews = useSelector(selectorProductReviews);

    const { _id } = product;

    useEffect(() => {
        dispatch(fetchProductReviews(_id));
    }, []);

    return (
        <div>
            {productReviews.length === 0 ? (
                <p>No reviews yet. Be first who will describe this product.</p>
            ) : (
                <ul className='reviews__list-items'>
                    {productReviews.map((review, index) => (
                        <li key={index}>
                            <div className='reviews__list-item'>
                                <div className='reviews__user-data'>
                                    <div className='reviews__user-icon'>
                                        <UserIcon />
                                    </div>
                                    <strong className='reviews__user-name'>{review.customer.firstName}</strong>
                                </div>
                                <div className='reviews__user-comment'>
                                    {review.content}
                                    {
                                        (token) && ((user.login === review.customer.login) || (admin)) ?
                                            <Button
                                                type='submit'
                                                className='button__delete-review'
                                                onClick={() => {
                                                    const { _id } = review;
                                                    dispatch(actionUpdateProductReviewsAfterDelete({
                                                        token,
                                                        _id,
                                                    }))
                                                }}
                                            >
                                                Delete
                                            </Button> : null
                                    }
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default ReviewList;