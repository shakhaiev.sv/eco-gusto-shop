import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import {
    selectorCurrentProduct,
    selectorNewReview,
    selectorToken,
    selectorUserFirstName,
} from '../../../../store/selectors/selectors.js';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import validation from './validation/validation.js';
import cn from 'classnames'
import Button from '../../../Button/Button.jsx';
import './ReviewForm.scss'
import { actionUpdateProductReviews } from "../../../../store/slices/reviewSlice.js";
import { ModalContext } from "../../../../context/ModalContext.jsx";

const ReviewForm = () => {

    const dispatch = useDispatch();
    const token = useSelector(selectorToken);
    const userName = useSelector(selectorUserFirstName)
    const product = useSelector(selectorCurrentProduct);
    const { _id } = product;
    const reviewFormData = useSelector(selectorNewReview)
    const { fnShowModalAuth } = useContext(ModalContext)

    const handleShowModal = () => {
        fnShowModalAuth()
    }

    return (
        <>
            {(!token) ? <span className='review__cta'>Please<Button onClick={handleShowModal} className='button__log-in'>Log in</Button>to be able leave a product review.</span> :
                <Formik
                    initialValues={reviewFormData}
                    validationSchema={validation}
                    onSubmit={(values, { resetForm }) => {
                        if (token) {
                            dispatch(actionUpdateProductReviews({
                                token, newComment: {
                                    product: _id,
                                    content: values.comment,
                                }
                            }));
                            resetForm(reviewFormData);
                        }
                    }}
                >
                    {({ errors, touched }) => (
                        <Form className='review-form'>
                            {
                                <div className='review-form__user_cta'>
                                    <p><strong>{userName}</strong>,</p>
                                    <span>Leave your comment about this product</span>
                                </div>
                            }
                            <Field

                                className='review-form__comment'
                                as="textarea"
                                name="comment"
                                placeholder="Enter your comment"
                                error={errors.comment && touched.comment}

                            />
                            <ErrorMessage name="comment" component="div" className="error-message" />
                            <Button type="submit" className={cn('review-form__submit-button', { 'active': (token) })}><span>Submit Review</span></Button>
                        </Form>
                    )}
                </Formik>
            }
        </>
    );
};

export default ReviewForm;