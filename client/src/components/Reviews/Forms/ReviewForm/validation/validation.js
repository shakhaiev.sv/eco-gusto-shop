import * as yup from "yup";

const validation = yup.object({

    comment: yup
        .string()
        .required('Comment is required')
        .min(10, 'Comment must be at least 10 characters')
        .max(500, 'Comment maximum length is 500 characters'),
});

export default validation