import React from 'react';
import ReviewForm from './Forms/ReviewForm/ReviewForm.jsx';
import ReviewList from './Forms/ReviewList/ReviewList.jsx';
import './Reviews.scss'



const Reviews = () => {

    return (
        <div className="reviews">
            <ReviewForm />
            <ReviewList />
        </div>
    );
};

export default Reviews;