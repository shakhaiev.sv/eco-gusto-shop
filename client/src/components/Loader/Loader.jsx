import React from 'react';

import './Loader.scss'
import Logo from '../../pictures/svg/header/logo.svg?react';

const Loader = () => {
    return (
        <div className='pop-up-wrapper'>
            <div className='pop-up-content'>
                <div className="pop-up-logo">
                    <Logo />
                    <div className="pop-up-logo__text">
                        <span className="pop-up-logo__text-gusto">Gusto</span>
                        <span className="pop-up-logo__text-shop">supermarket</span>
                    </div>
                </div>
                <div className='pop-up-wrap'>
                    <span className="pop-up"></span>
                </div>

            </div>
        </div>
    );
};

Loader.propTypes = {};

export default Loader;