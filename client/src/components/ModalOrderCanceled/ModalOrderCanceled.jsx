import React, { useContext, } from 'react';
import ModalBase from '../ModalBase/ModalBase';
import { ModalContext } from '../../context/ModalContext.jsx';
import Button from '../Button/Button';
import './ModalOrderCanceled.scss'
import { useDispatch, useSelector } from 'react-redux'
import { actionPutOrderCanceled } from '../../store/slices/order.js'
import { selectorToken, selectorDataClient } from '../../store/selectors/selectors.js'

const ModalOrderCanceled = (props) => {
    const { orderCanceled } = props
    const token = useSelector(selectorToken)
    const dataClient = useSelector(selectorDataClient)

    const dispatch = useDispatch();
    const { fnShowModalOrderCanceled } = useContext(ModalContext)

    const handleCanceledOrder = () => {
        dispatch(actionPutOrderCanceled({
            token, orderId: orderCanceled._id, dataCancelOrder: {
                email: dataClient.email,
                letterSubject: `Gusto supermarket? Your order №${orderCanceled.numberNo} canceled`,
                letterHtml: `<div style="font-family: Arial, sans-serif; padding: 20px;">
                <p style="color: #555;">Dear ${orderCanceled.customerId.firstName},</p>
                <p style="color: #555;">You have canceled your order</p>`
            }
        }))
        fnShowModalOrderCanceled()
    }

    return (
        <ModalBase
            headerTitle='Order'
            onClose={() => { fnShowModalOrderCanceled() }}
            classNameModal='modal-order__canceled'
        >
            <h1 className='modal-order__canceled-title'>Are you sure you want to cancel the order?</h1>
            <div className='modal-order__canceled-btn'>
                <Button
                    type='button'
                    className='modal-order__btn'
                    onClick={handleCanceledOrder}
                >
                    Yes, I wish
                </Button>

                <Button
                    type='button'
                    className='modal-order__btn'
                    onClick={() => { fnShowModalOrderCanceled() }}
                >
                    No
                </Button>

            </div>
        </ModalBase>
    );
};

ModalOrderCanceled.propTypes = {};

export default ModalOrderCanceled;