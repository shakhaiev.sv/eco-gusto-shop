import * as yup from 'yup'

const validation = yup.object({
    loginOrEmail: yup
        .string()
        .email('Invalid email format')
        .required('Email is required')
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Email is not valid"),
    password: yup
        .string()
        .min(6, 'Password minimum length is 6 characters')
        .max(25, 'Password maximum length is 25 characters')
        .required("Password is required"),
})

export default validation 