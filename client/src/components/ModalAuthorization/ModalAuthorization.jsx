import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ModalBase from '../ModalBase/ModalBase.jsx';
import './ModalAuthorization.scss';
import { ModalContext } from '../../context/ModalContext.jsx';
import { Formik, Form } from 'formik';
import Input from '../Form/Input/Input.jsx';
import Close from '../../pictures/svg/modal/close.svg?react'
import Mail from '../../pictures/svg/modal/mail-open.svg?react'
import LockClose from '../../pictures/svg/modal/lock-closed.svg?react'
import LockOpen from '../../pictures/svg/modal/lock-open.svg?react'
import cn from 'classnames'
import validation from './validationSchema/validation.js';
import { useLocation, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import {
    selectorAuthorizationForm,
    selectorResponceAuth,
    selectorListCart,
    selectorListFavorite,
} from '../../store/selectors/selectors.js';
import Button from '../Button/Button.jsx';
import { actionsFetchAuthorizationClient, actionDataAuthorizationClient } from '../../store/slices/clients.js';
import {
    actionUpdateListCardForAuthClient,
    actionGetCartForAuthClient
} from '../../store/slices/cart.js';
import {
    actionUpdateListFavoritesForAuthClient,
    actionGetFavoritesForAuthClient
} from '../../store/slices/favorite.js';

const ModalAuthorization = () => {
    const [viewPassword, setViewPassword] = useState(false)
    const { fnShowModalAuth } = useContext(ModalContext);
    const handleShowModalAuth = () => {
        fnShowModalAuth();
    };
    const dispatch = useDispatch();
    const dataAuthorizationFormClient = useSelector(selectorAuthorizationForm);
    const responceAuthorization = useSelector(selectorResponceAuth);
    const listCart = useSelector(selectorListCart);
    const listFavorites = useSelector(selectorListFavorite);
    const location = useLocation()
    const navigte = useNavigate()

    const handleViewPassword = () => {
        setViewPassword((prevState) => !prevState)
    }

    useEffect(() => {
        if (responceAuthorization.token) {
            handleShowModalAuth();
            // Обновление данных по корзине 
            if (listCart.length) {
                dispatch(actionUpdateListCardForAuthClient({
                    token: responceAuthorization.token,
                    dataCartList: {
                        products: listCart.map(({ cartQuantity, _id }) => ({
                            product: _id,
                            cartQuantity
                        }))
                    }
                }));
            } else {
                dispatch(actionGetCartForAuthClient(responceAuthorization.token));
            }

            // Оновлення даних по улюбленим продуктам
            if (listFavorites.length) {
                dispatch(actionUpdateListFavoritesForAuthClient({
                    token: responceAuthorization.token,
                    dataFavoritesList: {
                        products: listFavorites.map(({ _id }) => _id)
                    }
                }));
            } else {
                dispatch(actionGetFavoritesForAuthClient(responceAuthorization.token));
            }
        }

        if (location.pathname === '/registration' && responceAuthorization.token) {
            navigte('/products')
        }
    }, [responceAuthorization, listCart, listFavorites]);


    return (
        <ModalBase
            onClose={handleShowModalAuth}
            classNameModal='form-authorization__modal'
        >
            <Formik
                initialValues={dataAuthorizationFormClient}
                validationSchema={validation}
                onSubmit={(values, { resetForm }) => {
                    dispatch(actionsFetchAuthorizationClient(values));
                    dispatch(actionDataAuthorizationClient(values));
                    if (responceAuthorization.token) {
                        resetForm();
                    }
                }}
            >
                {({ values, errors, touched }) => (
                    <Form className='form-authorization'>
                        <div className='form-authorization__wrap-title'>
                            <h2 className='form-authorization__title'>Customer login</h2>
                            <Button type='button' className='form-authorization__btn-close' onClick={handleShowModalAuth}><Close /> </Button>
                        </div>
                        <div className='input-box'>
                            <Input
                                classNameLabel={cn('form-auth__label', { 'filled': values.loginOrEmail })}
                                label='Email'
                                type='email'
                                name='loginOrEmail'
                                className='form-auth__input '
                                error={errors.loginOrEmail && touched.loginOrEmail}
                                autoComplete="off"
                            >
                                <Mail />
                            </Input>
                        </div>
                        <div className='input-box'>
                            <Input
                                classNameLabel={cn('form-auth__label', { 'filled': values.password })}
                                label='Password'
                                type={viewPassword ? 'text' : 'password'}
                                name='password'
                                autoComplete="off"
                                className='form-auth__input '
                                error={errors.password && touched.password}
                            >
                            </Input>
                            <Button
                                type='button'
                                className='btn-lock'
                                onClick={handleViewPassword}>
                                {viewPassword ? <LockOpen /> : <LockClose />}

                            </Button>
                        </div>
                        <div className='row-valid'>
                            {
                                (responceAuthorization.password || responceAuthorization.message) && <p
                                    className='is-not-valid'>Check the correctness of the entered data</p>
                            }
                        </div>
                        <div className='row'>
                            <Button
                                type='submit'
                                className='btn-authorization'
                            >Sign in</Button>
                        </div>
                        <div className='row'>
                            <span className='form-authorization__text'>Don't have an account?</span>
                            <Button
                                to='/registration'
                                className='form-authorization__registr'
                                onClick={handleShowModalAuth}
                            > Register</Button>
                        </div>
                    </Form>
                )}
            </Formik>
        </ModalBase >
    );
};

ModalAuthorization.propTypes = {};

export default ModalAuthorization;
