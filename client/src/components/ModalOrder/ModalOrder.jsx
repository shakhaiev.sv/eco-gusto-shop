import React, { useContext, useEffect } from 'react';
import './ModalOrder.scss'
import ModalBase from '../ModalBase/ModalBase';
import Button from '../Button/Button';
import { ModalContext } from '../../context/ModalContext.jsx'
import { useNavigate } from 'react-router-dom';
import { actionDeleteDataOrder } from '../../store/slices/order.js'
import { useDispatch, } from 'react-redux';
import MiniCard from '../MiniCard/MiniCard.jsx';
import { selectorToken } from '../../store/selectors/selectors.js'
import {
    actionDeleteCartAuthClient,
    actionDeleteCart,
    actionAllLengthCoodsOnCart
} from '../../store/slices/cart.js'
import { useSelector } from 'react-redux';

const ModalOrder = (props) => {

    const token = useSelector(selectorToken);
    const { dataResponce, setIsSubmit } = props
    const navigate = useNavigate()
    const { fnShowModalOrder } = useContext(ModalContext)
    const dispatch = useDispatch();

    const handleShowModalOrder = () => {
        if (token) {
            dispatch(actionDeleteCartAuthClient(token))
        } else {
            dispatch(actionAllLengthCoodsOnCart())
        }
        dispatch(actionDeleteCart())
        dispatch(actionDeleteDataOrder())
        fnShowModalOrder()
        navigate('/')
    }

    const handleEditCart = () => {
        fnShowModalOrder()
        dispatch(actionDeleteDataOrder())
        setIsSubmit(false)
    }

    return (
        <ModalBase
            headerTitle='Order'
            onClose={handleEditCart}
            classNameModal='modal-order__content'
        >
            {dataResponce.result && <div className='modal-order'>
                <h2 className='modal-order__text'>Thank You for Your Order!</h2>
                <div className='modal-order__wrap'>
                    <h3 className='modal-order__title-details'>Order details:</h3>
                    <p className='modal-order__text-details'>Order number - <span>{dataResponce.order.orderNo}</span></p>
                    <p className='modal-order__text-details'>Amount due - <span>{dataResponce.order.totalSum.toFixed(2)}</span></p>
                    <p className='modal-order__text-details'>Delivery address - <span>{dataResponce.order.deliveryAddress.city}</span> <span>{dataResponce.order.deliveryAddress.address}</span></p>
                    <p className='modal-order__text-details'>Delivery date - <span>{dataResponce.order.deliveryAddress.deliverDate}</span></p>
                </div>
                <div className='modal-order__wrap' >
                    <h3 className='modal-order__title-details'>Contact details:</h3>
                    <p className='modal-order__text-details'>First Name- <span>{dataResponce.order?.customer?.firstName || dataResponce.order?.customerId?.firstName}</span></p>
                    <p className='modal-order__text-details'>Last Name - <span>{dataResponce.order?.customer?.lastName || dataResponce.order?.customerId?.lastName}</span></p>
                    <p className='modal-order__text-details'>Your phone - <span>{dataResponce.order.mobile}</span></p>
                    <p className='modal-order__text-details'>Your email (the order was sent to this post office) - <span>{dataResponce.order.email}</span></p>
                </div>
                <p className='modal-order__text' >Our specialist will contact you shortly to confirm the order.</p>
                <Button
                    type='button'
                    className='modal-order__btn'
                    onClick={handleShowModalOrder}
                >OK</Button>
            </div>}
            {dataResponce.message && <div className='modal-order__unavailable'>
                <h1 className='modal-order__unavailable-title'>Oops, this product is no longer available</h1>
                <ul className='modal-order__unavailable-list'>
                    {
                        dataResponce?.productAvailibilityInfo?.unavailableProducts.map((item, index) => {
                            return <li key={index} className='modal-order__unavailable-item'>
                                <MiniCard dataProduct={item} linkNot={true} />
                                <p className='modal-order__unavailable-realquantity'>Real quantity: <span>{item.quantity}</span> </p>
                            </li>
                        })
                    }
                </ul>
                <div className='modal-order__unavailable-btn-wrap' >
                    <Button
                        type='button'
                        className='modal-order__btn'
                        onClick={handleEditCart}
                    >
                        ОК
                    </Button>
                </div>
            </div>
            }
        </ModalBase>
    );
};

ModalOrder.propTypes = {};

export default ModalOrder;