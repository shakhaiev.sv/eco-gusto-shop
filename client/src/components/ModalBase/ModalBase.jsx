import React from 'react';
import PropTypes from 'prop-types';
import './ModalBase.scss'
import Portal from '../portal'
import Close from '../../pictures/svg/modal/close.svg?react'
import Button from '../Button/Button.jsx';
import cn from 'classnames'

const ModalBase = (props) => {
    const { children,
        headerTitle,
        onClose,
        classNameModal
    } = props


    const handleOnClose = () => {
        onClose()
    }

    return (
        <Portal
            component={<div className='modal-wrap' onClick={(e) => {
                if (e.target.classList.contains('modal-wrap')) {
                    onClose()
                }
            }}>
                <div className={cn('modal', classNameModal)}>
                    {headerTitle && <div className='modal-header-wrap'>
                        <h1 className='modal-header-title'>{headerTitle}</h1>
                        <Button className='modal-btn-close' onClick={handleOnClose}><Close /></Button>
                    </div>}
                    <div className='modal-content'>
                        {children}
                    </div>
                </div>
            </div>}
        />

    );
};

ModalBase.propTypes = {
    children: PropTypes.any
};

export default ModalBase;