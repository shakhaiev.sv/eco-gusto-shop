import React, { useEffect } from 'react';
import './SnackbarAlert.scss';

const SnackbarAlert = ({ open, setOpen }) => {

  useEffect(() => {
    setTimeout(() => {
      setOpen(false);
    }, 1000);
  }, []);

  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
  };

  return (
    <div>
      <div className={`snackbar ${open ? 'show' : ''}`}>
        <div className="snackbar-content">
          <span>The product was successfully added to the cart!</span>
          <button className="close-btn" onClick={handleClose}>
            &times;
          </button>
        </div>
      </div>
    </div>
  );
};

export default SnackbarAlert;