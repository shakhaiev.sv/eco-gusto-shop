import React, { useRef, useState, useContext, useEffect } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

import validationSchema from './validation/validationSchema.js'
import './ModalCreateProducts.scss';
import Input from '../../Form/Input/Input.jsx'
import Button from '../../Button/Button.jsx';
import { ModalContext } from '../../../context/ModalContext';
import ModalBase from '../../ModalBase/ModalBase';
import { sendRequest, sendRequestUploadImage } from '../../../helpers/SendRequest.js';
import { stickersNameforProduct } from '../../../contans/constans.js'
import { useSelector } from 'react-redux';
import AddImage from '../../../pictures/svg/product/addImage.svg?react'
import RemoveImg from '../../../pictures/svg/product/removeImage.svg?react'

import { selectorCategories, selectorToken } from '../../../store/selectors/selectors.js'

const ModalCreateAndEditProducts = ({ onSubmit }) => {
    const { currentEditProduct, fnShowModalCreateProducts } = useContext(ModalContext);
    const imgContainerRef = useRef(null);
    const imgInputHelperRef = useRef(null);
    const imgFiles = useRef([]);
    const [error, setError] = useState('');
    const [selectedImage, setSelectedImage] = useState(null);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('')
    const authToken = useSelector(selectorToken);
    const [imageUrl, setImageUrl] = useState('');


    const categories = useSelector(selectorCategories)

    const initialValues = (currentEditProduct._id) ? {
        name: currentEditProduct.name || '',
        currentPrice: currentEditProduct.currentPrice || '',
        previousPrice: currentEditProduct.previousPrice || '',
        quantity: currentEditProduct.quantity || '',
        brand: currentEditProduct.brand || '',
        weight: currentEditProduct.weight || '',
        shelfLife: currentEditProduct.shelfLife || '',
        country: currentEditProduct.country || '',
        categoryName: currentEditProduct.categoryName || '',
        categoryId: currentEditProduct.categoryId || '',
        storageTemperature: currentEditProduct.storageTemperature || '',
        nutritionFacts: {
            calories: currentEditProduct.nutritionFacts.calories || '',
            proteins: currentEditProduct.nutritionFacts.proteins || '',
            fats: currentEditProduct.nutritionFacts.fats || '',
            carbohydrates: currentEditProduct.nutritionFacts.carbohydrates || '',
        },
        numberOfunits: currentEditProduct.numberOfunits || '',
        imageUrls: [currentEditProduct.imageUrls[0]],
        sticker: currentEditProduct.sticker || '',
        reviews: currentEditProduct.reviews || '',
    } : {
        name: '',
        currentPrice: '',
        previousPrice: '',
        quantity: '',
        brand: '',
        weight: '',
        shelfLife: '',
        country: '',
        categoryName: '',
        categoryId: '',
        storageTemperature: '',
        nutritionFacts: {
            calories: '',
            proteins: '',
            fats: '',
            carbohydrates: '',
        },
        numberOfunits: '',
        sticker: '',
        reviews: ''
    };


    useEffect(() => {
        if (currentEditProduct._id) {
            setSelectedImage(currentEditProduct.imageUrls[0])
        }
    }, [])

    const addImgHandler = async (event) => {
        event.preventDefault();
        try {
            if (authToken) {
                const fileInput = document.querySelector("#add-single-img");
                const file = fileInput.files[0];


                if (file) {
                    let formData = new FormData();
                    formData.append('file', file);
                    const response = await sendRequestUploadImage('/products/images', "POST", formData, authToken);



                    if (response && response.imageUrl) {
                        setSelectedImage(response.imageUrl);
                    }



                }
            }
        } catch (err) {
            console.error(err);
        }
    };



    const removeImgHandler = () => {
        setSelectedImage(null);
        imgFiles.current = [];
    };
    const getImgFileList = () => {
        const imgFilesHelper = new DataTransfer();
        imgFiles.current.forEach((imgFile) => imgFilesHelper.items.add(imgFile));
        return imgFilesHelper.files;
    };

    const handleSubmit = async (values, { setSubmitting }) => {
        try {
            const files = getImgFileList();
            const imageUrls = await Promise.all(
                Array.from(files).map(file => {
                    return new Promise((resolve, reject) => {
                        const reader = new FileReader();
                        reader.onload = () => resolve(reader.result);
                        reader.onerror = reject;
                        reader.readAsDataURL(file);
                    });
                })
            );

            const productData = (currentEditProduct._id) ? {
                ...values,
                imageUrls: [selectedImage]
            } : {
                ...values,
                imageUrls: [selectedImage],
                categoryName: categories.find((item) => item.id == values.categoryId).name,
            };


            if (currentEditProduct._id) {
            


                const response = await sendRequest(`/products/${currentEditProduct._id}`, "PUT", productData, authToken);
                if (response.id) {
                    setAlertMessage('Product successfully updated!');
                }

            } else {
                const response = await sendRequest('/products', 'POST', productData, authToken);
                if (response._id) {
                    setAlertMessage('Product successfully created!');


                }
            }

            setShowAlert(true);
           
            setTimeout(() => {
                setShowAlert(false)
                fnShowModalCreateProducts()

            }, 1000);



            if (typeof onSubmit === 'function') {
                onSubmit(response);
            }
        } catch (error) {
            console.error('Error creating product:', error);
        }
        setSubmitting(false);
    };


    return (
        <ModalBase
            classNameModal='create-products__modal'
            onClose={() => { fnShowModalCreateProducts() }}
            headerTitle={currentEditProduct._id ? `Edit "${currentEditProduct.name}" card` : "Create New Product"}
        >
            {showAlert && <div className="alert">{alertMessage}</div>}
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={handleSubmit}
            >
                {({ isSubmitting, errors, touched }) => (
                    <Form className="create-product-form">
                        <div className="create-product-form__wrap">
                            <div className="create-product-form__img">
                                <div ref={imgContainerRef} className="create-product-form__img-wrap ">
                                    {selectedImage ? (
                                        <>
                                            <img src={selectedImage} alt="Selected" className="create-product-form__preview-img" />

                                            <Button type="button" className="create-product-form__remove-img" onClick={removeImgHandler}><RemoveImg /></Button>
                                        </>
                                    ) : (
                                        <>
                                            <label id="add-img-label" htmlFor="add-single-img"><AddImage /></label>
                                            <input
                                                className='product-img'
                                                ref={imgInputHelperRef}
                                                type="file"
                                                id="add-single-img"
                                                accept="image/*"
                                                onChange={addImgHandler}
                                            />

                                        </>
                                    )}

                                </div>
                                {error && <div className="error">{error}</div>}
                                <p className='create-product-form__img-warning'>Accept file (only jpg/jpeg/png/webp)</p>
                            </div>

                            <div className="create-product-form__desc">
                                <div className="form-group">
                                    <Input
                                        type="text"
                                        id="name"
                                        name="name"
                                        placeholder="Name"
                                        error={errors.name && touched.name}
                                    ><span>Name</span></Input>
                                </div>
                                <div className="form-group">
                                    <Input
                                        type="text"
                                        id="numberOfunits"
                                        name="numberOfunits"
                                        placeholder="Number of Units"
                                        error={errors.numberOfunits && touched.numberOfunits}
                                    ><span>Number of Units</span></Input>
                                </div>
                                <div className="form-group">
                                    <Input
                                        type="number"
                                        id="currentPrice"
                                        name="currentPrice"
                                        placeholder="Current Price"
                                        error={errors.currentPrice && touched.currentPrice}
                                    ><span>Current Price</span></Input>
                                </div>
                                <div className="form-group">

                                    <Input
                                        type="number"
                                        id="previousPrice"
                                        name="previousPrice"
                                        placeholder="Previous Price"
                                        error={errors.previousPrice && touched.previousPrice}
                                    ><span>Previous Price</span></Input>
                                </div>
                                {/* Придумать что-то с этим */}
                                <div className="form-group">
                                    <Input
                                        type="text"
                                        id="weight"
                                        name="weight"
                                        placeholder="Weight"
                                        error={errors.weight && touched.weight}
                                    ><span>Weight</span></Input>
                                </div>
                                <div className="form-group">
                                    <Input
                                        type="number"
                                        id="quantity"
                                        name="quantity"
                                        placeholder="Quantity"
                                        error={errors.quantity && touched.quantity}
                                    ><span>Quantity</span></Input>
                                </div>
                                <div className="form-group">
                                    <Input
                                        type="number"
                                        id="reviews"
                                        name="reviews"
                                        placeholder="Reviews"
                                        error={errors.reviews && touched.reviews}
                                    ><span>Reviews</span></Input>
                                </div>
                                <div className="form-group">
                                    <label htmlFor='categories'><span>Category</span></label>
                                    <Field as="select"
                                        id="categories"
                                        name="categoryId"
                                    >
                                        <option value="">Select category</option>
                                        {categories?.map((category, index) => (
                                            <option key={index} value={category.id}>
                                                {category.name}
                                            </option>
                                        ))}
                                    </Field>

                                    <ErrorMessage name="categoryId" component="div" className="error" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor='sticker'><span>Sticker</span></label>
                                    <Field as="select"
                                        id="sticker"
                                        name="sticker"
                                    >
                                        <option value="">Select sticker</option>
                                        {stickersNameforProduct?.map((sticker, index) => (
                                            <option key={index} value={sticker}>
                                                {sticker}
                                            </option>
                                        ))}
                                    </Field>

                                    <ErrorMessage name="sticker" component="div" className="error" />
                                </div>

                            </div>
                        </div>
                        <div className="create-product-form__info">
                            <h2 className='create-product-form__info-title'>About the product</h2>
                            <div className="create-product-form__info-wrap">
                                <div className="create-product-form__info-property">
                                    <p className='create-product-form__property-title'>Nutritional properties, 100g </p>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="calories"
                                            name="nutritionFacts.calories"
                                            placeholder="Calories"
                                            error={errors.nutritionFacts?.calories && touched.nutritionFacts?.calories}
                                        ><span>Calories</span></Input>
                                    </div>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="proteins"
                                            name="nutritionFacts.proteins"
                                            placeholder="Proteins"
                                            error={errors.nutritionFacts?.proteins && touched.nutritionFacts?.proteins}
                                        ><span>Proteins</span></Input>
                                    </div>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="fats"
                                            name="nutritionFacts.fats"
                                            placeholder="Fats"
                                            error={errors.nutritionFacts?.fats && touched.nutritionFacts?.fats}
                                        ><span>Fats</span></Input>
                                    </div>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="carbohydrates"
                                            name="nutritionFacts.carbohydrates"
                                            placeholder="Carbohydrates"
                                            error={errors.nutritionFacts?.carbohydrates && touched.nutritionFacts?.carbohydrates}
                                        ><span>Carbohydrates</span></Input>
                                    </div>
                                </div>
                                <div className="create-product-form__info-general">
                                    <p className='create-product-form__general-title'>General information</p>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="brand"
                                            name="brand"
                                            placeholder="Brand"
                                            error={errors.brand && touched.brand}
                                        ><span>Brand</span></Input>
                                    </div>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="shelfLife"
                                            name="shelfLife"
                                            placeholder="Shelf Life"
                                            error={errors.shelfLife && touched.shelfLife}
                                        ><span>Shelf Life</span></Input>
                                    </div>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="storageTemperature"
                                            name="storageTemperature"
                                            placeholder="Storage Temperature"
                                            error={errors.storageTemperature && touched.storageTemperature}
                                        ><span>Storage Temperature</span></Input>
                                    </div>
                                    <div className="form-group">
                                        <Input
                                            type="text"
                                            id="country"
                                            name="country"
                                            placeholder="Country"
                                            error={errors.country && touched.country}
                                        ><span>Country</span></Input>
                                    </div>

                                </div>
                            </div>
                            <div className="form-group">
                                <button type="submit" className='button primary__button' disabled={isSubmitting}>Create Product</button>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </ModalBase >
    );
};

export default ModalCreateAndEditProducts;
