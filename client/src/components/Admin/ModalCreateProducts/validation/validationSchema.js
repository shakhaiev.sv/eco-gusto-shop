import * as Yup from 'yup'

const validation = Yup.object({
    name: Yup
        .string()
        .trim()
        .required(' '),
    currentPrice: Yup
        .number()
        .required(' '),
    previousPrice: Yup
        .number(),
    quantity: Yup
        .number()
        .required(' '),
    brand: Yup
        .string()
        .trim()
        .required(' '),
    weight: Yup
        .string()
        .trim()
        .required(' '),
    shelfLife: Yup
        .string()
        .trim()
        .required(' '),
    country: Yup
        .string()
        .trim()
        .required(' '),
    categoriesName: Yup
        .string()
        .trim(),
    categoryId: Yup
        .string(),
    storageTemperature: Yup
        .string()
        .trim()
        .required(' '),
    nutritionFacts: Yup.object({
        calories: Yup
            .string()
            .trim()
            .required(' '),
        proteins: Yup
            .string()
            .trim()
            .required(' '),
        fats: Yup
            .string()
            .trim()
            .required(' '),
        carbohydrates: Yup
            .string()
            .trim()
            .required(' '),
    }),
    reviews: Yup
        .number()
        .required(' '),
    numberOfunits: Yup
        .string()
        .trim()
        .required(' '),
})

export default validation 