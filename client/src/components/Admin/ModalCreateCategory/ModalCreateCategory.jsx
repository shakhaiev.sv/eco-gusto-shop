import React, { useContext, useState, useRef, useEffect } from 'react';
import ModalBase from '../../ModalBase/ModalBase';
import { ModalContext } from '../../../context/ModalContext';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useSelector } from 'react-redux';
import { sendRequest, sendRequestUploadImage } from '../../../helpers/SendRequest';
import { selectorToken } from '../../../store/selectors/selectors';
import './ModalCreateCategory.scss';

import AddImage from '../../../pictures/svg/product/addImage.svg?react'
import RemoveImg from '../../../pictures/svg/product/removeImage.svg?react'
import Button from '../../Button/Button';

const ModalCreateandEditCategory = () => {
    const { showModalCreateCategory, fnShowModalCreateCategory, currentEditCategory } = useContext(ModalContext);
    const [selectedImage, setSelectedImage] = useState(null);
    const [error, setError] = useState('');
    const imgContainerRef = useRef(null);
    const imgInputHelperRef = useRef(null);
    const imgFiles = useRef([]);
    const authToken = useSelector(selectorToken);

    useEffect(() => {
        if (currentEditCategory.id) {
            setSelectedImage(currentEditCategory.imgUrl)

        }
    }, [])

    const formik = useFormik({
        initialValues: (currentEditCategory.id) ? {
            imgUrl: currentEditCategory.imgUrl,
            categoryName: currentEditCategory.name,
        } : {
            imgUrl: '',
            categoryName: '',
        },
        validationSchema: Yup.object({
            categoryName: Yup.string()
                .trim()
                .required('Category name is required'),
        }),
        onSubmit: async (values) => {
            await handleSaveCategory(values);
        },
    });




    if (!showModalCreateCategory) return null;

    const addImgHandler = async (event) => {
        event.preventDefault();
        try {
            if (authToken) {
                const fileInput = imgInputHelperRef.current;
                const file = fileInput.files[0];
                if (file) {
                    let formData = new FormData();
                    formData.append('file', file);

                    const response = await sendRequestUploadImage('/products/images', "POST", formData, authToken);

                    const { imageUrl: imgUrl } = response;

                    if (imgUrl) {
                        setSelectedImage(`${imgUrl}`);
                    } else {
                        setError('Failed to upload image. Please try again.');
                    }
                    imgFiles.current = [file];
                }
            }
        } catch (err) {
            console.error(err);
            setError('An error occurred while uploading the image.');
        }
    };

    const removeImgHandler = () => {
        setSelectedImage(null);
        imgFiles.current = [];
    };

    const handleSaveCategory = async (values) => {

        if (authToken) {
            const patternNewCategory = {
                name: values.categoryName,
                imgUrl: selectedImage,
            };

            if (currentEditCategory.id) {
                const response = await sendRequest(`/catalog/${currentEditCategory.id}`, 'PUT', patternNewCategory, authToken);
                if (response._id) {
                    alert(`Category ${response.name} edit to DB`)
                    fnShowModalCreateCategory();
                }
            } else {
                const response = await sendRequest('/catalog', 'POST', patternNewCategory, authToken);
                if (response?.id) {
                    alert(`Category ${response.name} add to DB`)
                    fnShowModalCreateCategory();
                }

            }
        };

    }

    return (
        <ModalBase
            classNameModal='create-category__modal'
            onClose={fnShowModalCreateCategory}
            headerTitle={currentEditCategory.id ? `Edit "${currentEditCategory.name}" card` : "Create New Category"}
        >
            <form onSubmit={formik.handleSubmit} className="create-category__form">
                <div className="create-category__content">
                    <div ref={imgContainerRef} className="custom-category__image-container">
                        {selectedImage ? (
                            <>
                                <img src={selectedImage} alt="Selected" className="preview-image" />
                                <Button type="button" className="delete-button" onClick={removeImgHandler}><RemoveImg /></Button>
                            </>
                        ) : (
                            <>
                                <label id="add-img-label" htmlFor="add-single-img"><AddImage /></label>
                                <input
                                    className='product-img'
                                    ref={imgInputHelperRef}
                                    type="file"
                                    id="add-single-img"
                                    accept="image/*"
                                    onChange={addImgHandler}
                                />
                                {error && <div className="error">{error}</div>}
                            </>
                        )}
                    </div>
                    <div className='form-group'>
                        <input
                            type="text"
                            name="categoryName"
                            value={formik.values.categoryName}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            placeholder="Enter category name"
                            className="create-category__input"
                        />

                        {formik.touched.categoryName && formik.errors.categoryName ? (
                            <div className="error">{formik.errors.categoryName}</div>
                        ) : null}
                    </div>
                </div>
                <button type="submit" className="create-category__button">
                    Save Category
                </button>
            </form>
        </ModalBase >
    );

}
export default ModalCreateandEditCategory;
