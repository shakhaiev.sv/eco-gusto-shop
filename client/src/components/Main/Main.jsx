import React from 'react';
import PropTypes from 'prop-types';
import './Main.scss'
import Hero from './Hero/Hero.jsx'

import BestDeals from './BestDeals/BestDeals.jsx';
import Categories from './Categories/Categories.jsx';
import Deals from "./Deals/Deals.jsx";
import PopularProducts from "./PopularProducts/PopularProducts.jsx";
import BannerSection from "./BannerSection/BannerSection.jsx";

const Main = () => {

    return (
        <>
            <Hero />
            <Categories />
            <PopularProducts />
            <Deals />
            <BestDeals />
            <BannerSection />
        </>
    );
};

Main.propTypes = {};

export default Main;