import React, { useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux"
import CardDeals from "../../CardDeals/CardDeals.jsx"
import { actionFetchDeals } from "../../../store/slices/deals.js"
import { selectorDeals } from "../../../store/selectors/selectors.js"
import "./Deals.scss"

import Container from '../../../layout/container/Container.jsx';
import Button from '../../Button/Button.jsx';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper/modules';
import { useMedia } from '../../../hooks/UseMedia.js'

const DealsOfTheDay = () => {

    const dispatch = useDispatch();
    const dealsList = useSelector(selectorDeals);

    useEffect(() => {
        if (!dealsList.length) {
            dispatch(actionFetchDeals())
        }
    }, [])

    const countClide = useMedia("(max-width:868px)")

    return (
        <section className="sect-deals">
            <Container>
                <div className='sect-deals__title-wrap'>
                    <h1 className="sect-deals__title sect-title">Deals Of The Day</h1>
                    <Button to='/deals' className="sect-deals__all-deals">All Deals</Button>
                </div>
                <Swiper
                    slidesPerView={(countClide) ? 1 : 2.6}
                    spaceBetween={70}
                    modules={[Navigation]}
                    navigation={true}
                    className="sect-deals__slider"
                >
                    {
                        dealsList?.map((deals, index) => <SwiperSlide
                            key={index}
                            className='sect-deals__item'
                        >
                            <CardDeals
                                dataDeals={deals}

                            />
                        </SwiperSlide>)
                    }
                </Swiper>
            </Container>
        </section>
    );
};

export default DealsOfTheDay;