import React from 'react';
import PropTypes from 'prop-types';
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectorCategories } from '../../../store/selectors/selectors.js'
import { actionFetchCategories } from '../../../store/slices/categories.js'
import './Categories.scss'
import Container from '../../../layout/container/Container.jsx'
import Button from '../../Button/Button.jsx'
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper/modules';
import { useMedia } from '../../../hooks/UseMedia.js'


const Categories = () => {
    const dispath = useDispatch()
    const listCategories = useSelector(selectorCategories)
    useEffect(() => {
        if (!listCategories.length) {
            dispath(actionFetchCategories())
        }
    }, [listCategories.length])

    const countClide = useMedia("(max-width:768px)")

    return (
        <section className='sect-categories'  >
            <Container>
                <div className='sect-categories__wrapper'>
                    <h1 className='sect-categories__title sect-title'>Shop by Categories</h1>
                    <Swiper
                        slidesPerView={(countClide) ? 1.7 : 4.6}
                        spaceBetween={30}
                        modules={[Navigation]}
                        navigation={true}
                        className="sect-categories__slider"
                    >
                        {
                            listCategories?.map((item, index) => <SwiperSlide
                                key={index}
                                className='sect-categories__item'
                            >
                                <Button
                                    to={`/products?categoryName=${item.name}`}
                                    className='sect-categories__link'
                                >
                                    <img className='sect-categories__img' width='80px' src={item.imgUrl} alt="" />
                                    <p className='sect-categories__name'>{item.name}</p>

                                </Button>
                            </SwiperSlide>
                            )
                        }
                    </Swiper>

                </div>
            </Container>
        </section >
    );
};

Categories.propTypes = {};

export default Categories;