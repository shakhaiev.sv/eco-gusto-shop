import React from 'react';
import Container from '../../../layout/container/Container.jsx'
import './Hero.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination } from 'swiper/modules'


const Hero = () => {
  return (
    <section className="sect-hero">
      <Container>
        <Swiper
          style={{
            '--swiper-pagination-color': '#3BB77E',
          }}
          className='sect-hero__slider'
          spaceBetween={50}
          slidesPerView={1}
          autoplay={{
            delay: 8000,
            pauseOnMouseEnter: true,
            disableOnInteraction: false
          }}
          modules={[Autoplay, Pagination]}
          pagination={{ clickable: true }}
          loop
        >
          <SwiperSlide
            className='sect-hero__slide'
          >
            <h1 className="sect-hero__slide-title">Fresh Vegetables Big discount</h1>
            <p className='sect-hero__slide-text'>Save up to 50% off on your first order</p>
            <div className='sect-hero__slide-wrap'>
              <img className='sect-hero__slide-img' src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721852979/slide1_n04stc.png" alt="" />
            </div>
          </SwiperSlide>
          <SwiperSlide
            className='sect-hero__slide'
          >
            <h1 className="sect-hero__slide-title">Don't miss amazing grocery deals</h1>
            <p className='sect-hero__slide-text'>Sign up for the daily newsletter</p>
            <div className='sect-hero__slide-wrap'>
              <img className='sect-hero__slide-img' src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721852980/slide2_kxawq4.png" alt="" />
            </div>
          </SwiperSlide>
        </Swiper>
      </Container >
    </section >
  );
};

export default Hero;
