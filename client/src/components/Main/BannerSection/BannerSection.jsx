import React from 'react';
import './BannerSection.scss';
import Container from '../../../layout/container/Container';


const BannerSection = () => {
    return (
        <section className="banner">
            <Container>
                <div className="banner-wrap" >
                    <div className="banner-content">
                        <div className="banner-text">
                            <h2>Stay home & get your daily needs from our shop</h2>
                            <p>Start Your Daily Shopping with <span className="nest-mart">Nest Mart</span></p>
                            <form action="" className="banner-form">
                                <input type="email" required placeholder="Your emaill address" />
                                <button type="submit">Subscribe</button>
                            </form>
                        </div>
                        <div className="banner-image">
                            <img src='https://res.cloudinary.com/dre55ftmq/image/upload/v1721852663/banner-9_npkh2r.png' alt="Banner" />
                        </div>
                    </div>
                </div>
            </Container>
        </section>
    );
};

export default BannerSection;
