import React from 'react';
import './BestDeals.scss';
import Container from '../../../layout/container/Container';
import MiniCard from '../../MiniCard/MiniCard.jsx'
import { priorityValueforBestDeals } from '../../../contans/constans.js'
import { useEffect, } from 'react';
import { actionFetchBestDeals } from '../../../store/slices/deals.js'
import { useDispatch, useSelector } from 'react-redux'
import { selectorBestDeals, selectorPageDeals } from '../../../store/selectors/selectors.js'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Pagination, } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';


const BestDeals = () => {
  const dispatch = useDispatch();
  const listBestDeals = useSelector(selectorBestDeals)
  const pageDeals = useSelector(selectorPageDeals)
  useEffect(() => {
    if (!Object.keys(listBestDeals).length) {
      const queryParams = priorityValueforBestDeals.map((item, index) => {
        if (index === priorityValueforBestDeals.length - 1) {
          return `priorityName=${item}`
        } else {
          return `priorityName=${item}&`
        }
      }).join('')
      dispatch(actionFetchBestDeals({ queryParams, pageDeals }))

    }
  }, [])

  return (
    <section className="sect-best-deals">
      <Container>
        <ul className="sect-best-deals__list">
          {
            priorityValueforBestDeals?.map((itemPriorityValue, indexPriorityValue) => {
              return <li key={indexPriorityValue} className='sect-best-deals__item'>
                <h3 className='sect-best-deals__title' >{itemPriorityValue}</h3>
                <Swiper
                  style={{
                    '--swiper-pagination-color': '#3BB77E',
                  }}
                  slidesPerView={3}
                  direction={"vertical"}
                  spaceBetween={20}
                  freeMode={true}
                  pagination={{
                    clickable: true
                  }}
                  modules={[Pagination]}
                  className='sect-best-deals__under-list'>
                  {
                    (listBestDeals[itemPriorityValue]?.message) ? <></> : listBestDeals[itemPriorityValue]?.map((itemProduct, indexProduct) => {
                      return <SwiperSlide
                        className='sect-best-deals__under-item'
                        key={indexProduct}
                      >
                        <MiniCard
                          className='sect-best-deals__under-link'
                          dataProduct={itemProduct}
                        />
                      </SwiperSlide>
                    })
                  }
                </Swiper>
              </li>
            })
          }
        </ul>
      </Container>

    </section >
  );
};

export default BestDeals;