import React, { useEffect } from 'react';
import { useSelector, useDispatch } from "react-redux"
import Product from "../../Product/Product.jsx"
import { actionFetchPopularProduct } from "../../../store/slices/products.js"
import {
    selectorPopularProducts,
    selectorPopularProductsPage,
    selectorNextPagePopulaProducts
} from '../../../store/selectors/selectors.js'
import "./PopularProducts.scss"
import Container from '../../../layout/container/Container.jsx';
import { stickersNameforProduct } from '../../../contans/constans.js'
import { useInView } from 'react-intersection-observer'

const PopularProducts = () => {
    const { ref, inView } = useInView({
        threshold: 0
    })
    const dispatch = useDispatch();
    const listPopulatProducts = useSelector(selectorPopularProducts)
    const popularProductsPage = useSelector(selectorPopularProductsPage)
    const nextPagePopularProducts = useSelector(selectorNextPagePopulaProducts)

    useEffect(() => {
        if (nextPagePopularProducts.page) {
            dispatch(actionFetchPopularProduct({
                queryParams: stickersNameforProduct.map((item, index) => {
                    if (index === stickersNameforProduct.length - 1) {
                        return `sticker=${item}`
                    } else {
                        return `sticker=${item}&`
                    }
                }).join(''),
                pagination: nextPagePopularProducts
            }))
        }

    }, [inView])

    useEffect(() => {
        if (!listPopulatProducts.length) {
            dispatch(actionFetchPopularProduct({
                queryParams: stickersNameforProduct.map((item, index) => {
                    if (index === stickersNameforProduct.length - 1) {
                        return `sticker=${item}`
                    } else {
                        return `sticker=${item}&`
                    }
                }).join(''),
                pagination: popularProductsPage
            }))
        }
    }, [listPopulatProducts])

    return (
        <section className="sect-popular-prod">
            <Container>
                <h1 className="sect-popular-product__title sect-title">Popular Products</h1>
                <ul className="sect-popular-product__list">
                    {
                        listPopulatProducts?.map((product, index) => {
                            if (listPopulatProducts.length - 1 === index) {
                                return <li
                                    ref={ref}
                                    key={index}
                                    className='sect-popular-prod__item'>
                                    <Product
                                        dataProduct={product}
                                    />
                                </li>
                            } else {
                                return <li
                                    key={index}
                                    className='sect-popular-prod__item'>
                                    <Product
                                        dataProduct={product}
                                    />
                                </li>
                            }


                        })
                    }
                </ul>
            </Container>
        </section>
    );
};

export default PopularProducts;