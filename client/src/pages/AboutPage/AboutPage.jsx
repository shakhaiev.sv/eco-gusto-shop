
import { useEffect } from 'react';
import Container from '../../layout/container/Container';
import '../AboutPage/AboutPage.scss'


const AboutPage = () => {

    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
    }, [])

    return (
        <section className='sect-about'>
            <Container>
                <div className='about--section'>
                    <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853250/about_t2pdsz.png" alt="aboutimg" className='about--img' />
                    <div className="about--chapter">
                        <h1 className='about--title'>Welcome to Gusto</h1>
                        <p className='about--text'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate id est laborum.</p>
                        <p className="about--text">Ius ferri velit sanctus cu, sed at soleat accusata. Dictas prompta et Ut placerat legendos
                            interpre.Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante Etiam sit amet
                            orci eget. Quis commodo odio aenean sed adipiscing. Turpis massa tincidunt dui ut ornare
                            lectus. Auctor elit sed vulputate mi sit amet. Commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate id est laborum.</p>
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853251/about2_dohnna.png" alt="about2 png" className='about--images' />
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853251/about3_wcsfma.png" alt="about3 png" className='about--images' />
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853254/about4_lqhlcb.png" alt="about4 png" className='about--images' />
                    </div>
                </div>
                <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853262/section2_s9pfpu.png" alt="section2 image" className='about--img__2' />
                <div className="about--section__2">
                    <div className="card">
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853255/card1_ounmxh.png" alt="img card 1" className='card--img' />
                        <h1 className="card--title">Best Prices & Offers</h1>
                        <p className="card--text">There are many variations of passages of Lorem
                            Ipsum available, but the majority have suffered
                            alteration in some form</p>
                        <a href="#" className='card--link'>Read more</a>
                    </div>
                    <div className="card">
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853257/card2_shbmr6.png" alt="img card 1" className='card--img' />
                        <h1 className="card--title">Wide Assortment</h1>
                        <p className="card--text">There are many variations of passages of Lorem
                            Ipsum available, but the majority have suffered
                            alteration in some form</p>
                        <a href="#" className='card--link'>Read more</a>
                    </div>
                    <div className="card">
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853258/card3_j9aidy.png" alt="img card 1" className='card--img' />
                        <h1 className="card--title">Free Delivery</h1>
                        <p className="card--text">There are many variations of passages of Lorem
                            Ipsum available, but the majority have suffered
                            alteration in some form</p>
                        <a href="#" className='card--link'>Read more</a>
                    </div>
                    <div className="card">
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853258/card4_bgplyo.png" alt="img card 1" className='card--img' />
                        <h1 className="card--title">Easy Returns</h1>
                        <p className="card--text">There are many variations of passages of Lorem
                            Ipsum available, but the majority have suffered
                            alteration in some form</p>
                        <a href="#" className='card--link'>Read more</a>
                    </div>
                    <div className="card">
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853260/card5_gdhhne.png" alt="img card 1" className='card--img' />
                        <h1 className="card--title">100% Satisfaction</h1>
                        <p className="card--text">There are many variations of passages of Lorem
                            Ipsum available, but the majority have suffered
                            alteration in some form</p>
                        <a href="#" className='card--link'>Read more</a>
                    </div>
                    <div className="card">
                        <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853261/card6_cd54v6.png" alt="img card 1" className='card--img' />
                        <h1 className="card--title">Great Daily Deal</h1>
                        <p className="card--text">There are many variations of passages of Lorem
                            Ipsum available, but the majority have suffered
                            alteration in some form</p>
                        <a href="#" className='card--link'>Read more</a>
                    </div>
                </div>
                <div className="about--section__3">
                    <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853254/about7_porocm.png" alt="about image" />
                    <div className="section3--conteiner">
                        <h3 className="section3--title">Our performance</h3>
                        <h1 className='section3--title__2'>Your Partner for e-
                            commerce grocery
                            solution</h1>
                        <p className="section3--text">Ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                            doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
                            inventore veritatis et quasi architecto</p>
                        <p className="section3--text__2">Pitatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
                            ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia</p>
                    </div>

                </div>
                <div className="section3--conteiner__2">
                    <div className='conteiner2--block'>
                        <h1 className='block--title'>Who we are</h1>
                        <p className='block--text'>Volutpat diam ut venenatis tellus in metus. Nec dui nunc
                            mattis enim ut tellus eros donec ac odio orci ultrices in.
                            ellus eros donec ac odio orci ultrices in.</p>
                    </div>
                    <div className='conteiner2--block'>
                        <h1 className='block--title'>Our history</h1>
                        <p className='block--text'>Volutpat diam ut venenatis tellus in metus. Nec dui nunc
                            mattis enim ut tellus eros donec ac odio orci ultrices in.
                            ellus eros donec ac odio orci ultrices in.</p>
                    </div>
                    <div className='conteiner2--block'>
                        <h1 className='block--title'>Our mission</h1>
                        <p className='block--text'>Volutpat diam ut venenatis tellus in metus. Nec dui nunc
                            mattis enim ut tellus eros donec ac odio orci ultrices in.
                            ellus eros donec ac odio orci ultrices in.</p>
                    </div>

                </div>

                <div className='sect-data-static'>
                    <ul className='sect-data-static__list'>
                        <li className='sect-data-static__item'><p className='sect-data-static__num'>0+</p> <span className='sect-data-static__text'>Glorious years</span></li>
                        <li className='sect-data-static__item'><p className='sect-data-static__num'>0+</p> <span className='sect-data-static__text' >Happy clients</span></li>
                        <li className='sect-data-static__item'><p className='sect-data-static__num'>0+</p> <span className='sect-data-static__text' >Projects complete</span></li>
                        <li className='sect-data-static__item'><p className='sect-data-static__num'>0+</p> <span className='sect-data-static__text' >Team advisor</span></li>
                        <li className='sect-data-static__item'><p className='sect-data-static__num'>0+</p> <span className='sect-data-static__text' >Products Sale</span></li>
                    </ul>
                </div>

            </Container>
        </section>
    );
}



export default AboutPage;