import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

import './NotFoundPage.scss'

const NotFoundPage = () => {

    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
    }, [])

    return (
        <section className="not-found-page">
            <div className="not-found-page__img-wrapper">
                <img src='https://res.cloudinary.com/dre55ftmq/image/upload/v1723055057/pagenotfound_ue0jkl.png' alt="Error 404" />
            </div>
            <div className="not-found-page__content">
                Page not found. Please, visit <Link to={"/"}>Home Page</Link>.
            </div>
        </section>
    );
};

export default NotFoundPage;