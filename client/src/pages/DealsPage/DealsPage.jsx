

import './DealsPage.scss'
import { useSelector, useDispatch } from "react-redux"
import React, { useEffect } from 'react';
import { selectorDeals } from "../../store/selectors/selectors.js"
import { actionFetchDeals } from "../../store/slices/deals.js"
import Container from '../../layout/container/Container.jsx';
import CardDeals from '../../components/CardDeals/CardDeals.jsx'

const DealsPage = () => {

    const dispatch = useDispatch();
    const dealsList = useSelector(selectorDeals);
    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })

        if (!dealsList.length) {
            dispatch(actionFetchDeals())
        }
    }, [])


    return (
        <section className='sect-deals-page'>
            <Container>
                <h1 className="sect-deals-page__title sect-title">Deals Of The Day</h1>
                <ul className="sect-deals-page__list">
                    {dealsList?.map((deals, index) => <li
                        key={index}
                        className='sect-deals-page__item'
                    >
                        <CardDeals
                            dataDeals={deals}
                        />
                    </li>
                    )}
                </ul>

            </Container>
        </section>
    );
};

DealsPage.propTypes = {};

export default DealsPage;