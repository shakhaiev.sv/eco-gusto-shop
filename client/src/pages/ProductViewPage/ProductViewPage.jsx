import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Stars from 'reactjs-star-rating';
import Button from '/src/components/Button/Button.jsx';
import CartIcon from '../../pictures/svg/deals/cart.svg?react';
import FavoritesIcon from '../../pictures/svg/product/favorite.svg?react';
import HomeIcon from '../../pictures/svg/breadcrumbs/homeIcon.svg?react'
import cn from 'classnames';
import Container from '../../layout/container/Container.jsx';
import { useDispatch, useSelector } from 'react-redux';
import SnackbarAlert from '../../components/SnackbarAlert/SnackbarAlert.jsx';
import { Swiper, SwiperSlide } from 'swiper/react';
import { FreeMode, Navigation, Thumbs } from 'swiper/modules';
import Reviews from '../../components/Reviews/Reviews.jsx'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';
import './ProductViewPage.scss';

import {
    actionAddProductToCartForAuthClient,
    actionAddCardToCartList,
    actionAllLengthCoodsOnCart
} from '../../store/slices/cart.js';
import {
    actionAddFavoriteList,
    actionAddProductToFavoritesForAuthClient,
    actionDeleteProductFromFavoritesAuthClient,
} from '../../store/slices/favorite.js';

import { fetchProductReviews } from '../../store/slices/reviewSlice.js'

import {
    selectorCurrentProduct,
    selectorToken,
    selectorListFavorite,
    selectorProductReviews,
} from '../../store/selectors/selectors.js';


import { actionFetchProdyctById } from '../../store/slices/products.js';


const ProductViewPage = () => {

    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const dispatch = useDispatch();
    const token = useSelector(selectorToken);
    const product = useSelector(selectorCurrentProduct);
    const { id } = useParams();
    const { _id } = product;
    const productReviews = useSelector(selectorProductReviews);
    const listFavorite = useSelector(selectorListFavorite);
    const [openSnackbarAlert, setOpenSnackbarAlert] = useState(false);

    const handleAddCardtoCart = () => {
        if (token) {
            dispatch(actionAddProductToCartForAuthClient({ token, idProduct: _id }));
        } else {
            dispatch(actionAddCardToCartList(product));
            dispatch(actionAllLengthCoodsOnCart());
        }
        setOpenSnackbarAlert(true);
    };

    const handleAddCardToFavorite = async () => {
        if (token) {
            if (listFavorite.some((item) => item._id === product._id)) {
                dispatch(actionDeleteProductFromFavoritesAuthClient({ token, idProduct: _id }));
            } else {
                dispatch(actionAddProductToFavoritesForAuthClient({ token, idProduct: _id }));
            }
        } else {
            dispatch(actionAddFavoriteList(product));
        }
    };


    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
        dispatch(actionFetchProdyctById(id));
    }, [dispatch, id]);


    useEffect(() => {
        dispatch(fetchProductReviews(id));
    }, []);


    return (
        <section className='sect-page-product'>
            <Container>
                {Object.keys(product).length > 0 && (
                    <>
                        <nav>
                            <ul className='breadcrumbs'>
                                <li className='breadcrumbs__home-page'>
                                    <Link className='breadcrumbs__home-link' to={`/`}>
                                        <HomeIcon />
                                    </Link>
                                </li>
                                <li className='breadcrumbs__category-name breadcrumb-item'>
                                    <Link className='breadcrumbs__category-link' to={`/products?categoryName=${product.categoryName}`}>
                                        {product.categoryName}
                                    </Link>
                                </li>
                                <li className='breadcrumbs__product-name breadcrumb-item'>{product.name}</li>
                            </ul>
                        </nav>
                        <div className='sect-page-product__content'>
                            <h1 className='product-page__name'>{product.name}</h1>
                            <div className='sect-page-product__wrap'>
                                <div className='product-page__img-wrap'>
                                    <Swiper
                                        style={{
                                            '--swiper-navigation-color': '#000',
                                            '--swiper-pagination-color': '#000',
                                        }}
                                        spaceBetween={10}
                                        navigation={true}
                                        thumbs={{ swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null }}
                                        modules={[FreeMode, Navigation, Thumbs]}
                                        className="product-page__slider mySwiper2"
                                    >
                                        {
                                            product.imageUrls?.map((image, index) =>
                                                <SwiperSlide
                                                    key={index}
                                                    className='product-page__slider-img'
                                                >
                                                    <img className='product-page__img' src={image} alt={name} />
                                                </SwiperSlide>
                                            )
                                        }
                                    </Swiper>
                                    <Swiper
                                        onSwiper={setThumbsSwiper}
                                        spaceBetween={10}
                                        slidesPerView={4}
                                        freeMode={true}
                                        watchSlidesProgress={true}
                                        modules={[FreeMode, Navigation, Thumbs]}
                                        className="mySwiper"
                                    >
                                        {
                                            product.imageUrls?.map((image, index) =>
                                                <SwiperSlide
                                                    key={index}
                                                    className='product-page__slider-img'
                                                >
                                                    <img className='product-page__img' src={image} alt={name} />
                                                </SwiperSlide>
                                            )
                                        }
                                    </Swiper>
                                </div>

                                <div className='product-page__desc'>
                                    <div className='product-page__desc-header'>
                                        <div className='product-page__reviews'><Stars
                                            className='starIcon'
                                            maxRating='5'
                                            size={20}
                                            defaultRating={product.reviews}
                                            color='#ffd700'
                                            readOnly={true}
                                            showLabel={false}
                                        />
                                            <span>{product.reviews} stars</span></div>
                                        <div>
                                            <Button
                                                type='button'
                                                className={cn('product-page__btn-favorite', {
                                                    'is-favorite': listFavorite.some((item) => item['_id'] === _id)
                                                })}
                                                onClick={handleAddCardToFavorite}
                                            >
                                                <span
                                                    className='product-page__btn-favorite-cta'>{(listFavorite.some((item) => item['_id'] === _id) ? "Saved" : "Save to favorites")}</span>
                                                <FavoritesIcon />
                                            </Button>
                                        </div>
                                    </div>
                                    <p className='product-page__article'>Item Number: <span>{product.itemNo}</span></p>
                                    <p className='product-page__category'>Category:
                                        <Link className='product-page__category-link'
                                            to={`/products?categoryName=${product.categoryName}`}>
                                            <span> {product.categoryName}</span>
                                        </Link>
                                    </p>
                                    <p className='product-page__country'>Country of
                                        origin: <span>{product.country}</span>
                                    </p>
                                    <div className='product-page__price'>
                                        <p className="product-page__currentprice">${product.currentPrice?.toFixed(2)}
                                            <span className='product-page__units'>{product.numberOfunits}</span></p>
                                        {product.previousPrice && <del
                                            className="product-page__previousprice">${product.previousPrice.toFixed(2)}</del>}
                                    </div>
                                    <p className={cn('product-page__quantity', { 'product-page__non-available': !product.quantity })}>
                                        <span>{product.quantity ? "In stock" : "Out of Stock"}</span></p>
                                    <Button
                                        type='button'
                                        className={cn("product-page__btn-add", { 'btn-add-disabled': !product.quantity })}
                                        onClick={handleAddCardtoCart}
                                        disabled={!product.quantity}
                                    >
                                        <CartIcon /><span>Add to cart</span>
                                    </Button>
                                </div>
                                {product.sticker && <p className={cn('product-page__sticker', {
                                    'sale': (product.sticker === 'Sale'),
                                    'new': (product.sticker === 'New'),
                                    'hot': (product.sticker === 'Hot')
                                })}>
                                    {product.sticker}
                                </p>}
                            </div>
                            <div className='product-page__info-wrap'>
                                <Tabs className='product-page__info'>
                                    <TabList className='product-page__info-nav'>
                                        <Tab className='product-page__info-nav-item'>Description</Tab>
                                        <Tab className='product-page__info-nav-item'>Characteristics</Tab>
                                        <Tab className='product-page__info-nav-item'>Reviews
                                            <span className="product-page__reviews-counter">
                                                {productReviews?.length}
                                            </span>
                                        </Tab>
                                    </TabList>

                                    <TabPanel className='product-page__product-description'>
                                        {product.description || <p> {product.name} came to us right from {product.country} and has a very delicious taste. We keep it in a temperature between {product.storageTemperature} degrees for not more then {product.shelfLife}. This product is waiting for you in the best conditions and always fresh. </p>}
                                        <br></br>
                                        {product.previousPrice && product.previousPrice > product.currentPrice ? <p>For the moment {product.name} is on Sale and you can buy it only for ${product.currentPrice} {product.numberOfunits}. Use this opportunity right now and buy {product.name} for special price.</p> : null}
                                    </TabPanel>
                                    <TabPanel className='product-page__product-characteristics'>
                                        <ul className='product-page__info-property'>
                                            <div className='product-page__property-title'>
                                                Nutritional properties, 100g
                                            </div>
                                            {Object.keys(product.nutritionFacts).map((item, index) => (
                                                <li key={index} className='product-page__property-info'>
                                                    <div>
                                                        <span>{item[0].toUpperCase() + item.slice(1)}</span>
                                                    </div>
                                                    <span>{product.nutritionFacts[item]}</span>
                                                </li>
                                            ))
                                            }
                                        </ul>

                                        <div className='product-page__info-general'>
                                            <div className='product-page__general-title'>General information</div>
                                            <div className='product-page__general-info'>
                                                <div>
                                                    <span>Brand</span>
                                                </div>
                                                <span>{product.brand}</span>
                                            </div>
                                            <div className='product-page__general-info'>
                                                <div>
                                                    <span>Shelf life</span>
                                                </div>
                                                <span>{product.shelfLife}</span></div>
                                            <div className='product-page__general-info'>
                                                <div>
                                                    <span>Storage temperature</span>
                                                </div>
                                                <span>{product.storageTemperature}</span></div>
                                            <div className='product-page__general-info'>
                                                <div>
                                                    <span>Country</span>
                                                </div>
                                                <span>{product.country}</span>
                                            </div>
                                        </div>
                                    </TabPanel>
                                    <TabPanel className='product-page__product-reviews'>
                                        <Reviews />
                                    </TabPanel>
                                </Tabs>
                            </div>
                        </div>
                        {openSnackbarAlert && <SnackbarAlert open={openSnackbarAlert} setOpen={setOpenSnackbarAlert} />}
                    </>
                )}
            </Container>
        </section>
    );
};

export default ProductViewPage;