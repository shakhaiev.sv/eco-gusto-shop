// components/UserPage/UserPage.jsx
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateUserData, resetState } from '../../store/slices/userSlice';

import './UserPage.scss';
const EditIcon = () => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="24" height="24">
        <path d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160L0 416c0 53 43 96 96 96l256 0c53 0 96-43 96-96l0-96c0-17.7-14.3-32-32-32s-32 14.3-32 32l0 96c0 17.7-14.3 32-32 32L96 448c-17.7 0-32-14.3-32-32l0-256c0-17.7 14.3-32 32-32l96 0c17.7 0 32-14.3 32-32s-14.3-32-32-32L96 64z" />
    </svg>
);

const UpdateIcon = () => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="24" height="24">
        <path d="M438.6 105.4c12.5 12.5 12.5 32.8 0 45.3l-256 256c-12.5 12.5-32.8 12.5-45.3 0l-128-128c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0L160 338.7 393.4 105.4c12.5-12.5 32.8-12.5 45.3 0z" />
    </svg>
);
const UserPage = () => {
    const dispatch = useDispatch();
    const {
        id,
        firstName,
        lastName,
        email,
        login,
        gender,
        telephone,
        loading,
        error,
        successMessage
    } = useSelector((state) => state.user);
    const [editable, setEditable] = useState(false);
    const [localUserData, setLocalUserData] = useState({
        id,
        firstName,
        lastName,
        email,
        login,
        gender,
        telephone
    });

    useEffect(() => {
        return () => {
            dispatch(resetState());
        };
    }, [dispatch]);

    useEffect(() => {
        setLocalUserData({
            id,
            firstName,
            lastName,
            email,
            login,
            gender,
            telephone
        });
    }, [id, firstName, lastName, email, login, gender, telephone]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setLocalUserData(prevData => ({ ...prevData, [name]: value }));
    };

    const handleUpdate = () => {
        dispatch(updateUserData(localUserData));
        setEditable(false);
    };


    if (error) return <p className="error-message">Error: {error}</p>;

    return (
        <div className="user-page">
            <div className="title">User Page
                <div className="button">
                    {editable ? (
                        <div className="update-icon" onClick={handleUpdate}>
                            <UpdateIcon />
                        </div>
                    ) : (
                        <div className="edit-icon" onClick={() => setEditable(true)}>
                            <EditIcon />
                        </div>
                    )}
                </div>
            </div>

            <div className="content">
                <form>
                    <div className="user-details">
                        <div className="input-box">
                            <span className="details">First Name</span>
                            <input
                                type="text"
                                name="firstName"
                                value={localUserData.firstName}
                                onChange={handleInputChange}
                                readOnly={!editable}
                            />
                        </div>
                        <div className="input-box">
                            <span className="details">Last Name</span>
                            <input
                                type="text"
                                name="lastName"
                                value={localUserData.lastName}
                                onChange={handleInputChange}
                                readOnly={!editable}
                            />
                        </div>
                        <div className="input-box">
                            <span className="details">Email</span>
                            <input
                                type="email"
                                name="email"
                                value={localUserData.email}
                                onChange={handleInputChange}
                                readOnly={!editable}
                            />
                        </div>
                        <div className="input-box">
                            <span className="details">Login</span>
                            <input
                                type="text"
                                name="login"
                                value={localUserData.login}
                                onChange={handleInputChange}
                                readOnly={!editable}
                            />
                        </div>
                        <div className="input-box">
                            <span className="details">Phone</span>
                            <input
                                type="tel"
                                name="telephone"
                                value={localUserData.telephone}
                                onChange={handleInputChange}
                                readOnly={!editable}
                            />
                        </div>
                        <div className="input-box gender-box">
                            <span className="details">Gender</span>
                            <label htmlFor="dot-1">
                                <input
                                    type="radio"
                                    name="gender"
                                    id="dot-1"
                                    value="male"
                                    checked={localUserData.gender === 'male'}
                                    onChange={handleInputChange}
                                    disabled={!editable}
                                />
                                <span className="dot one"></span>
                                <span className="gender">Male</span>
                            </label>
                            <label htmlFor="dot-2">
                                <input
                                    type="radio"
                                    name="gender"
                                    id="dot-2"
                                    value="female"
                                    checked={localUserData.gender === 'female'}
                                    onChange={handleInputChange}
                                    disabled={!editable}
                                />
                                <span className="dot two"></span>
                                <span className="gender">Female</span>
                            </label>
                            <label htmlFor="dot-3">
                                <input
                                    type="radio"
                                    name="gender"
                                    id="dot-3"
                                    value="other"
                                    checked={localUserData.gender === 'other'}
                                    onChange={handleInputChange}
                                    disabled={!editable}
                                />
                                <span className="dot three"></span>
                                <span className="gender">Other</span>
                            </label>
                        </div>
                    </div>
                </form>
                {successMessage && <p className="success-message">{successMessage}</p>}
            </div>
        </div>
    );
};


export default UserPage;
