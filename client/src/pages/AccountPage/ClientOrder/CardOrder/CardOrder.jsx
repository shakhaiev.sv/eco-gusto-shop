import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import './CardOrder.scss'
import Button from '../../../../components/Button/Button';
import Arrow from '../../../../pictures/svg/goodsPage/arrow.svg?react'
import Close from '../../../../pictures/svg/modal/close.svg?react'
import MiniCard from '../../../../components/MiniCard/MiniCard';
import cn from 'classnames'
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper/modules';
import { ModalContext } from '../../../../context/ModalContext.jsx';
import { selectorAdmin, selectorToken, } from '../../../../store/selectors/selectors.js'
import { useSelector, useDispatch } from 'react-redux';
import CheckMark from '../../../../pictures/svg/client/check.svg?react'
import { actionUpdateOrderStatusOnShipped, actionDeleteOrderForAdmin } from '../../../../store/slices/order.js'


const CardOrder = (props) => {
    const dispatch = useDispatch();
    const token = useSelector(selectorToken)
    const admin = useSelector(selectorAdmin)
    const [showDescOrder, setShowDescOrder] = useState(false)
    const { dataoOrder, setOrderCanceled } = props

    const { fnShowModalOrderCanceled } = useContext(ModalContext)
    const year = new Date(dataoOrder.date).getFullYear()
    const month = (new Date(dataoOrder.date).getMonth() < 9) ? `0${new Date(dataoOrder.date).getMonth() + 1}` : `${new Date(dataoOrder.date).getMonth() + 1}`
    const day = (new Date(dataoOrder.date).getDate() < 11) ? `0${new Date(dataoOrder.date).getDate()}` : `${new Date(dataoOrder.date).getDate()}`
    const hours = new Date(dataoOrder.date).getHours()
    const minutes = new Date(dataoOrder.date).getMinutes()



    const handleShowDescOrder = () => {
        setShowDescOrder(!showDescOrder)
    }

    const handleShippedOrderClient = (e) => {
        dispatch(actionUpdateOrderStatusOnShipped({ token, orderId: dataoOrder._id }))
    }

    const handleModalOrderCanceled = (dataoOrder) => {
        fnShowModalOrderCanceled()
        setOrderCanceled(dataoOrder)
    }

    const handleDeleteOrderForAdmin = (e) => {
        dispatch(actionDeleteOrderForAdmin({ token, orderId: dataoOrder._id }))
    }

    return (
        <>
            <div className='client-order__card-content'>
                <div className='client-order__card-head'>
                    <div className='client-order__card-data'>
                        <p className='client-order__card-text'><strong>Order date:</strong> <span>{`${day}-${month}-${year} ${hours}:${minutes}`}</span></p>
                        <p className='client-order__card-text' ><strong>Order number: </strong><span>{dataoOrder.orderNo}</span></p>
                        {dataoOrder.canceled ? <p
                            className='client-order__card-canceled'
                        ><strong>Order canceled </strong>
                        </p> : <p
                            className='client-order__card-status' ><strong>Status order: </strong><span>{dataoOrder.status}</span></p>

                        }
                        <Button
                            onClick={handleShowDescOrder}
                            className={cn('client-order__btn-show', { 'btn-hide': showDescOrder })}
                            type='button'><Arrow /></Button>
                    </div>
                    <div className={cn('client-order__card-desc', { 'card-desc-open': showDescOrder })}>
                        <h3 className='client-order__card-title'>List of purchased goods: </h3>
                        <Swiper
                            style={{
                                '--swiper-navigation-color': '#3BB77E'
                            }}
                            slidesPerView={2.2}
                            spaceBetween={10}
                            modules={[Navigation]}
                            navigation={true}
                            className="client-order__card-slider"
                        >
                            {dataoOrder.products.map((item, index) => <SwiperSlide
                                key={index}
                                className='client-order__card-product'>
                                <MiniCard
                                    dataProduct={item.product}
                                    linkNot={true}
                                />
                                <p className='client-order__card-text'><strong>Quantity: </strong><span>{item.cartQuantity}</span></p>
                            </SwiperSlide>
                            )}
                        </Swiper>
                        <div className='client-order__card-info'>
                            <h3 className='client-order__card-title'>Contact details:</h3>
                            <p className='client-order__card-text'><strong>Order amount: </strong><span>{`$${dataoOrder.totalSum.toFixed(2)}`}</span></p>
                            <p className='client-order__card-text'><strong>Delivery address: </strong><span>{`city ${dataoOrder.deliveryAddress.city} street ${dataoOrder.deliveryAddress.address}`}</span></p>
                            <p className='client-order__card-text'><strong>FullName: </strong><span>{dataoOrder.customerId ? `${dataoOrder.customerId?.firstName} ${dataoOrder.customerId?.lastName}` : `${dataoOrder.customer?.firstName} ${dataoOrder.customer?.lastName}`}</span></p>
                            <p className='client-order__card-text'><strong>Email: </strong><span>{dataoOrder.email}</span></p>
                            <p className='client-order__card-text'><strong>Phone: </strong><span>{dataoOrder.mobile}</span></p>

                        </div>
                    </div>

                </div>
                {admin ? <>
                    {
                        (dataoOrder.status === 'not shipped' && !dataoOrder.canceled) &&
                        <div className='sect-client-order__btn-wrapper'>
                            <Button
                                title='Done'
                                type='button'
                                className='client-order__btn-shipped'
                                onClick={handleShippedOrderClient}
                            >
                                <CheckMark />
                            </Button>
                            <Button
                                title='Remove order'
                                type='button'
                                className='client-order__btn-cancel'
                                onClick={handleDeleteOrderForAdmin}
                            >
                                <Close />
                            </Button>
                        </div>
                    }</> : <>
                    {!dataoOrder.canceled && dataoOrder.status !== 'shipped' &&
                        <Button
                            title='Order canceled'
                            type='button'
                            className='client-order__btn-cancel'
                            onClick={() => handleModalOrderCanceled(dataoOrder)}
                        ><Close /></Button>
                    } </>
                }
            </div>
        </>
    );
};

CardOrder.propTypes = {};

export default CardOrder;