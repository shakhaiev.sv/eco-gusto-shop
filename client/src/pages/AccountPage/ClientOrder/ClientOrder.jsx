import React, { useEffect, useContext, useState } from 'react';
import './ClientOrder.scss'
import CardOrder from '../ClientOrder/CardOrder/CardOrder.jsx'
import { selectorToken, selectorAllOrderClient, } from '../../../store/selectors/selectors.js'
import { useSelector, useDispatch } from 'react-redux'
import { actionGetOrderClient } from '../../../store/slices/order.js'
import { ModalContext } from '../../../context/ModalContext.jsx'
import ModalOrderCanceled from '../../../components/ModalOrderCanceled/ModalOrderCanceled.jsx';


const ClientOrder = () => {
    const [firstRequest, setFirstRequest] = useState(true)
    const [orderCanceled, setOrderCanceled] = useState({})
    const { showModalOrderCanceled, } = useContext(ModalContext)
    const token = useSelector(selectorToken)
    const orderClient = useSelector(selectorAllOrderClient)

    const dispatch = useDispatch();

    useEffect(() => {
        if (!orderClient.length && firstRequest) {
            dispatch(actionGetOrderClient(token))
        }
        setFirstRequest(false)
    }, [orderClient])



    return (
        <section className='sect-client-order'>
            <div className='sect-client-order__wrap'>
                <h1 className='sect-client-order__title'>My order</h1>
                {
                    orderClient.length ? <>
                        <ul className='sect-client-order__list'>
                            {orderClient.map((item, index) => {
                                return <li className='sect-client-order__item' key={index}>
                                    <CardOrder
                                        setOrderCanceled={setOrderCanceled}
                                        orderCanceled={orderCanceled}
                                        dataoOrder={item}
                                    />
                                </li>
                            })}

                        </ul>
                    </> : <p className='sect-admin-order__is-data'>
                        Not data available!
                    </p>
                }

            </div>
            {showModalOrderCanceled && <ModalOrderCanceled
                orderCanceled={orderCanceled}
                setOrderCanceled={setOrderCanceled}
            />}
        </section>
    );
};

ClientOrder.propTypes = {};

export default ClientOrder;