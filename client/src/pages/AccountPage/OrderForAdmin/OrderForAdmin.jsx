import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { selectorToken, selectorOrderClientForAdmin, selectorLoadingOrder, } from '../../../store/selectors/selectors.js'
import { actionGetAllOrders, actionisLoading } from '../../../store/slices/order.js'
import CardOrder from '../ClientOrder/CardOrder/CardOrder.jsx';
import FilterForOrder from './FilterForOrder/FilterForOrder.jsx';
import Loader from '../../../components/Loader/Loader.jsx';
import cn from 'classnames'

import './OrderForAdmin.scss'

const OrderForAdmin = () => {
    const [firstRequest, setFirstRequest] = useState(true)
    const [isLoader, setIsLoader] = useState(true)

    const token = useSelector(selectorToken)
    const dispatch = useDispatch()
    const orderClientForAdmin = useSelector(selectorOrderClientForAdmin)
    const loading = useSelector(selectorLoadingOrder)

    const today = new Date();

    const futureDate = new Date();
    futureDate.setDate(today.getDate() + 2);

    const formatDate = (date) => {
        const year = date.getFullYear();
        const month = (date.getMonth() < 9) ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;
        const day = (date.getDate() < 10) ? `0${date.getDate()}` : `${date.getDate()}`;
        return `${year}-${month}-${day}`;
    };

    const currentDate = formatDate(today);
    const datePlusTwo = formatDate(futureDate);

    useEffect(() => {
        if (loading) {
            const timerLoading = setTimeout(() => {
                setIsLoader(false)
                dispatch(actionisLoading(false))
            }, 1000)

            return () => clearTimeout(timerLoading)
        }

        if (orderClientForAdmin.length) {
            setIsLoader(false)
        }

    }, [loading])


    useEffect(() => {
        if (firstRequest && token && !orderClientForAdmin.length) {
            dispatch(actionGetAllOrders({
                token, dataFilter: {
                    dateFrom: currentDate,
                    dateTo: datePlusTwo,
                }
            }))
            setFirstRequest(false)
        }
    }, [orderClientForAdmin, token])

    return (
        <section className='sect-admin-order '>
            <h1 className='sect-client-order__title'>Order client</h1>
            <FilterForOrder
                setLoader={setIsLoader}
                currentDate={currentDate}
                datePlusTwo={datePlusTwo}

            />
            <div className={cn('sect-admin-order__data ', { 'is-loader': isLoader })}>
                {isLoader ? <Loader /> : <>
                    {
                        orderClientForAdmin.length ? <ul className='sect-admin-order__list'>
                            {
                                orderClientForAdmin?.map(((item, index) => <li
                                    className='sect-admin-order__content-item'
                                    key={index}>
                                    <CardOrder

                                        dataoOrder={item}
                                    />
                                </li>
                                ))
                            }
                        </ul> : <p className='sect-admin-order__is-data'>
                            Not data available!
                        </p>
                    }
                </>

                }
            </div>





        </section>
    );
};

OrderForAdmin.propTypes = {};

export default OrderForAdmin;