import React from 'react';
import PropTypes from 'prop-types';
import { Formik, Form, Field } from 'formik';
import DateInput from '../../../../components/Form/DateInput/DateInput.jsx'
import validation from './validation/validationSchema.js'
import Button from '../../../../components/Button/Button.jsx';
import './FilterForOrder.scss'
import cn from 'classnames'
import { selectorAdmin, selectorToken, selectorDateFilterOrderCient } from '../../../../store/selectors/selectors.js'
import { useSelector, useDispatch } from 'react-redux';
import { actionGetAllOrders, actiionSaveDateFilterOrder } from '../../../../store/slices/order.js'


const FilterForOrder = (props) => {

    const dispatch = useDispatch();
    const { currentDate, datePlusTwo, setLoader } = props
    const admin = useSelector(selectorAdmin)
    const token = useSelector(selectorToken)
    const dateFilter = useSelector(selectorDateFilterOrderCient)

    const intialValues = (Object.keys(dateFilter).length) ? dateFilter : {
        dateFrom: currentDate,
        dateTo: datePlusTwo,
        statusDelivery: ''
    }

    return (
        <Formik
            initialValues={intialValues}
            validationSchema={validation}
            onSubmit={(values) => {
                setLoader(true)
                dispatch(actionGetAllOrders({
                    token, dataFilter: {
                        dateFrom: values.dateFrom,
                        dateTo: values.dateTo,
                        statusDelivery: (values.statusDelivery === 'canceled') ? { canceled: true } : { status: values.statusDelivery, canceled: false }
                    }
                }))
                dispatch(actiionSaveDateFilterOrder(values))
            }}
        >
            {({ errors, touched, }) => (
                <Form
                    className='form-filter-order'
                >
                    <div className='form-filter-order__wrapper'>
                        <div className='form-filter-order__wrap'>
                            <DateInput
                                classNameLabel='form-filter-order__label'
                                className='form-filter-order__date'
                                name='dateFrom'
                                error={errors.dateFrom && touched.dateFrom}
                            >
                                <span>Date from</span>
                            </DateInput>
                        </div >
                        <div className='form-filter-order__wrap'>
                            <DateInput
                                classNameLabel='form-filter-order__label'
                                className='form-filter-order__date'
                                name='dateTo'
                                error={errors.dateTo && touched.dateTo}
                            ><span>Date to</span>

                            </DateInput>
                        </div >
                        {(admin) && <div className='form-filter-order__wrap wrap-select '>
                            <label className={cn({ 'has-valid': (errors.statusDelivery && touched.statusDelivery) })} htmlFor="statusDelivery"><span>Status delivery</span></label>
                            <Field
                                className={cn('form-filter-order__select', { 'has-valid': (errors.statusDelivery && touched.statusDelivery) })}
                                as='select'
                                name='statusDelivery'
                                id='statusDelivery'
                            >
                                <option value="0">Select status delivery</option>
                                <option value="not shipped">Not shipped</option>
                                <option value="shipped">Shipped</option>
                                <option value="canceled">Canceled</option>
                            </Field>

                        </div>
                        }
                    </div>

                    <div className='form-filter-order__btn-wrap'>
                        <Button
                            type='submit'
                            className='form-filter-order__btn'
                        >
                            Get Data
                        </Button>
                    </div>
                </Form>
            )}
        </Formik >
    );
};

FilterForOrder.propTypes = {};

export default FilterForOrder;