
import React, { useEffect, useState, useContext } from 'react';
import UserPage from './UserPage';
import UserInformation from './UserInformation';
import ClientOrder from './ClientOrder/ClientOrder.jsx';
import './AccountPage.scss';
import { useSelector, useDispatch } from 'react-redux';
import { selectorAdmin, selectorToken } from '../../store/selectors/selectors.js'
import StatisticPage from '../StatisticPage/StatisticPage.jsx';
import Button from '../../components/Button/Button.jsx';
import { actionLogOut } from '../../store/slices/clients.js'
import { actionLogOutUser } from '../../store/slices/userSlice.js'
import { actionUpdateDataFromOrder } from '../../store/slices/order.js'
import { useNavigate, useLocation } from 'react-router-dom';
import OrderForAdmin from './OrderForAdmin/OrderForAdmin.jsx'
import { ModalContext } from '../../context/ModalContext.jsx';


const AccountPage = () => {
    const { fnShowModalAuth } = useContext(ModalContext)
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const token = useSelector(selectorToken)
    const location = useLocation()
    const ADMIN = useSelector(selectorAdmin)
    const [activePage, setActivePage] = useState('userPage');
    useEffect(() => {
        if (location.pathname === '/account' && !token) {
            const timer = setTimeout(() => {
                fnShowModalAuth()
            }, 1000)
            return () => clearTimeout(timer)
        }
    }, [token])

    const handleLogOut = () => {
        dispatch(actionLogOut())
        dispatch(actionLogOutUser())
        dispatch(actionUpdateDataFromOrder(undefined))
        navigate('/')
    }

    return (
        <div className="account-page">
            <div className="sidebar">
                <button onClick={() => setActivePage('userPage')}>User Information</button>
                {ADMIN ? <Button
                    type='button'
                    onClick={() => setActivePage('ClientOrderForAdmin')}>
                    Client Order
                </Button > : <Button
                    type='button'
                    onClick={() => setActivePage('ClientOrder')}
                >My Order</Button >}

                {ADMIN && <>
                    <button onClick={() => setActivePage('statisticPage')}>Statistic Page</button>
                </>

                }
                <Button type='button' onClick={handleLogOut}> Log out </Button>
            </div>
            <div className="content">
                {activePage === 'userPage' && <UserPage />}
                {activePage === 'userInformation' && <UserInformation />}
                {activePage === 'ClientOrder' && <ClientOrder />}
                {ADMIN && <>
                    {activePage === 'statisticPage' && <StatisticPage />}
                    {activePage === 'ClientOrderForAdmin' && <OrderForAdmin />}
                </>
                }
            </div>
        </div >
    );
};

export default AccountPage;
