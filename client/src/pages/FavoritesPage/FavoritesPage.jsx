
import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Product from '../../components/Product/Product.jsx';
import Container from "../../layout/container/Container.jsx";
import { selectorListFavorite } from "../../store/selectors/selectors.js";
import { actionSetListFavoriteFromLocalStorage } from "../../store/slices/favorite.js";
import "./FavoritesPage.scss";
import Button from "../../components/Button/Button.jsx";

const FavoritesPage = () => {
    const dispatch = useDispatch();
    const favoriteProducts = useSelector(selectorListFavorite) || [];


    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
        dispatch(actionSetListFavoriteFromLocalStorage());
    }, [dispatch]);


    return (
        <section className="sect-favorite">
            <Container>
                {
                    favoriteProducts.length ? <>
                        <h1 className="sect-favorite__title">Favorite Products</h1>
                        <ul className="sect-favorite__list">
                            {favoriteProducts?.map((product, index) => (
                                <li key={index} className="sect-favorite__item">
                                    <Product dataProduct={product} />
                                </li>)
                            )}
                        </ul>
                    </>
                        : <div className="sect-favorite__empty">
                            <p className="sect-favorite__empty-text">
                                Oops! No favorite products, check out our  <Button to='/products' >best products</Button> </p>
                            <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1722772652/EmptyCart_dcihgy.png" alt="empty cart" />
                        </div>

                }

            </Container>
        </section>
    );
};

export default FavoritesPage;
