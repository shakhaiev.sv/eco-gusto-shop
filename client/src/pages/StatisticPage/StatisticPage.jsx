import React, { useEffect, useState } from "react";
import './StatisticPage.scss';
import Container from "../../layout/container/Container";
import StatisticIcon from '../../pictures/svg/statisticPage/statistics.svg?react'
import { useDispatch, useSelector } from "react-redux";
import { selectorStatisticData, selectorToken, selectorOrdersStatuses } from "../../store/selectors/selectors.js";
import { actionGetAllOrdersForStatistic } from "../../store/slices/order.js";
import FilterStatistic from "./FilterStatistic/FilterStatistic.jsx";

const StatisticPage = () => {
    const [firstRequest, setFirstRequest] = useState(true)
    const token = useSelector(selectorToken)

    const dispatch = useDispatch()
    const statisticData = useSelector(selectorStatisticData);
    const ordersStatuses = useSelector(selectorOrdersStatuses)

    const today = new Date();

    const futureDate = new Date();
    futureDate.setDate(today.getDate() + 2);

    const formatDate = (date) => {
        const year = date.getFullYear();
        const month = (date.getMonth() < 9) ? `0${date.getMonth() + 1}` : `${date.getMonth() + 1}`;
        const day = (date.getDate() < 10) ? `0${date.getDate()}` : `${date.getDate()}`;
        return `${year}-${month}-${day}`;
    };


    const currentDate = formatDate(today);
    const datePlusTwo = formatDate(futureDate);


    useEffect(() => {
        if (firstRequest && token && !Object.keys(statisticData).length) {
            dispatch(actionGetAllOrdersForStatistic({
                token, dataFilter: {
                    dateFrom: currentDate,
                    dateTo: datePlusTwo,
                }
            }))
            setFirstRequest(false)
        }

    }, [statisticData, ordersStatuses])

    const { minOrders, maxOrders, totalOrders, totalSumOfAllOrders, averageCheck } = statisticData;
    const { all, notShipped, canceled } = ordersStatuses;

    return (
        <div className="statistic-page">
            <Container>
                <div className="statistic-page-title">
                    <h1>Statistics</h1>
                    <StatisticIcon />
                </div>

                <FilterStatistic
                    currentDate={currentDate}
                    datePlusTwo={datePlusTwo}
                />

                <div className="statistic-cards">
                    <div className="basic-info">
                        <div className="statistic-card total-orders">
                            <h2>Total Orders </h2>
                            <p>{totalOrders}</p>
                        </div>
                        <div className="statistic-card total-sum">
                            <h2>Total Sum</h2>
                            <p>${totalSumOfAllOrders}</p>

                        </div>
                        <div className="statistic-card average-check">
                            <h2>Average Check</h2>
                            <p>${averageCheck}</p>
                        </div>
                    </div>

                    <div className="additional-info">
                        <div className="statistic-card highest-order">
                            <h2>Highest Order</h2>
                            <p>${maxOrders}</p>
                        </div>
                        <div className="statistic-card lowest-order">
                            <h2>Lowest Order</h2>
                            <p>${minOrders}</p>
                        </div>
                    </div>
                </div>
                <div className="orders-statuses">
                    <div className="statistic-card not-shipped">
                        <h2>Not Shipped</h2>
                        <p>
                            {notShipped}
                        </p>
                    </div>
                    <div className="statistic-card canceled">
                        <h2>Canceled</h2>
                        <p>
                            {canceled}
                        </p>
                    </div>
                    <div className="statistic-card all">
                        <h2>All</h2>
                        <p>
                            {all}
                        </p>
                    </div>
                </div>
            </Container>
        </div>
    );
};

export default StatisticPage;