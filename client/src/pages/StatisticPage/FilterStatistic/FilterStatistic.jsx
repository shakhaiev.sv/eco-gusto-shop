import React from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import DateInput from '../../../components/Form/DateInput/DateInput.jsx'
import validation from './validation/validationSchema.js'
import Button from '../../../components/Button/Button.jsx';
import { selectorToken, selectorDateFilterStatistic } from '../../../store/selectors/selectors.js'
import { useSelector, useDispatch } from 'react-redux';
import { actionGetAllOrdersForStatistic, actiionSaveDateFilterStatictic } from '../../../store/slices/order.js'

const FilterStatistic = (props) => {
    const { currentDate, datePlusTwo } = props
    const token = useSelector(selectorToken)
    const dispatch = useDispatch();
    const dateFilterStatistic = useSelector(selectorDateFilterStatistic)
    const intialValues = (Object.keys(dateFilterStatistic).length) ? dateFilterStatistic : {
        dateFrom: currentDate,
        dateTo: datePlusTwo,
    }

    return (
        <Formik
            initialValues={intialValues}
            validationSchema={validation}
            onSubmit={(values) => {
                dispatch(actionGetAllOrdersForStatistic({
                    token, dataFilter: {
                        dateFrom: values.dateFrom,
                        dateTo: values.dateTo,
                    }
                }))
                dispatch(actiionSaveDateFilterStatictic(values))
            }}
        >
            {({ errors, touched, }) => (
                <Form
                    className='form-filter-order'
                >
                    <div className='form-filter-order__wrapper'>
                        <div className='form-filter-order__wrap'>
                            <DateInput
                                classNameLabel='form-filter-order__label'
                                className='form-filter-order__date'
                                name='dateFrom'
                                error={errors.dateFrom && touched.dateFrom}
                            >
                                <span>Date from</span>
                            </DateInput>
                        </div >
                        <div className='form-filter-order__wrap'>
                            <DateInput
                                classNameLabel='form-filter-order__label'
                                className='form-filter-order__date'
                                name='dateTo'
                                error={errors.dateTo && touched.dateTo}
                            ><span>Date to</span>

                            </DateInput>
                        </div >
                    </div>
                    <div className='form-filter-order__btn-wrap'>
                        <Button
                            type='submit'
                            className='form-filter-order__btn'
                        >
                            Get Data
                        </Button>
                    </div>
                </Form>
            )}
        </Formik >
    );
};

FilterStatistic.propTypes = {};

export default FilterStatistic;