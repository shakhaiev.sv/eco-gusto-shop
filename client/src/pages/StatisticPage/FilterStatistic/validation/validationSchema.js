import * as yup from 'yup'
const validationSchema = yup.object({

    dateFrom: yup
        .date()
        .required(' '),
    dateTo: yup
        .date()
        .min(yup.ref('dateFrom'), 'Date to cannot be before date from')
        .required(' '),


})

export default validationSchema 