import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Main from '../../components/Main/Main.jsx'
const HomePage = () => {

    useEffect(() => {

        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
    }, [])


    return (
        <Main />
    );
};

HomePage.propTypes = {};

export default HomePage;