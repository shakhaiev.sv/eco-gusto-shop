import React, { useEffect } from 'react';
import './ContactPage.scss'
import ContactInfo from './section/contactInfo/ContactInfo.jsx';
import ContactForm from './section/contactForm/ContactForm.jsx';


const ContactPage = () => {
    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
    }, [])
    return (
        <>
            <ContactInfo />
            <ContactForm />
        </>
    );
};

ContactPage.propTypes = {};

export default ContactPage;