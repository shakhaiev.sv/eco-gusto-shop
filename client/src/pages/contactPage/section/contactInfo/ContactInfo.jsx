import React from 'react';
import Container from '../../../../layout/container/Container.jsx'
import './ContactInfo.scss'
import Button from '../../../../components/Button/Button.jsx';
import Map from '../../../../pictures/svg/contact/map.svg?react'



const ContactInfo = () => {
    return (
        <section className='sect-contact'>
            <Container>
                <div className='sect-contact__container'>
                    <h1 className='sect-contact__title'>How can help you ?</h1>
                    <div className='sect-contact__wrap'>
                        <div className='sect-contact__desc'>
                            <h2 className='sect-contact__subtitle' >
                                Let us know how we can help you
                            </h2>
                            <p className='sect-contact__text'>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                            </p>
                            <p className='sect-contact__text'>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                            </p>
                        </div>
                        <ul className='sect-contact__list'>
                            <li className='sect-contact__item' >
                                <a href='#' className='sect-contact__item-title' >
                                    Visit Feedback
                                </a>
                                <p className='sect-contact__item-text'>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                </p>
                            </li>
                            <li className='sect-contact__item' >
                                <a href='#' className='sect-contact__item-title' >
                                    Employer Services
                                </a>
                                <p className='sect-contact__item-text'>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                </p>
                            </li>
                            <li className='sect-contact__item' >
                                <a href='#' className='sect-contact__item-title' >
                                    Billing Inquiries
                                </a>
                                <p className='sect-contact__item-text'>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                </p>
                            </li>
                            <li className='sect-contact__item' >
                                <a href='#' className='sect-contact__item-title' >
                                    General Inquiries
                                </a>
                                <p className='sect-contact__item-text'>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className='sect-contact__map'>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.3148766198733!2d-87.62614042354059!3d41.88608486491361!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e2ca8b34afe61%3A0x6caeb5f721ca846!2s205%20N%20Michigan%20Ave%20Suit%20810%2C%20Chicago%2C%20IL%2060601%2C%20%D0%A1%D0%A8%D0%90!5e0!3m2!1sru!2sua!4v1719775606143!5m2!1sru!2sua"
                        width="100%"
                        height="350" style={{ border: 0, borderRadius: '15px' }} loading="lazy"></iframe>
                </div>
                <div className='sect-contact__container'>
                    <ul className='sect-contact__address'>
                        <li className='sect-contact__address-item'>
                            <h4 className='sect-contact__address-title'>Office</h4>
                            <p className='sect-contact__address-info'>205 North Michigan Avenue, Suite 810 Chicago, 60601, USA</p>
                            <a className='sect-contact__address-phone' href="tel:(123)4567890">Phone: (123) 456-7890</a>
                            <a className='sect-contact__address-email' href="mailto:contact@Evara.com">Email: contact@Evara.com</a>
                            <Button
                                type='button'
                                className='btn-map' >
                                <Map />
                                <span>View map</span>
                            </Button>
                        </li>
                        <li className='sect-contact__address-item'>
                            <h4 className='sect-contact__address-title'>Studio</h4>
                            <p className='sect-contact__address-info'>205 North Michigan Avenue, Suite 810 Chicago, 60601, USA</p>
                            <a className='sect-contact__address-phone' href="tel:(123)4567890">Phone: (123) 456-7890</a>
                            <a className='sect-contact__address-email' href="mailto:contact@Evara.com">Email: contact@Evara.com</a>
                            <Button
                                type='button'
                                className='btn-map'
                            >
                                <Map />
                                <span>View map</span>
                            </Button>
                        </li>
                        <li className='sect-contact__address-item'>
                            <h4 className='sect-contact__address-title'>Shop</h4>
                            <p className='sect-contact__address-info' >205 North Michigan Avenue, Suite 810 Chicago, 60601, USA</p>
                            <a className='sect-contact__address-phone' href="tel:(123)4567890">Phone: (123) 456-7890</a>
                            <a className='sect-contact__address-email' href="mailto:contact@Evara.com">Email: contact@Evara.com</a>
                            <Button
                                type='button'
                                className='btn-map' >
                                <Map />
                                <span>View map</span>
                            </Button>
                        </li>
                    </ul>
                </div>
            </Container>
        </section>
    );
};

ContactInfo.propTypes = {};



export default ContactInfo;