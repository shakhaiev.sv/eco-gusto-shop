import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Container from '../../../../layout/container/Container.jsx';
import { Formik, Form } from 'formik'
import Input from '../../../../components/Form/Input/Input.jsx';
import validation from './validationSchema/validation.js'
import Button from '../../../../components/Button/Button.jsx'
import { useDispatch, useSelector } from 'react-redux'
import { selectorContactForm } from '../../../../store/selectors/selectors.js'
import { actionSubmitFormContact } from '../../../../store/slices/clients.js'
import TextArea from '../../../../components/Form/TextArea/TextArea.jsx';
import './ContactForm.scss'

const ContactForm = () => {
    const dispatch = useDispatch();
    const dataContactForm = useSelector(selectorContactForm)

    return (
        <section className='sect-contact__form'>
            <Container>
                <div className='sect-contact__container' >
                    <h1 className='sect-contact__title'>Contact form</h1>
                    <div className='sect-contact__wrap'>
                        <Formik
                            initialValues={dataContactForm}
                            validationSchema={validation}
                            onSubmit={(values, { resetForm }) => {
                                dispatch(actionSubmitFormContact(values))
                                resetForm()
                            }}

                        >
                            {({ errors, touched, values, handleChange, handleBlur }) => {
                                return <Form className='form-contact'>
                                    <h2 className='form-contact__title'>Drop Us a Line</h2>
                                    <p className='form-contact__text'>Your email address will not be published. Required fields are marked *</p>
                                    <div className='row'>
                                        <Input
                                            type='text'
                                            placeholder="First Name"
                                            name='firstName'
                                            className='contact-input'
                                            error={errors.firstName && touched.firstName}
                                        />
                                        <Input
                                            type='text'
                                            placeholder="Your Email"
                                            name='email'
                                            className='contact-input'
                                            error={errors.email && touched.email}
                                        />
                                    </div>
                                    <div className='row'>
                                        <Input
                                            replacement={{ _: /\d/ }}
                                            type='tel'
                                            placeholder="+38 (___) ___-__-__"
                                            mask="+38 (___) ___-__-__"
                                            name='phone'
                                            className='contact-input'
                                            component={Input}
                                            value={values.phone || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={errors.phone && touched.phone}
                                        />
                                        <Input
                                            type='text'
                                            placeholder="Subject"
                                            name='subject'
                                            className='contact-input'
                                            error={errors.subject && touched.subject}
                                        />
                                    </div>
                                    <div className='row'>
                                        <TextArea
                                            className='contact-textarea'
                                            name='description'
                                            placeholder="Description"
                                            error={errors.description && touched.description}
                                        />
                                    </div>
                                    <div className='row'>
                                        <Button
                                            type='submit'
                                            className='contact-btn'
                                        >
                                            Send Message
                                        </Button>
                                    </div>
                                </Form>
                            }}
                        </Formik>
                        <div className='sect-contact__img-bl'>
                            <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1721853572/contact_esrvks.jpg" className='sect-contact__img' alt="contact" />
                        </div>
                    </div>
                </div>
            </Container >
        </section >
    );
};

ContactForm.propTypes = {};

export default ContactForm;