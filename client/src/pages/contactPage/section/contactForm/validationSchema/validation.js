import * as yup from 'yup'
const validation = yup.object({
    firstName: yup
        .string()
        .matches(/^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀ-ỿ\s\-\/.]+$/, "Please use only letters")
        .min(3, 'Name minimum length is 3 characters')
        .max(40, 'Name maximum length is 40 characters')
        .required('Name is required'),
    email: yup
        .string()
        .email('Invalid email format')
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Email is not valid')
        .required('Email is required'),
    phone: yup
        .string('')
        .min(19, 'Please enter correct phone number')
        .max(19, 'Please enter correct phone number')
        .required('Phone Number is required'),
    subject: yup
        .string()
        .min(3, 'Subject minimum length is 3 characters')
        .max(40, 'Subject maximum length is 40 characters')
        .required('Subject is required'),
    description: yup
        .string()
        .min(10, 'Description minimum length is 10 characters')
        .max(225, 'Description maximum length is 225 characters')
        .required('Description is required'),
})

export default validation 