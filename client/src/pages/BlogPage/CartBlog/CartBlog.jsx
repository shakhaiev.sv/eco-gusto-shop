
import React from 'react';
import PropTypes from 'prop-types';
import '../CartBlog/CartBlog.scss';

const CardBlog = (props) => {
    const { dataBlog } = props;
    const { img, name, category } = dataBlog;

    return (

        <div className='card-blog'>
            <img src={img} alt="img" className='card-blog__image' />
            <p className='card-blog__category'>{category}</p>
            <h3 className='card-blog__title'>{name}</h3>
            <p className='card-blog__meta'>
                <span className='card-blog__date'>25 April 2022</span>
                <span className='card-blog__views'>126k Views</span>
                <span className='card-blog__read-time'>4 mins read</span>
            </p>
        </div>
    );
};

CardBlog.propTypes = {
    dataBlog: PropTypes.shape({
        img: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        category: PropTypes.string.isRequired,
    }).isRequired,
};

export default CardBlog;
