
import React, { useEffect } from 'react';
import Container from '../../layout/container/Container.jsx';
import CardBlog from '../BlogPage/CartBlog/CartBlog.jsx';
import '../BlogPage/BlogPage.scss';

const BlogPage = () => {

    const arayBlog = [

        {
            id: 1,
            img: 'https://res.cloudinary.com/dre55ftmq/image/upload/v1725715260/blog-1_dyizot.png',
            name: "The Intermediate Guide to Healthy Food",
            category: "Side Dish"
        },

        {
            id: 2,
            img: 'https://res.cloudinary.com/dre55ftmq/image/upload/v1725715259/blog-2_sib1yk.png',
            name: "Caprese Chicken  Smashed ",
            category: "Soups and Stews"
        },

        {
            id: 3,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715259/blog-3_vppfvk.png",
            name: "Summer Quinoa Salad Jars with Lemon Dill",
            category: "Salad"
        },

        {
            id: 4,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715860/blog-4_jrw8dn.png",
            name: "Harissa Chickpeas with Whipped Feta",
            category: "Dessert"
        },

        {
            id: 5,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1726948954/blog-5_yykrqr.png",
            name: "Almond Butter Chocolate Chip Zucchini Bars",
            category: "Breakfast"
        },

        {
            id: 6,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715859/blog-6_pxw3ks.png",
            name: "Smoky Beans & Greens Tacos with Aji Verde",
            category: "Vegan"
        },

        {
            id: 7,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715860/blog-7_hqzvdi.png",
            name: "Sticky Ginger Rice Bowls with Pickled Veg",
            category: "Gluten Free"
        },

        {
            id: 8,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715859/blog-8_tgoesc.png",
            name: "Creamy Garlic Sun-Dried Tomato Pasta",
            category: "Side Dish"
        },

        {
            id: 9,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715859/blog-9_le1dmp.png",
            name: "The Absolute Easiest Spinach and Pizza",
            category: "Dairy Free"
        },

        {
            id: 10,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715859/blog-10_k5w7bb.png",
            name: "Sticky Ginger Rice Bowls with Pickled",
            category: "Salad"
        },

        {
            id: 11,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715859/blog-14_ci9t2o.png",
            name: "The litigants on the screen are not actors",
            category: "Vegetarian"
        },

        {
            id: 12,
            img: "https://res.cloudinary.com/dre55ftmq/image/upload/v1725715859/blog-15_bz1zen.png",
            name: "The litigants on the screen are not actors",
            category: "Vegetarian"
        },
    ];

    useEffect(() => {
        window.scrollTo({
            top: "120px",
            behavior: 'smooth'
        })
    }, [])

    return (
        <section className='sect-blog'>
            <Container>
                <div className='sect-blog__header'>
                    <p className='sect-blog__header-text'>Blog & News</p>
                </div>
                <div className='sect-blog__grid'>
                    {arayBlog.map((blog) => (
                        <CardBlog key={blog.id} dataBlog={blog} />
                    ))}
                </div>


            </Container>
        </section>

    );
};

export default BlogPage;

