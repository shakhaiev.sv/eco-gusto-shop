import React, { useEffect, useContext, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ModalContext } from '../../context/ModalContext.jsx'
import ModalAuthorization from "../../components/ModalAuthorization/ModalAuthorization.jsx";

import {
  selectorListCart,
  selectorSummCart,
  selectorDataResponceFromOrder,
  selectorToken,
} from '../../store/selectors/selectors.js';

import {
  actionSummCart,
  actionDeleteCartAuthClient,
  actionDeleteCart,
  actionAllLengthCoodsOnCart
} from '../../store/slices/cart.js'

import { actionDeleteDataOrder } from '../../store/slices/order.js'

import './CartPage.scss';
import Container from '../../layout/container/Container.jsx';
import CheckoutForm from './CheckoutForm/CheckoutForm.jsx';
import CardCart from './CardCart/CardCart.jsx';
import ModalOrder from '../../components/ModalOrder/ModalOrder.jsx';
import Button from '../../components/Button/Button.jsx';
import { useLocation } from 'react-router-dom';

const CartPage = () => {
  const [isSubmit, setIsSubmit] = useState(false)
  const listCart = useSelector(selectorListCart) || [];
  const summCart = useSelector(selectorSummCart);
  const dispatch = useDispatch();
  const { showModalAuth, showModalOrder, fnShowModalOrder } = useContext(ModalContext)
  const dataResOrder = useSelector(selectorDataResponceFromOrder);
  const location = useLocation()
  const token = useSelector(selectorToken);

  useEffect(() => {
    window.scrollTo({
      top: "120px",
      behavior: 'smooth'
    })
    dispatch(actionSummCart())
  }, []);

  useEffect(() => {
    if (dataResOrder.result || dataResOrder.message) {
      fnShowModalOrder()
    }

  }, [dataResOrder])

  useEffect(() => {
    return () => {
      if (showModalOrder) {
        if (token) {
          dispatch(actionDeleteCartAuthClient(token))
        } else {
          dispatch(actionAllLengthCoodsOnCart())
        }
        dispatch(actionDeleteCart())
        dispatch(actionDeleteDataOrder())
        fnShowModalOrder()
      }
      const classBody = document.querySelector('body')
      if (classBody.classList.contains('is-overflow')) {
        classBody.classList.remove('is-overflow')
      }
    };
  }, [location])

  return (
    <section className='sect-cart'>
      <Container>
        {listCart.length ? <>
          <h1 className='sect-cart__title sect-title'>Your Cart</h1>
          <div className='sect-cart__wrap'>
            <CheckoutForm
              isSubmit={isSubmit}
              setIsSubmit={setIsSubmit}
            />
            <div className='sect-cart__wrap-goods'>
              <ul className='sect-cart__list' >
                {
                  listCart?.map((item, index) => <li
                    className='sect-cart__item'
                    key={index}
                  >
                    <CardCart
                      dataProduct={item}
                    />
                  </li>
                  )
                }
              </ul>
              <div className='sect-cart__total '>
                <p className='sect-cart__total-title'>Total</p>
                <p className='sect-cart__total-price'>{`$${Number(summCart).toFixed(2)}`}</p>
              </div>
            </div>
          </div>
        </> : <div className="sect-cart__empty">
          <p className="sect-cart__empty-text">
            Oh! No products available, check our  <Button to='/products' >best products</Button>
          </p>
          <img src="https://res.cloudinary.com/dre55ftmq/image/upload/v1722772652/EmptyCart_dcihgy.png" alt="empty cart" />
        </div>
        }
        {showModalAuth && <ModalAuthorization />}
        {showModalOrder && <ModalOrder
          dataResponce={dataResOrder}
          setIsSubmit={setIsSubmit}
        />}
      </Container >

    </section>
  );
};

export default CartPage;