import * as yup from 'yup'

const validationSchema = yup.object({

    firstName: yup
        .string()
        .matches(/^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀ-ỿ\s\-\/.]+$/, "Please use only letters")
        .min(3, 'First Name minimum length is 3 characters')
        .max(40, 'First Name maximum length is 40 characters')
        .required('First Name is required'),
    lastName: yup
        .string()
        .matches(/^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀ-ỿ\s\-\/.]+$/, "Please use only letters")
        .min(3, 'Last Name minimum length is 3 characters')
        .max(40, 'Last Name maximum length is 40 characters')
        .required('Last Name is required'),
    phone: yup
        .string('')
        .min(19, 'Please enter correct phone number')
        .max(19, 'Please enter correct phone number')
        .required('Phone Number is required'),
    city: yup
        .string()
        .min(2, 'City minimum length is 2 characters')
        .max(40, 'City maximum length is 40 characters')
        .matches(/^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀ-ỿ\s\-\/.]+$/, "Please use only letters")
        .required('City is required'),
    streetAddress: yup
        .string()
        .min(2, 'Address minimum length is 2 characters')
        .max(80, 'Address maximum length is 80 characters')
        .required('Address is required'),
    email: yup
        .string()
        .email('Invalid email format')
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Email is not valid')
        .required('Email is required'),
    country: yup
        .string()
        .min(2, 'Country minimum length is 2 characters')
        .max(80, 'Country maximum length is 80 characters')
        .matches(/^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀ-ỿ\s\-\/.]+$/, "Please use only letters")
        .required('Country is required'),
    deliverDate: yup
        .date()
        .required('Delivery date is required'),


})

export default validationSchema 