import React, { useEffect, useContext, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Formik, Form } from 'formik';
import {
  selectorToken,
  selectorDataClientForDeliveryCart,
  selectorListCart,
  selectorDataClient,
  selectorDataResponceFromOrder,
} from '../../../store/selectors/selectors.js';
import {
  actionSendOrderForAuthClient,
  actionSendOrder,
  actionUpdateDataFromOrder,
} from '../../../store/slices/order.js'
import validationForm from './validationSchema/validationSchema.js'
import Input from '../../../components/Form/Input/Input.jsx';
import DateInput from '../../../components/Form/DateInput/DateInput.jsx';
import Button from '../../../components/Button/Button.jsx';
import './CheckoutForm.scss'



const CheckoutForm = (props) => {
  const { isSubmit } = props
  const dispatch = useDispatch();
  const token = useSelector(selectorToken);
  const dataClientForDeliveryCart = useSelector(selectorDataClientForDeliveryCart)
  const listCart = useSelector(selectorListCart)
  const dataClientAuth = useSelector(selectorDataClient)
  const dataResOrder = useSelector(selectorDataResponceFromOrder);


  useEffect(() => {
    if (Object.values(dataClientAuth).length && token) {
      dispatch(actionUpdateDataFromOrder(dataClientAuth))
    }
  }, [dataClientAuth])


  const year = new Date().getFullYear()
  const month = (new Date().getMonth() < 9) ? `0${new Date().getMonth() + 1}` : `${new Date().getMonth() + 1}`
  const day = new Date().getDate()

  return (
    <Formik
      initialValues={dataClientForDeliveryCart}
      validationSchema={validationForm}
      onSubmit={(values, { resetForm }) => {
        if (token) {
          dispatch(actionSendOrderForAuthClient({
            token, dataClientForOrder: {
              deliveryAddress: {
                country: values.country,
                city: values.city,
                address: values.streetAddress,
                deliverDate: values.deliverDate
              },
              shipping: "Kiev 50UAH",
              paymentInfo: "Credit card",
              email: values.email,
              mobile: values.phone,
              letterSubject: "Thank you for order! You are welcome!",
              letterHtml: `<div style="font-family: Arial, sans-serif; padding: 20px;">
                <h2 style="color: #333;">Thank You for Your Order!</h2>
                <p style="color: #555;">Dear ${values.firstName},</p>
                <p style="color: #555;">We appreciate your business and we're excited to get your order on its way! Below are your order details:</p>
                <p style="color: #555;"><strong>Shipping Address:</strong> ${values.streetAddress}, ${values.city}, ${values.country}, ${values.postal}</p>
                <p style="color: #555;">If you have any questions, feel free to contact our support team.</p>
                <p style="color: #555;">Best regards,<br>Your Eco Gusto</p></div>`
            }
          }))
        } else {
          dispatch(actionSendOrder({
            customer: {
              firstName: values.firstName,
              lastName: values.lastName,
            },
            mobile: values.phone,
            email: values.email,
            deliveryAddress: {
              country: values.country,
              city: values.city,
              address: values.streetAddress,
              deliverDate: values.deliverDate
            },
            products: listCart.map((item) => {
              return { product: item._id, cartQuantity: item.cartQuantity }
            }),
            letterSubject: "Thank you for order! You are welcome!",
            letterHtml: `<div style="font-family: Arial, sans-serif; padding: 20px;">
              <h2 style="color: #333;">Thank You for Your Order!</h2>
              <p style="color: #555;">Dear ${values.firstName},</p>
              <p style="color: #555;">We appreciate your business and we're excited to get your order on its way! Below are your order details:</p>
              <p style="color: #555;"><strong>Shipping Address:</strong> ${values.streetAddress}, ${values.city}, ${values.country}</p>
              <p style="color: #555;">If you have any questions, feel free to contact our support team.</p>
              <p style="color: #555;">Best regards,<br>Your Eco Gusto</p></div>`
          }))
        }

        setIsSubmit(true);

        if (dataResOrder.result) {
          resetForm()
        }

      }}
    >
      {({ errors, touched, values, handleChange, handleBlur }) => (
        <Form className='form-order'>
          <h1 className="form-order__title">Delivery Form</h1>

          <div className='row'>
            <Input
              type='text'
              placeholder="First Name"
              name='firstName'
              className='input-order'
              error={errors.firstName && touched.firstName}
            >
              <span>First Name</span>
            </Input>
            <Input
              type='text'
              placeholder="Last Name"
              name='lastName'
              className='input-order'
              error={errors.lastName && touched.lastName}
            >
              <span>Last Name</span>
            </Input>
          </div>

          <div className='row'>
            <Input
              replacement={{ _: /\d/ }}
              type='tel'
              placeholder="+38 (___) ___-__-__"
              mask="+38 (___) ___-__-__"
              name='phone'
              onChange={handleChange}
              component={Input}
              className='input-order'
              error={errors.phone && touched.phone}
            >
              <span>Your phone</span>
            </Input>
            <Input
              type='text'
              placeholder="Your Email"
              name='email'
              className='input-order'
              error={errors.email && touched.email}
            >
              <span>Your email</span>
            </Input>
          </div>
          <div className='row'>
            <Input
              type='text'
              placeholder="Country"
              name='country'
              className='input-order'
              error={errors.country && touched.country}
            >
              <span>Country</span>
            </Input>
            <Input
              type='text'
              placeholder="City"
              name='city'
              className='input-order'
              error={errors.city && touched.city}
            >
              <span>City</span>
            </Input>
          </div>
          <div className='row'>
            <Input
              type='text'
              placeholder="Street Address"
              name='streetAddress'
              className='input-order'
              error={errors.streetAddress && touched.streetAddress}
            >
              <span>Street Address</span>
            </Input>
            <DateInput
              className='input-order'
              name='deliverDate'
              min={`${year}-${month}-${day}`} // Мінімальна дата
              error={errors.deliverDate && touched.deliverDate}>
              <span>Delivery date</span>
            </DateInput>
          </div>
          <div className='row'>
            <Button
              type='submit'
              className='btn-order'
              disabled={isSubmit}
            >Submit</Button>
          </div>
        </Form>
      )
      }
    </Formik >
  );
};

export default CheckoutForm;