import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MiniCard from '../../../components/MiniCard/MiniCard.jsx';
import Button from '../../../components/Button/Button.jsx';
import FavoritesIcon from "../../../pictures/svg/product/favorite.svg?react";
import RemoveIcon from "../../../pictures/svg/order/remove.svg?react";
import './CardCart.scss'
import { actionAddFavoriteList } from '../../../store/slices/favorite.js'
import cn from 'classnames'

import {
  actionDeleteProductFromCartAuthClient,
  actionIncreaseProductInCartAuthClient,
  actionDecreaseProductInCartAuthClient,
  actionAllLengthCoodsOnCart,
  actionRemoveCardFromCartList,
  actionSummCart,
  actionIncreaseProductInCart,
  actionDecreaseProductInCart
} from '../../../store/slices/cart.js'

import { selectorToken, selectorListFavorite } from '../../../store/selectors/selectors.js'

const CardCart = ({ dataProduct }) => {

  const dispatch = useDispatch();
  const token = useSelector(selectorToken)
  const listFavorite = useSelector(selectorListFavorite)
  const handleAddCardToFavorite = () => {
    dispatch(actionAddFavoriteList(dataProduct));

  };

  const handleDeleteProductFromCart = () => {
    if (token) {
      dispatch(actionDeleteProductFromCartAuthClient({ token, idProduct: dataProduct._id }))
    } else {
      dispatch(actionRemoveCardFromCartList(dataProduct))
      dispatch(actionAllLengthCoodsOnCart())
      dispatch(actionSummCart())
    }
  }

  const handleIncreaseProductInCart = () => {
    if (token) {
      dispatch(actionIncreaseProductInCartAuthClient({ token, idProduct: dataProduct._id }))
    } else {
      dispatch(actionIncreaseProductInCart(dataProduct))
      dispatch(actionAllLengthCoodsOnCart())
      dispatch(actionSummCart())
    }

  }

  const handleDecreaseProductInCart = () => {
    if (token) {
      dispatch(actionDecreaseProductInCartAuthClient({ token, idProduct: dataProduct._id }))
    } else {
      dispatch(actionDecreaseProductInCart(dataProduct))
      dispatch(actionAllLengthCoodsOnCart())
      dispatch(actionSummCart())
    }
  }

  return (

    <>
      <MiniCard dataProduct={dataProduct} />
      <div className="cart-item__controllers">
        <div className='cart-item__wrap-btn'>
          <Button
            type='button'
            className={cn('cart-item__btn-favorite', { 'is-favorite': listFavorite?.some((item) => item['_id'] === dataProduct._id) })}
            onClick={handleAddCardToFavorite}
          >
            <FavoritesIcon />
          </Button>
          <Button
            type='button'
            className='cart-item__btn-remove'
            onClick={handleDeleteProductFromCart}
          >
            <RemoveIcon />
          </Button>
        </div>

        <div className="cart-item__wrap-qauntity">
          <Button
            type='button'
            className="cart-item__btn-qauntity"
            onClick={handleIncreaseProductInCart}
          >+</Button>
          <span>{dataProduct.cartQuantity}</span>
          <Button
            type='button'
            className="cart-item__btn-qauntity"
            onClick={handleDecreaseProductInCart}
          >-</Button>
        </div>


      </div>


    </>
  );
};

export default CardCart;