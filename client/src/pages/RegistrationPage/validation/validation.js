import * as yup from "yup";

const validation = yup.object({

    fullName: yup
        .string()
        .matches(/^[a-zA-ZÀ-ÖÙ-öù-ÿĀ-žḀ-ỿ\s\-\/.]+$/, "Please use only letters")
        .min(3, 'Full Name must be at least 3 characters')
        .max(50, 'Full Name maximum length is 50 characters')
        .required('Full Name is required'),
    email: yup
        .string()
        .email('Invalid email format')
        .required('Email is required')
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "Email is not valid"),
    phone: yup
        .string()
        // .matches(/^\+\d{1,15}$/, 'Phone number is not valid')
        .min(19, 'Please enter correct phone number')
        .max(19, 'Please enter correct phone number')
        .required('Phone Number is required'),
    password: yup
        .string()
        .min(6, 'Minimum Password length is 6 characters')
        .max(40, 'Maximum Password length is 40 characters')
        .required('Password is required'),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password'), null], 'Passwords must match')
        .required('This field is required'),
    gender: yup
        .string()
        .required('Gender is required')

});

export default validation