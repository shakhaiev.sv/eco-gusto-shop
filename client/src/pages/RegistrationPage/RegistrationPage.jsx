import React, { useEffect, useContext, useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { registerUser } from '../../store/slices/registrationSlice';
import Input from '../../components/Form/Input/Input.jsx'
import validation from './validation/validation.js'
import './RegistrationPage.scss';
import { selectorResponceRegistr } from '../../store/selectors/selectors.js'
import { ModalContext } from '../../context/ModalContext.jsx';
import ModalAuthorization from '../../components/ModalAuthorization/ModalAuthorization.jsx';

const RegistrationPage = () => {
  const [message, setMeassage] = useState('')
  const dispatch = useDispatch();
  const responceRegistr = useSelector(selectorResponceRegistr)
  const { loading, error } = useSelector((state) => state.registration);
  const { fnShowModalAuth, showModalAuth } = useContext(ModalContext)

  useEffect(() => {
    if (responceRegistr?.result) {
      fnShowModalAuth()
    } else {
      setMeassage(responceRegistr.message || responceRegistr.password)
    }
  }, [responceRegistr])

  return (
    <div className="registration-page">
      <div className="title">Registration</div>
      <div className="content">
        <Formik

          initialValues={{
            fullName: '',
            email: '',
            phone: '',
            password: '',
            confirmPassword: '',
            gender: ''
          }}
          validationSchema={validation}
          onSubmit={(values) => {
            values.username = values.email; 
            values.telephone = `+${values.phone.replace(/[^\d]/g, '')}`
            dispatch(registerUser(values));
          }}

        >
          {({ errors, touched, values, handleChange, handleBlur }) => (

            <Form>
              <div className="user-details">
                <div className="input-box">
                  <span className="details">Full Name</span>
                  <Field type="text" name="fullName" placeholder="Enter your name" />
                  <ErrorMessage name="fullName" component="div" className="error-message" />
                </div>
                <div className="input-box">
                  <span className="details">Email</span>
                  <Field type="email" name="email" placeholder="Enter your email" />
                  <ErrorMessage name="email" component="div" className="error-message" />
                </div>
                <div className="input-box">
                  <span className="details">Phone Number</span>
                  <Input
                    type="tel"
                    name="phone"
                    replacement={{ _: /\d/ }}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="+38 (___) ___-__-__"
                    mask="+38 (___) ___-__-__"
                    component={Input}
                    error={errors.phone && touched.phone}
                  />
                </div>
                <div className="input-box">
                  <span className="details">Password</span>
                  <Field type="password" name="password" placeholder="Enter your password" />
                  <ErrorMessage name="password" component="div" className="error-message" />
                </div>
                <div className="input-box">
                  <span className="details">Confirm Password</span>
                  <Field type="password" name="confirmPassword" placeholder="Confirm your password" />
                  <ErrorMessage name="confirmPassword" component="div" className="error-message" />
                </div>
              </div>
              <div className="gender-details">
                <span className="gender-title">Gender</span>
                <div className="category">
                  <label htmlFor="dot-1">
                    <Field type="radio" name="gender" id="dot-1" value="male" />
                    <span className="dot one"></span>
                    <span className="gender">Male</span>
                  </label>
                  <label htmlFor="dot-2">
                    <Field type="radio" name="gender" id="dot-2" value="female" />
                    <span className="dot two"></span>
                    <span className="gender">Female</span>
                  </label>
                  <label htmlFor="dot-3">
                    <Field type="radio" name="gender" id="dot-3" value="other" />
                    <span className="dot three"></span>
                    <span className="gender">Prefer not to say</span>
                  </label>
                </div>
                <ErrorMessage name="gender" component="div" className="error-message" />
              </div>
              {message && <div className='registration-res-message'>{message}</div>}
              <div className="button">
                <button type="submit" disabled={loading}>Register</button>
                {loading && <p>Loading...</p>}
                {error && <p className="error-message">{error}</p>}
              </div>
            </Form>
          )}
        </Formik>
      </div>
      {showModalAuth && <ModalAuthorization />}
    </div>
  );
};

export default RegistrationPage;
