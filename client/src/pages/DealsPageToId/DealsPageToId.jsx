import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from "react-redux"
import { selectorDeals, selectorDealsById } from "../../store/selectors/selectors.js"
import { actionFetchDealsbyId } from '../../store/slices/deals.js'
import { useParams } from 'react-router-dom';
import Container from '../../layout/container/Container.jsx';
import Product from '../../components/Product/Product.jsx';
import './DealsPageToId.scss'


const DealsPageToId = () => {

    const [listProducts, setListProducts] = useState([])
    const dispath = useDispatch()
    const { id } = useParams()
    const dealsList = useSelector(selectorDeals);
    const dealById = useSelector(selectorDealsById);

    useEffect(() => {
        try {
            if (dealsList.length) {
                setListProducts(dealsList.find((item) => item['_id'] === id).products)
            }
        } catch (err) {
            console.error(err);
        }
    }, [])


    useEffect(() => {
        try {
            if (!dealsList.length) {
                dispath(actionFetchDealsbyId(id))
                setListProducts(dealById[0]?.products)
            }
        } catch (err) {
            console.error(err);
        }
    }, [dealById.length])


    return (
        <section className='sect-deals-product' >
            <Container>
                <h1 className='sect-deals-product__title'>{dealById[0]?.name}</h1>
                <ul className='sect-deals-product__list' >
                    {
                        listProducts?.map(({ product }, index) => <li key={index} className='sect-deals-product__item'>
                            <Product
                                dataProduct={product}
                            />
                        </li>

                        )
                    }
                </ul>

            </Container>
        </section>
    );
};

DealsPageToId.propTypes = {};

export default DealsPageToId;