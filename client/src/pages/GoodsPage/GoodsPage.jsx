import React, { useState, useEffect, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { useInView } from 'react-intersection-observer'

import Container from "../../layout/container/Container";
import DoubleRangeSlider from "../../components/DoubleRangeSlider/DoubleRangeSlider";
import CategoryButton from "./CategoryButton/CategoryButton.jsx";
import Product from "../../components/Product/Product.jsx";
import Button from "../../components/Button/Button.jsx";
import { ModalContext } from "../../context/ModalContext.jsx";
import ModalCreateProducts from "../../components/Admin/ModalCreateProducts/ModalCreateProducts.jsx";
import ModalCreateCategory from "../../components/Admin/ModalCreateCategory/ModalCreateCategory.jsx";

import { actionFetchCategories } from "../../store/slices/categories.js";
import {
  selectorAdmin,
  selectorNextPageProducts,
  selectorProductsPage,
  selectorProducts,
  selectorCategories,
} from "../../store/selectors/selectors.js";
import {
  actionFetchProducts
} from "../../store/slices/products.js";
import "./GoodsPage.scss";
import { useMedia } from '../../hooks/UseMedia.js'

import Filter from '../../pictures/svg/doubleRangeSlider/filter.svg?react';
import Arrow from '../../pictures/svg/goodsPage/arrow.svg?react'
import Create from '../../pictures/svg/product/create.svg?react'
import Edit from '../../pictures/svg/product/edit.svg?react'

import { Swiper, SwiperSlide } from "swiper/react";


const GoodsPage = () => {
  const { ref, inView } = useInView({ threshold: 0.1 })
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation()

  const listProducts = useSelector(selectorProducts);
  const firstPage = useSelector(selectorProductsPage)
  const nextPage = useSelector(selectorNextPageProducts)
  const categories = useSelector(selectorCategories);
  const Admin = useSelector(selectorAdmin);


  const {
    showModalCreateCategory,
    fnShowModalCreateCategory,
    showModalCreateProducts,
    fnShowModalCreateProducts } = useContext(ModalContext);


  const [selectedCategories, setSelectedCategories] = useState([]);
  const [isCategoriesFiltersOpen, setIsCategoriesFiltersOpen] = useState(true);
  const [isPriceFilterOpen, setIsPriceFilterOpen] = useState(false);
  const [isOtherFiltersOpen, setIsOtherFiltersOpen] = useState(false);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [showFilter, setShowFilter] = useState(false)
  const [filterOptions, setFilterOptions] = useState({ Hot: false, Sale: false, New: false });

  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(3000);


  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const categoryName = searchParams.getAll('categoryName');

    if (categoryName) {
      setSelectedCategories(categoryName);
    }

    dispatch(actionFetchCategories());
    const stickers = ['Hot', 'Sale', 'New'];
    const newFilterOptions = { ...filterOptions };

    stickers.forEach(sticker => {
      newFilterOptions[sticker] = searchParams.getAll('sticker').includes(sticker);
    });

    setFilterOptions(newFilterOptions);
  }, []);


  useEffect(() => {
    window.scrollTo({
      top: "120px",
      behavior: 'smooth'
    })
    dispatch(actionFetchProducts({ queryParams: location.search, pagination: firstPage }))
  }, [location.search, dispatch, firstPage]);

  useEffect(() => {
    if (inView && nextPage.page) {
      dispatch(actionFetchProducts({ queryParams: location.search, pagination: nextPage }))
    }
  }, [inView])


  useEffect(() => {
    const filteredListProductsByPrice = listProducts.filter(product => product.currentPrice >= minPrice && product.currentPrice <= maxPrice);
    setFilteredProducts(filteredListProductsByPrice);
  }, [listProducts, maxPrice, minPrice])


  const handleCategoryClick = (category) => {
    setSelectedCategories(prev => {
      const updatedCategories = prev.includes(category)
        ? prev.filter(ctg => ctg !== category)
        : [...prev, category];
      window.scrollTo({
        top: "120px",
        behavior: 'smooth'
      })
      return updatedCategories;
    });

  };

  const handleCheckbox = (e) => {
    const { name, checked } = e.target;

    setFilterOptions(prevOptions => ({
      ...prevOptions,
      [name]: checked,
    }));
    window.scrollTo({
      top: "120px",
      behavior: 'smooth'
    });
  };

  useEffect(() => {
    const updateSearchParams = () => {
      const searchParams = new URLSearchParams();
      selectedCategories.forEach(ctg => {
        searchParams.append('categoryName', ctg);
      });

      Object.entries(filterOptions).forEach(([key, value]) => {
        if (value) {
          searchParams.append('sticker', key);
        }
      });


      const newSearchParams = `?${searchParams.toString()}`;

      navigate(newSearchParams, { replace: true });
    };
    updateSearchParams();
  }, [selectedCategories, filterOptions]);


  const handlePriceChange = (min, max) => {
    setMinPrice(min)
    setMaxPrice(max)
  }



  const handleDropdownFilters = (filterType) => {
    switch (filterType) {
      case 'category':
        setIsCategoriesFiltersOpen(prev => !prev);
        break;
      case 'price':
        setIsPriceFilterOpen(prev => !prev);
        break;
      case 'checkbox':
        setIsOtherFiltersOpen(prev => !prev);
        break;
      default:
        break;
    }
  }


  // mobile
  const isMediaQuery = useMedia("(max-width:959px)")
  const isMediaMobQuery = useMedia('max-width:350px')
  const handleShowFilter = () => {
    setShowFilter(!showFilter)
  }
  // mobile

  const handleShowModalCreatePrdoducts = () => {
    fnShowModalCreateProducts()
  }

  return (
    <section className="sect-goods">
      <Container>
        <div className="sect-goods__wrap">


          {/* mobile */}
          {isMediaQuery && <>
            <Button
              className='sect-goods__btn-filter'
              type='button'
              onClick={handleShowFilter}

            ><Filter />Filter</Button>
            {showFilter && <>
              <h4 className="sect-goods__mob-title">Category</h4>
              <Swiper
                slidesPerView={isMediaMobQuery ? 2.5 : 1.5}
                spaceBetween={20}
                className="sect-goods__mob-slider"
              >
                {
                  categories?.map((category, index) => <SwiperSlide
                    key={index}
                    className="sect-goods__item"
                  >
                    <CategoryButton
                      category={category}
                      isActive={selectedCategories.includes(category.name)}
                      onClick={() => handleCategoryClick(category.name)}
                    />
                  </SwiperSlide>
                  )
                }
              </Swiper>

              <h4 className="sect-goods__mob-title ">Fill by price</h4>
              <DoubleRangeSlider
                setMinPrice={setMinPrice}
                setMaxPrice={setMaxPrice}
                minPrice={minPrice}
                maxPrice={maxPrice} />

              <h4 className="sect-goods__mob-title ">Other</h4>
              <div className="checkbox-list">
                <div className="checkbox-item">
                  <input
                    type="checkbox"
                    name="Hot"
                    checked={filterOptions.Hot}
                    onChange={handleCheckbox} />
                  <label htmlFor="hot">Hot</label>
                </div>
                <div className="checkbox-item">
                  <input
                    type="checkbox"
                    name="Sale"
                    checked={filterOptions.Sale}
                    onChange={handleCheckbox} />
                  <label htmlFor="sale">Sale</label>
                </div>
                <div className="checkbox-item">
                  <input
                    type="checkbox"
                    name="New"
                    checked={filterOptions.New}
                    onChange={handleCheckbox} />
                  <label htmlFor="new">New</label>
                </div>
              </div>
            </>}
          </>
          }
          {/* mobile */}

          <div className='sidebar-wrapper'>
            <div className="category-wrapper sidebar-element">
              <h4 onClick={() => handleDropdownFilters('category')}>Category
                <Arrow className={isCategoriesFiltersOpen ? 'arrow-down' : 'arrow-up'} />
              </h4>

              {isCategoriesFiltersOpen &&
                <ul className="category-list">
                  {Admin && <div className="custom__create-category">
                    <label id="add-img-label" htmlFor="add-single-img" onClick={() => { fnShowModalCreateCategory() }}>
                      <Create />
                      <span>Create New Category</span>
                    </label>
                  </div>
                  }
                  {categories.map((category, index) => <li key={index} className="category-item">
                    {Admin && <Button
                      type='button'
                      className='category-edit'
                      onClick={() => {
                        fnShowModalCreateCategory(category)
                      }}
                    >
                      <Edit />
                    </Button>}
                    <CategoryButton
                      key={category.name}
                      category={category}
                      className='category-btn'
                      isActive={selectedCategories.includes(category.name)}
                      onClick={handleCategoryClick}
                    />
                  </li>
                  )}
                </ul>}
            </div>
            <div className="price-filter-wrapper sidebar-element">
              <h4 onClick={() => handleDropdownFilters('price')}>Fill by price
                <Arrow className={isPriceFilterOpen ? 'arrow-down' : 'arrow-up'} />
              </h4>
              {isPriceFilterOpen &&
                <DoubleRangeSlider
                  setMinPrice={(min) => handlePriceChange(min, maxPrice)}
                  setMaxPrice={(max) => handlePriceChange(minPrice, max)}
                  minPrice={minPrice}
                  maxPrice={maxPrice} />}

            </div>
            <div className="checkbox-wrapper sidebar-element">
              <h4 onClick={() => handleDropdownFilters('checkbox')}>Other
                <Arrow className={isOtherFiltersOpen ? 'arrow-down' : 'arrow-up'} />

              </h4>
              {isOtherFiltersOpen &&
                <div className="checkbox-list">
                  <div className="checkbox-item">
                    <input
                      type="checkbox"
                      name="Hot"
                      checked={filterOptions.Hot || false}
                      onChange={handleCheckbox} />
                    <label htmlFor="Hot">Hot</label>
                  </div>
                  <div className="checkbox-item">
                    <input
                      type="checkbox"
                      name="Sale"
                      checked={filterOptions.Sale || false}
                      onChange={handleCheckbox} />
                    <label htmlFor="Sale">Sale</label>
                  </div>
                  <div className="checkbox-item">
                    <input
                      type="checkbox"
                      name="New"
                      checked={filterOptions.New || false}
                      onChange={handleCheckbox} />
                    <label htmlFor="New">New</label>
                  </div>
                </div>
              }
            </div>
          </div>

          <ul className="cards-container">
            {
              Admin &&
              <li className="custom__create-product">
                <div className="custom__create-product-card"
                  onClick={() => { fnShowModalCreateProducts() }}>
                  <Create />
                  <span>Create New Product</span>
                </div>
              </li>
            }
            {
              filteredProducts?.map((product, index) => {

                if (listProducts.length - 1 === index) {
                  return <li
                    ref={ref}
                    key={index}
                    className='cards-container__item'>
                    <Product
                      isEdit={true}
                      dataProduct={product}
                    />
                  </li>
                } else {
                  return <li
                    key={index}
                    className='cards-container__item'>
                    <Product
                      isEdit={true}
                      dataProduct={product}
                    />
                  </li>
                }
              })
            }
          </ul>
        </div>
        {/* list Products */}
        {showModalCreateProducts && <ModalCreateProducts />}
        {showModalCreateCategory && <ModalCreateCategory />}
      </Container>
    </section>
  );
};

export default GoodsPage;







