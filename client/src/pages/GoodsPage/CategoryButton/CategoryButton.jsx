import React from "react";
import Button from "../../../components/Button/Button";
import './CategoryButton.scss'
import cn from 'classnames'

const CategoryButton = ({ className, category, isActive, onClick }) => {
    const handleFilterCategory = () => {
        onClick(category.name)
    }

    return (<>
        <Button onClick={handleFilterCategory} className={cn(className, { 'active': isActive })}>
            <div className="category-name">
                <img src={category.imgUrl} alt={category.name} />
                {category.name}
            </div>
        </Button>
    </>

    );
}


export default CategoryButton