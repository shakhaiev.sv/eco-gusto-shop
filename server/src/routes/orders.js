const express = require("express");
const router = express.Router();
const passport = require("passport");

//Import controllers
const {
  createOrder,
  placeOrder,
  updateOrder,
  cancelOrder,
  deleteOrder,
  getOrders,
  getOrder,
  getOrdersAllClients,
  putOrderStatusOnShipped,
} = require("../controllers/orders");

// @route   POST /orders/create 
// @desc    Place Order not auth client
// @access  Private
router.post("/create",
  createOrder
)

// @route   POST /orders
// @desc    Place Order
// @access  Private
router.post("/",
  passport.authenticate("jwt", { session: false }),
  placeOrder
);

// @route   PUT /orders/:id
// @desc    Update order
// @access  Private
router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  updateOrder
);

// @route   PUT /orders/cancel/:id
// @desc    Cancel order
// @access  Private
router.put(
  "/cancel/:id",
  passport.authenticate("jwt", { session: false }),
  cancelOrder
);

// @route   DELETE /orders/:id
// @desc    Delete order
// @access  Private
router.delete(
  "/:id",
  passport.authenticate("jwt-admin", { session: false }),
  deleteOrder
);

// @route   GET /orders
// @desc    Get all orders
// @access  Private
router.get("/", passport.authenticate("jwt", { session: false }), getOrders);

// @route   Put/orders/id
// @desc    Put order on status = shipped
// @access  Private
router.put(
  '/shipped/:id',
  passport.authenticate("jwt-admin", { session: false }),
  putOrderStatusOnShipped
)


// @route   GET /orders/all
// @desc    Get one order by orderNo
// @access  Private

router.post(
  "/clients",
  passport.authenticate("jwt-admin", { session: false }),
  getOrdersAllClients
);
// @route   GET /orders/:orderNo
// @desc    Get one order by orderNo
// @access  Private
router.get(
  "/:orderNo",
  passport.authenticate("jwt", { session: false }),
  getOrder
);



module.exports = router;
