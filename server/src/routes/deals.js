const express = require("express");
const router = express.Router();
const passport = require("passport");


//Import controllers
const {
  addDeals,
  getDeals,
  getDealsById,
  deleteDeals

} = require("../controllers/deals");


// @route   POST /deals
// @desc    Create new deals
// @access  Private
router.post(
  "/",
  passport.authenticate("jwt-admin", { session: false }),
  addDeals
);



// @route   GET /deals
// @desc    GET existing deals
// @access  Public
router.get("/", getDeals);

// // @route   GET /deals/:id
// // @desc    GET existing deals by id
// // @access  Public
router.get("/:id", getDealsById);

// @route   DELETE /deals/:id
// @desc    Delete existing deals
// @access  Private
router.delete(
  "/:id",
  passport.authenticate("jwt-admin", { session: false }),
  deleteDeals
);

module.exports = router;
