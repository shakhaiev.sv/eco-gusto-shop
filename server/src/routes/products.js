const express = require("express");
const router = express.Router();
const passport = require("passport");
const multer = require("multer"); // multer for parsing multipart form data (files)
const fse = require("fs-extra");
const { CloudinaryStorage } = require('multer-storage-cloudinary');
const cloudinary = require('cloudinary').v2;

//Import controllers
const {
  addImages,
  addProduct,
  updateProduct,
  getProducts,
  getProductById,
  getProductByFilter,
  getProductOnSearch,

} = require("../controllers/products");



cloudinary.config({
  cloud_name: 'dre55ftmq',
  api_key: '143254223627366',
  api_secret: 'QsWCot2Z2kbsY0M03nwTu1E8-mE',
});

const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: {
    folder: 'static',
    format: async (req, file) => {
      if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {
        return 'jpg';
      } else if (file.mimetype === 'image/png') {
        return 'png';
      } else if (file.mimetype === 'image/webp') {
        return 'webp';
      }
      return 'png'; // або null, якщо потрібно заблокувати
    },
    public_id: (req, file) => file.originalname.split('.')[0], // видаляємо розширення
  },
});

const upload = multer({ storage: storage });

// // Configurations for multer
// const storage = multer.diskStorage({
//   //   // Destination, where files should be stored (image url)
//   destination: function (req, file, cb) {
//     var newDestination = req.headers.path; // We sen image url in header ("path"), when making axios request
//     fse.mkdirsSync(newDestination); // We creating folder in destination, specified in headers "path"

//     cb(null, newDestination); // Saving file
//   },

//   filename: function (req, file, cb) {
//     cb(null, file.originalname); // We accept original file-name
//   }
// });

// const fileFilter = (req, file, cb) => {
//   // Accept file (only jpeg/jpg/png)


//   if (
//     file.mimetype === "image/jpeg" ||
//     file.mimetype === "image/png" ||
//     file.mimetype === "image/jpg" ||
//     file.mimetype === "image/webp"
//   ) {
//     cb(null, true);
//   } else {
//     // reject file (if not jpeg/jpg/png)
//     cb(new Error('nvalid file format, only JPEG and PNG allowed'), false);
//   }
// };

// const upload = multer({
//   storage: storage,
//   limits: {
//     fileSize: 1024 * 1024 * 3 // Max size 5MB
//   },
//   fileFilter: fileFilter
// });


// @route   POST /products/images
// @desc    Add images
// @access  Private
router.post(
  "/images",
  passport.authenticate("jwt-admin", { session: false }),
  upload.single("file"),
  addImages
);

// @route   POST /products
// @desc    Create new product
// @access  Private
router.post(
  "/",
  passport.authenticate("jwt-admin", { session: false }),
  addProduct
);

// @route   PUT /products/:id
// @desc    Update existing product
// @access  Private
router.put(
  "/:id",
  passport.authenticate("jwt-admin", { session: false }),
  updateProduct
);

// @route   GET /products
// @desc    GET existing products
// @access  Public
router.get("/", getProducts);

// @route   GET /products/filter 
// @desc    GET existing product by filter
// @access  Public
router.post("/filter", getProductByFilter);


// @route   GET /products/:id
// @desc    GET existing product by id
// @access  Public
router.get("/:id", getProductById);


// @route   GET /products/:id
// @desc    GET existing product by search data
// @access  Public

router.post("/search", getProductOnSearch);

module.exports = router;
