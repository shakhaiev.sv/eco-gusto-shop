const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DealsSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    imgUrl: {
      type: String,
      required: true
    },
    reviews: {
      type: Number,
      required: true,
      default: 0
    },
    products: [
      {
        product: {
          type: Schema.Types.ObjectId,
          ref: "products"
        },
      }
    ],

    date: {
      type: Date,
      default: Date.now
    }
  },
  { strict: false }
);

DealsSchema.index({ "$**": "text" });

module.exports = Deals = mongoose.model("Deals", DealsSchema);
