const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = new Schema(
  {
    enabled: {
      type: Boolean,
      required: true,
      default: true
    },
    name: {
      type: String,
      required: true
    },
    currentPrice: {
      type: Number,
      required: true
    },
    previousPrice: {
      type: Number
    },
    categoryName: {
      type: String,
      required: true
    },
    categoryId: {
      type: String,
      required: true
    },
    imageUrls: [
      {
        type: String,
        required: true
      }
    ],
    quantity: {
      type: Number,
      required: true,
      default: 0
    },
    productUrl: {
      type: String
    },
    brand: {
      type: String
    },
    country: {
      type: String,
      required: true
    },
    weight: {
      type: String,
    },
    storageTemperature: {
      type: String,
      required: true
    },
    nutritionFacts: {
      calories: {
        type: String,
      },
      proteins: {
        type: String,
      },
      fats: {
        type: String,
      },
      carbohydrates: {
        type: String,
      },
    },
    numberOfunits: {
      type: String,
      required: true
    },
    shelfLife: {
      type: String,
      required: true
    },
    sticker: {
      type: String,
    },
    reviews: {
      type: Number,
    },
    priorityName: {
      type: String,
    },
    search: {
      type: String,
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  { strict: false }
);

ProductSchema.index({ "$**": "text" });

module.exports = Product = mongoose.model("products", ProductSchema);
