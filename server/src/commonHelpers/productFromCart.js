const Product = require('../models/Product')
const mongoose = require("mongoose");


module.exports = async (Products) => {
    try {
        const cartProducts = await Promise.all(
            Products.map(
                async (item) => {
                    const product = await Product.findOne({ _id: item.product })
                    return {
                        _id: new mongoose.Types.ObjectId(),
                        product: product,
                        cartQuantity: item.cartQuantity
                    }
                })
        )
        return cartProducts
    } catch (err) {
        return { message: `Error happened on server: "${err}" ` }
    }
}