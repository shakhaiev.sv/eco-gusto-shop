
module.exports = (deals) => {
    const newDeals = deals.map((item) => {
        item.currentPrice = item.products.reduce((acum, currenValue) => {
            return acum += currenValue.product.currentPrice
        }, 0);
        item.previousPrice = item.products.reduce((acum, currenValue) => {
            return acum += currenValue.product.previousPrice
        }, 0);
        return item;
    });
    return newDeals
}