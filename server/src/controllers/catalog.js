const Catalog = require("../models/Catalog");
const Product = require('../models/Product')
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");
const uniqueRandom = require("unique-random");

exports.addCategory = (req, res, next) => {

  const currentCategory = _.cloneDeep(req.body)
  currentCategory['parentId'] = 'null'

  Catalog.findOne({ _id: req.body.id }).then(category => {
    if (category) {
      return res
        .status(400)
        .json({ message: `Category with id "${category.id}" already exists` });
    } else {
      const newCategory = new Catalog(queryCreator(currentCategory));

      newCategory
        .save()
        .then(category => res.json({
          'id': category.id,
          'name': category.name,
          'imgUrl': category.imgUrl,
          'parentId': category.parentId
        }))
        .catch(err =>
          res.status(400).json({
            message: `Error happened on server: "${err}" `
          })
        );
    }
  });
};

exports.aupdateCategory = (req, res, next) => {
  Catalog.findOne({ _id: req.params.id })
    .then(category => {
      if (!category) {
        return res.status(400).json({
          message: `Category with id "${req.params.id}" is not found.`
        });
      } else {
        const initialQuery = _.cloneDeep(req.body);
        const updatedCategory = queryCreator(initialQuery);
        Catalog.findOneAndUpdate(
          { _id: req.params.id },
          { $set: updatedCategory },
          { new: true }
        )
          .then(editCategory => {
            if (editCategory.name !== category.name) {
              Product.find({ categoryName: category.name })
                .then(products => {
                  Promise.all(
                    products.map(
                      async (item) => {
                        const updateCardProduct = await Product.findByIdAndUpdate(
                          item._id,
                          { categoryName: editCategory.name },
                          { new: true }
                        )
                        return updateCardProduct
                      }
                    )
                  )

                })
                .catch(err =>
                  res.status(400).json({
                    message: `Error happened on server: "${err}" `
                  }))
            }

            res.json(editCategory)
          })
          .catch(err =>
            res.status(400).json({
              message: `Error happened on server: "${err}" `
            })
          );
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.deleteCategory = (req, res, next) => {
  Catalog.findOne({ _id: req.params.id }).then(async category => {
    if (!category) {
      return res.status(400).json({
        message: `Category with id "${req.params.id}" is not found.`
      });
    } else {
      const categoryToDelete = await Catalog.findOne({ _id: req.params.id });

      Catalog.deleteOne({ _id: req.params.id })
        .then(deletedCount =>
          res.status(200).json({
            message: `Category witn id "${categoryToDelete.id}" is successfully deleted from DB.`,
            'result': 1
          })
        )
        .catch(err =>
          res.status(400).json({
            message: `Error happened on server: "${err}" `
          })
        );
    }
  });
};

exports.getCategories = (req, res, next) => {
  Catalog.find()
    .then(catalog => res.send(catalog))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.getCategory = (req, res, next) => {
  Catalog.findOne({ _id: req.params.id })
    .then(category => {
      if (!category) {
        return res.status(400).json({
          message: `Category with id "${req.params.id}" is not found.`
        });
      } else {
        res.status(200).json(category);
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};
