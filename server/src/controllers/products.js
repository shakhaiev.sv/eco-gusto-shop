const Product = require("../models/Product");
const isValidMongoId = require("../validation/isValidMongoId");
const streamifier = require('streamifier');
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(0, 999999);

const queryCreator = require("../commonHelpers/queryCreator");
const filterParser = require("../commonHelpers/filterParser");
const _ = require("lodash");
const e = require("express");
const cloudinary = require('cloudinary').v2
require('dotenv').config()
const fs = require('fs');
const path = require('path');

cloudinary.config({
  secure: true
});



exports.addImages = async (req, res, next) => {
  res.json({ message: 'Файл завантажено успішно', imageUrl: req.file.path });


  // console.log(req.file.path);

  // const options = {
  //   use_filename: true,
  //   unique_filename: false,
  //   overwrite: true,
  // };

  // try {
  //   if (req.files.length > 0) {
  //     const uploadStream = cloudinary.uploader.upload_stream(options, (error, result) => {
  //       console.log(result, 'uploadStream', uploadStream);
  //       if (error) {
  //         return res.status(500).json({ message: error.message });
  //       }


  //       if (result) {
  //         res.json({
  //           success: 1,
  //           message: "Photos are received",
  //           imageUrl: result.url,
  //         });
  //       } else {
  //         res.json({
  //           message: "Something went wrong with Cloudinary upload."
  //         });
  //       }
  //     });

  //     // Pipe file buffer directly to Cloudinary
  //     streamifier.createReadStream(req.files[0].buffer).pipe(uploadStream);
  //   }
  // } catch (err) {
  //   res.status(400).json({ message: err.message });
  // }
};

// exports.addImages = async (req, res, next) => {
//   const options = {
//     use_filename: true,
//     unique_filename: false,
//     overwrite: true,
//   };

//   try {
//     if (req.files.length > 0) {
//       const resultCloudinary = await cloudinary.uploader.upload(req.files[0].path, options)

//       if (resultCloudinary) {
//         res.json({
//           success: 1,
//           message: "Photos are received",
//           imageUrl: resultCloudinary.url
//         });
//       } else {
//         res.json({
//           message:
//             "Something wrong with receiving photos at server. Please, check the path folder"
//         });
//       }
//     }
//     streamifier.createReadStream(req.files[0].buffer).pipe(resultCloudinary);

//   } catch (err) {
//     res.status(400).json({ message: err.message })
//   } finally {
//     const dirname = __dirname.slice(0, -15)
//     const filePath = path.join(dirname, `${req.files[0].destination.slice(1).split('/').join("\\")}`, req.files[0].filename)

//     fs.unlink(filePath, (err) => {
//       if (err) {
//         console.error(`Помилка при видаленні файлу: ${err.message}`);
//         return;
//       }
//       console.log(`Файл "${filePath}" успішно видалено`);
//     })
//   }

// };

exports.addProduct = (req, res, next) => {
  const productFields = _.cloneDeep(req.body);

  productFields.itemNo = rand();

  try {
    productFields.name = productFields.name
      .trim()
      .replace(/\s\s+/g, " ");

  } catch (err) {
    res.status(400).json({
      message: `Error happened on server: "${err}" `
    });
  }

  const updatedProduct = queryCreator(productFields);

  const newProduct = new Product(updatedProduct);

  newProduct
    .save()
    .then(product => res.json(product))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.updateProduct = (req, res, next) => {
  const { id } = req.params;
  if (!isValidMongoId(id)) {
    return res.status(400).json({
      message: `Product with id "${id}" is not valid`
    });
  }

  Product.findById(id)
    .then(product => {
      if (!product) {
        return res.status(400).json({
          message: `Product with id "${req.params.id}" is not found.`
        });
      } else {
        const productFields = _.cloneDeep(req.body);

        try {
          productFields.name = productFields.name
            .trim()
            .replace(/\s\s+/g, " ");
        } catch (err) {
          res.status(400).json({
            message: `Error happened on server: "${err}" `
          });
        }

        const updatedProduct = queryCreator(productFields);

        Product.findOneAndUpdate(
          { _id: req.params.id },
          { $set: updatedProduct },
          { new: true }
        )
          .then(product => res.json(product))
          .catch(err =>
            res.status(400).json({
              message: `Error happened on server: "${err}" `
            })
          );
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.getProducts = async (req, res, next) => {
  const mongooseQuery = filterParser(req.query);
  const perPage = Number(req.query.perPage);
  const startPage = Number(req.query.startPage);
  const sort = req.query.sort;
  const q = typeof req.query.q === 'string' ? req.query.q.trim() : null

  if (q) {
    mongooseQuery.name = {
      $regex: new RegExp(q, "i"),
    };
  }

  try {
    const products = await Product.find(mongooseQuery)
      .skip(startPage * perPage - perPage)
      .limit(perPage)
      .sort(sort)

    const total = await Product.countDocuments(mongooseQuery);
    const countPage = Math.ceil(total / perPage);

    res.json({ data: products, total, countPage });
  } catch (err) {
    res.status(400).json({
      message: `Error happened on server: "${err}" `
    });
  }
};

exports.getProductById = (req, res, next) => {
  const { id } = req.params;
  if (!isValidMongoId(id)) {
    return res.status(400).json({
      message: `Product with id "${id}" is not valid`
    });
  }
  Product.findById(id)
    .then(product => {
      if (!product) {
        res.status(400).json({
          message: `Product with itemNo ${req.params.itemNo} is not found`
        });
      } else {
        res.json(product);
      }
    })
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `
      })
    );
};

exports.getProductByFilter = async (req, res, next) => {
  try {
    const [selling, trending, recently, rated] = ['Top Selling', 'Trending Products', 'Recently Added', 'Top Rated']
    const result = {};
    const page = parseInt(req.body.page)
    const limit = parseInt(req.body.limit)

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const queryParams = req.query;
    const countProducts = await Product.countDocuments(queryParams)

    if (endIndex < countProducts) {
      result.next = {
        page: page + 1,
        limit: limit
      }
    }

    if (startIndex > 0) {
      result.previous = {
        page: page - 1,
        limit: limit
      }
    }

    result.countPages = Math.ceil(countProducts / limit)

    if (queryParams.hasOwnProperty('priorityName')) {
      if (queryParams?.priorityName.includes(rated)) {
        const resultRated = await Product.find().sort({ reviews: -1 }).limit(limit)
        result.results = {
          [rated]: resultRated
        }
      }
      if (queryParams?.priorityName.includes(recently)) {
        const resultRecently = await Product.find().sort({ date: -1 }).limit(limit)
        result.results[recently] = resultRecently
      }
      if (queryParams?.priorityName.includes(trending)) {
        const resultTrending = await Product.find({ sticker: 'Hot' }).limit(limit)
        result.results[trending] = resultTrending
      }
      if (queryParams?.priorityName.includes(selling)) {
        const resultTrending = await Product.find({ sticker: 'Sale' }).limit(limit)
        result.results[selling] = resultTrending
      }

      if (!Object.keys(result.results).length) {
        res.status(200).json({ message: `The filter is not specified correctly and nothing was found for this filter `, result: 0 });
      } else {
        res.status(200).json(result)
      }

    } else {
      result.results = await Product.find(queryParams).skip(startIndex).limit(limit)
      if (!result.results.length) {
        res.status(200).json({ message: `The filter is not specified correctly`, result: 0 });
      } else {
        res.status(200).json(result)
      }
    }

  } catch (err) {
    res.status(400).json({
      message: `Error happened on server: "${err}" `
    });
  }
}

exports.getProductOnSearch = (req, res, next) => {
  const { search } = req.body
  try {
    Product.find({ name: { $regex: search, $options: 'i' } })
      .then((data) => {
        if (!data.length) {
          res.status(200).json({ message: `Oops, nothing found`, result: 0 });
        } else {
          res.status(200).json(data)
        }
      })
      .catch((err) =>
        res.status(400).json({
          message: `Error happened on server: "${err}" `,
        })
      );
  } catch (err) {
    res.status(400).json({
      message: `Error happened on server: "${err}" `
    });
  }
}