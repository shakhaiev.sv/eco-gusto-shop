const Cart = require("../models/Cart");
const Order = require("../models/Order");
const Product = require("../models/Product");
const sendMail = require("../commonHelpers/mailSender");
const validateOrderForm = require("../validation/validationHelper");
const queryCreator = require("../commonHelpers/queryCreator");
const productAvailibilityChecker = require("../commonHelpers/productAvailibilityChecker");
const subtractProductsFromCart = require("../commonHelpers/subtractProductsFromCart");
const _ = require("lodash");
const productsFromCart = require('../commonHelpers/productFromCart')
const uniqueRandom = require("unique-random");
const rand = uniqueRandom(1000000, 9999999);

// Create a new order
exports.createOrder = async (req, res) => {

  try {
    const order = _.cloneDeep(req.body)
    order.orderNo = String(rand())
    order.products = await productsFromCart(order.products)
    order.totalSum = order.products.reduce((acum, item) => acum + item.product.currentPrice * item.cartQuantity, 0).toFixed(2)

    const productAvailibilityInfo = await productAvailibilityChecker(order.products);

    if (!productAvailibilityInfo.productsAvailibilityStatus) {
      return res.json({
        message: "Some of your products are unavailable for now",
        productAvailibilityInfo,
      });
    }

    const subscriberMail = req.body.email;
    const letterSubject = req.body.letterSubject || "Thank you for your order! You are welcome!";
    const letterHtml = req.body.letterHtml || `<div style="font-family: Arial, sans-serif; padding: 20px;">
                <h2 style="color: #333;">Thank You for Your Order!</h2>
                <p style="color: #555;">Dear ${req.body.customer.firstName},</p>
                <p style="color: #555;">We appreciate your business and we're excited to get your order on its way! Below are your order details:</p>
                <p style="color: #555;"><strong>Shipping Address:</strong> ${req.body.deliveryAddress.address}, ${req.body.deliveryAddress.city}, ${req.body.deliveryAddress.country}</p>
                <p style="color: #555;">If you have any questions, feel free to contact our support team.</p>
                <p style="color: #555;">Best regards,<br>Your Eco Gusto</p></div>`;

    if (!letterSubject) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterSubject' for the letter.",
      });
    }

    if (!letterHtml) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterHtml' for the letter.",
      });
    }

    const newOrder = new Order(order);

    newOrder
      .save()
      .then(async (saveOrder) => {
        const mailResult = await sendMail(subscriberMail, letterSubject, letterHtml, res)

        const getProductonDB = await Promise.all(
          saveOrder.products.map(
            async (item) => {
              const product = await Product.findOne({ _id: item.product._id })
              return product
            }
          )
        )

        const updateProductQuantity = await Promise.all(
          getProductonDB.map(
            async (item) => {
              const productNewQuantity = await Product.findByIdAndUpdate(
                item._id,
                { quantity: item.quantity - saveOrder.products.find((product) => String(product.product._id) === String(item._id)).cartQuantity },
                { new: true }
              )
              return productNewQuantity
            }
          )
        )
        if (updateProductQuantity.length) {
          res.status(200).json({ result: 1, order: saveOrder, mailResult });
        }
      })
      .catch((err) =>
        res.status(400).json({
          message: `Error happened on server: "${err}"`,
        })
      );
  } catch (error) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

// Place an order
exports.placeOrder = async (req, res) => {
  try {
    const order = _.cloneDeep(req.body);
    order.orderNo = String(rand()); // Generate a unique order number

    let cartProducts = [];

    if (req.body.deliveryAddress) {
      order.deliveryAddress = req.body.deliveryAddress;
    }

    if (req.body.shipping) {
      order.shipping = req.body.shipping;
    }

    if (req.body.paymentInfo) {
      order.paymentInfo = req.body.paymentInfo;
    }

    if (req.user && req.user.id) {
      order.customerId = req.user.id;
      cartProducts = await subtractProductsFromCart(order.customerId);
    }

    if (!req.body.products && cartProducts.length < 1) {
      return res.status(400).json({ message: "The list of products is required, but absent!" });
    }

    if (cartProducts.length > 0) {
      order.products = _.cloneDeep(cartProducts);
    } else {
      order.products = req.body.products;
    }

    // Calculate total order sum
    order.totalSum = order.products.reduce(
      (sum, cartItem) => sum + cartItem.product.currentPrice * cartItem.cartQuantity,
      0
    );

    const productAvailibilityInfo = await productAvailibilityChecker(order.products);

    if (!productAvailibilityInfo.productsAvailibilityStatus) {
      return res.json({
        message: "Some of your products are unavailable for now",
        productAvailibilityInfo,
      });
    }

    const subscriberMail = req.body.email;
    const letterSubject = req.body.letterSubject || "Thank you for your order! You are welcome!";
    const letterHtml = `<h1>Your order is placed. OrderNo is ${order.orderNo}.</h1><div>${req.body.letterHtml}</div>`;

    const { errors, isValid } = validateOrderForm(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    if (!letterSubject) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterSubject' for the letter.",
      });
    }

    if (!letterHtml) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterHtml' for the letter.",
      });
    }

    const newOrder = new Order(order);

    if (order.customerId) {
      await newOrder.populate("customerId").execPopulate();
    }

    newOrder
      .save()
      .then(async (savedOrder) => {
        const mailResult = await sendMail(subscriberMail, letterSubject, letterHtml, res);

        // Update product quantities
        for (const item of savedOrder.products) {
          const product = await Product.findById(item.product._id);
          const productQuantity = product.quantity;
          await Product.findByIdAndUpdate(
            item.product._id,
            { quantity: productQuantity - item.cartQuantity },
            { new: true }
          );
        }

        res.json({ result: 1, order: savedOrder, mailResult });
      })
      .catch((err) =>
        res.status(400).json({
          message: `Error happened on server: "${err}"`,
        })
      );
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

// Update an order
exports.updateOrder = async (req, res) => {
  try {
    const currentOrder = await Order.findById(req.params.id);

    if (!currentOrder) {
      return res.status(404).json({ message: `Order with id ${req.params.id} is not found` });
    }

    const order = _.cloneDeep(req.body);

    if (req.body.deliveryAddress) {
      order.deliveryAddress = req.body.deliveryAddress;
    }

    if (req.body.shipping) {
      order.shipping = req.body.shipping;
    }

    if (req.body.paymentInfo) {
      order.paymentInfo = req.body.paymentInfo;
    }

    if (req.body.customerId) {
      order.customerId = req.body.customerId;
    }

    if (req.body.products) {
      order.products = req.body.products;

      order.totalSum = order.products.reduce(
        (sum, cartItem) => sum + cartItem.product.currentPrice * cartItem.cartQuantity,
        0
      );

      const productAvailibilityInfo = await productAvailibilityChecker(order.products);

      if (!productAvailibilityInfo.productsAvailibilityStatus) {
        return res.json({
          message: "Some of your products are unavailable for now",
          productAvailibilityInfo,
        });
      }
    }

    const subscriberMail = req.body.email;
    const letterSubject = req.body.letterSubject || "Thank you for your order! You are welcome!";
    const letterHtml = `<h1>Your order is updated. OrderNo is ${order.orderNo}.</h1><div>${req.body.letterHtml}</div>`;

    const { errors, isValid } = validateOrderForm(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    if (!letterSubject) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterSubject' for the letter.",
      });
    }

    if (!letterHtml) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterHtml' for the letter.",
      });
    }

    const updatedOrder = await Order.findByIdAndUpdate(req.params.id, { $set: order }, { new: true })
      .populate("customerId")
      .exec();

    const mailResult = await sendMail(subscriberMail, letterSubject, letterHtml, res);


    res.json({ order: updatedOrder, mailResult });
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

// Cancel an order
exports.cancelOrder = async (req, res) => {
  try {
    const currentOrder = await Order.findById(req.params.id);

    if (!currentOrder) {
      return res.status(404).json({ message: `Order with id ${req.params.id} is not found` });
    }

    const subscriberMail = req.body.email;
    const letterSubject = req.body.letterSubject;
    const letterHtml = req.body.letterHtml;

    const { errors, isValid } = validateOrderForm(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    if (!letterSubject) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterSubject' for the letter.",
      });
    }

    if (!letterHtml) {
      return res.status(400).json({
        message:
          "This operation involves sending a letter to the client. Please provide field 'letterHtml' for the letter.",
      });
    }

    const canceledOrder = await Order.findByIdAndUpdate(
      req.params.id,
      { canceled: true },
      { new: true }
    )
      .populate("customerId")
      .exec();

    const mailResult = await sendMail(subscriberMail, letterSubject, letterHtml, res);

    res.json({ order: canceledOrder, mailResult });
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

// Delete an order
exports.deleteOrder = async (req, res) => {
  try {
    const order = await Order.findById(req.params.id);

    if (!order) {
      return res.status(404).json({ result: 0, message: `Order with id ${req.params.id} is not found.` });
    }

    const deletedOrder = await Order.findByIdAndDelete(req.params.id);

    res.status(200).json({
      result: 1,
      message: `Order with id "${deletedOrder._id}" is successfully deleted from DB. Order Details: ${deletedOrder}`,
    });
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

// Get all orders for the authenticated user
exports.getOrders = async (req, res) => {


  try {
    const orders = await Order.find({ customerId: req.user.id }).populate("customerId").exec();
    res.json(orders);
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

// Get a specific order by order number
exports.getOrder = async (req, res) => {


  try {
    const order = await Order.findOne({ orderNo: req.params.orderNo }).populate("customerId").exec();

    if (!order) {
      return res.status(404).json({ message: `Order with order number ${req.params.orderNo} is not found.` });
    }

    res.json(order);
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

exports.getOrdersAllClients = async (req, res) => {
  try {
    const orders = await Order.find({
      date: {
        $gte: new Date(req.body.dateFrom),
        $lte: new Date(req.body.dateTo)
      },
      ...req.body.statusDelivery
    }).populate('customerId')

    res.status(200).json(orders)
  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
};

exports.putOrderStatusOnShipped = async (req, res) => {
  try {
    const currentOrder = await Order.findById(req.params.id);

    if (!currentOrder) {
      return res.status(404).json({ message: `Order with id ${req.params.id} is not found` });
    }


    const subscriberMail = currentOrder.email;
    const letterSubject = `Gusto supermerket. Your order for number ${currentOrder.orderNo} has changed status to shipped`;
    const letterHtml = `<h1>Good day</h1>
    <div>Your order by number ${currentOrder.orderNo} is on the way.</div> 
    <div>Please wait for a call from our courier to clarify the details</div>
    `;

    const shippedOrder = await Order.findByIdAndUpdate(
      req.params.id,
      { status: 'shipped' },
      { new: true }
    )
      .populate('customerId')
      .exec()

    const mailResult = await sendMail(subscriberMail, letterSubject, letterHtml, res)

    res.status(200).json({ order: shippedOrder, mailResult })

  } catch (err) {
    res.status(500).json({
      message: `Error happened on server: "${err}"`,
    });
  }
}



module.exports = {
  createOrder: exports.createOrder,
  placeOrder: exports.placeOrder,
  updateOrder: exports.updateOrder,
  cancelOrder: exports.cancelOrder,
  deleteOrder: exports.deleteOrder,
  getOrders: exports.getOrders,
  getOrder: exports.getOrder,
  getOrdersAllClients: exports.getOrdersAllClients,
  putOrderStatusOnShipped: exports.putOrderStatusOnShipped,
};


