const Deals = require('../models/Deals')
const isValidMongoId = require("../validation/isValidMongoId");
const queryCreator = require("../commonHelpers/queryCreator");
const _ = require("lodash");




exports.addDeals = (req, res, next) => {

    const dealsFields = _.cloneDeep(req.body);
    const updatedDeals = queryCreator(dealsFields);
    const newDeals = new Deals(updatedDeals);
    newDeals
        .save()
        .then(deals => res.json(deals))
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};


exports.getDeals = async (req, res, next) => {
    Deals.find()
        .populate('products.product')
        .then((deals) => res.json(deals)
        )
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.getDealsById = (req, res, next) => {
    const { id } = req.params;
    if (!isValidMongoId(id)) {
        return res.status(400).json({
            message: `Deals with id "${id}" is not valid`
        });
    }
    Deals.findById(id)
        .populate('products.product')
        .then(deals => {
            if (!deals) {
                res.status(400).json({
                    message: `Deals with itemNo ${id} is not found`
                });
            } else {
                res.json(deals);
            }
        })
        .catch(err =>
            res.status(400).json({
                message: `Error happened on server: "${err}" `
            })
        );
};

exports.deleteDeals = (req, res, next) => {
    Deals.findOne({ _id: req.params.id }).then(async deals => {
        if (!deals) {
            return res.status(400).json({
                message: `Deals with id "${req.params.id}" is not found.`
            });
        } else {
            const dealsToDelete = await Deals.findOne({ _id: req.params.id });

            Deals.deleteOne({ _id: req.params.id })
                .then(deletedCount =>
                    res.status(200).json({
                        message: `Deals witn id "${dealsToDelete.id}" is successfully deleted from DB.`,
                        'result': 1
                    })
                )
                .catch(err =>
                    res.status(400).json({
                        message: `Error happened on server: "${err}" `
                    })
                );
        }
    });
}
