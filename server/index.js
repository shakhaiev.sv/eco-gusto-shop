const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const path = require('path');
const cors = require("cors")
require('dotenv').config();

const globalConfigs = require('./src/routes/globalConfigs.js');
const customers = require('./src/routes/customers.js');
const catalog = require('./src/routes/catalog.js');
const products = require('./src/routes/products.js');
const colors = require('./src/routes/colors.js');
const sizes = require('./src/routes/sizes.js');
const filters = require('./src/routes/filters.js');
const subscribers = require('./src/routes/subscribers.js');
const cart = require('./src/routes/cart.js');
const orders = require('./src/routes/orders.js');
const links = require('./src/routes/links.js');
const pages = require('./src/routes/pages.js');
const slides = require('./src/routes/slides.js');
const wishlist = require('./src/routes/wishlist.js');
const comments = require('./src/routes/comments.js');
const shippingMethods = require('./src/routes/shippingMethods.js');
const paymentMethods = require('./src/routes/paymentMethods.js');
const partners = require('./src/routes/partners.js');
const deals = require('./src/routes/deals.js')

const app = express();
app.use(cors())
app.use(express.json())


const port = process.env.PORT || 4000;
// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Connect to MongoDB
mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true })
  .then(() => console.log('MongoDB Connected'))
  .catch((err) => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport Config
require('./src/services/passport.js')(passport);

// Use Routes
app.use('/api/configs', globalConfigs);
app.use('/api/customers', customers);
app.use('/api/catalog', catalog);
app.use('/api/products', products);
app.use('/api/colors', colors);
app.use('/api/sizes', sizes);
app.use('/api/filters', filters);
app.use('/api/subscribers', subscribers);
app.use('/api/cart', cart);
app.use('/api/orders', orders);
app.use('/api/links', links);
app.use('/api/pages', pages);
app.use('/api/slides', slides);
app.use('/api/wishlist', wishlist);
app.use('/api/comments', comments);
app.use('/api/shipping-methods', shippingMethods);
app.use('/api/payment-methods', paymentMethods);
app.use('/api/partners', partners);
app.use('/api/deals', deals);

app.use(express.static('./client/dist'));

app.listen(port, () => console.log(`Server running on port ${port}`));

module.exports = app



